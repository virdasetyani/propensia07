<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Absensi extends Model {

	use SoftDeletes;

    protected $dates = ['deleted_at'];
	
	protected $table='absensi';

	public $timestamps = false;

	protected $fillable = ['Tanggal', 'Apegawai', 'AIdPeg', 'Hadir', 'Sakit', 'Izin', 'Alpha', 'JamDatang', 'JamPulang', 'Aterm', 'IsApproved'];

	protected $primaryKey = 'tanggal';
}
