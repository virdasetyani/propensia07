<?php namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Agenda extends Model {

	use SoftDeletes;

    protected $dates = ['deleted_at'];

	protected $table='agenda';

	public $timestamps = false;

	protected $fillable = ['Aktivitas', 'Deskripsi', 'Waktu'];

	protected $primaryKey = 'IdAgenda';

}
