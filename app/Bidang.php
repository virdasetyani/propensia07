<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bidang extends Model {

	use SoftDeletes;

    protected $dates = ['deleted_at'];
	
	protected $table='bidang';

	public $timestamps = false;

	protected $fillable = ['IdBid', 'Nama'];

	protected $primaryKey = 'IdBid';
/*	public function kpi()  {
        return $this->hasMany('App\Kpi_Bidang', 'KBidang', 'IdBid');
    } */

}
