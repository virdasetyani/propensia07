<?php namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model {
	use SoftDeletes;

    protected $dates = ['deleted_at'];

	protected $table='comment';

	public $timestamps = false;

	protected $fillable = ['Isi'];

	protected $primaryKey = 'IdComment';

}
