<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Dev_Plan extends Model {

	protected $table='dev_plan';

	public $timestamps = false;

	protected $primaryKey = "Id";

}
