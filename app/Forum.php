<?php namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Forum extends Model {
	use SoftDeletes;

    protected $dates = ['deleted_at'];

	protected $table='forum';

	public $timestamps = false;

	protected $fillable = ['Nama', 'Keterangan'];

	protected $primaryKey = 'IdForum';

	public function threads()
    {
        return $this->hasMany('App\Thread', 'IdFor', 'IdForum');
    }

}
