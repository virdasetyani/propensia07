<?php namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use View;
use App\Absensi;
use App\Pegawai;
use App\Term;
use Validator;
use Illuminate\Http\Request;

class AbsensiController extends Controller {

public function __construct()
	{ 
		$this->middleware('auth'); 
		$this->middleware('admin', ['only' => ['create', 'store', 'edit','update','confirm', 'destroy','index' ]]);
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return m_responsekeys(`, identifier)
	 */
	public function index()
	{
		$time = Carbon::now();
		$dataAbsenHariIni = Absensi::where('Tanggal', 'LIKE', $time->toDateString())->get();
		if($dataAbsenHariIni->count() > 0)
		{
			Return View::make('absensi/absensi', compact('dataAbsenHariIni'));
		}
		else
		{
			$dataPegawai = Pegawai::all();
			Return View::make('absensi/absensi', compact('dataPegawai'));
		}

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$dataPegawai = Pegawai::all();
		$dateTime = Carbon::now();
		$dataAbsenHariIni = Absensi::where('Tanggal', 'LIKE', $dateTime->toDateString())->get();
		$currentDate = $dateTime->toDateString();
		if($dataAbsenHariIni->isEmpty())
		{
			foreach ($dataPegawai as $data)
			{
				$absen = new Absensi;
				$absen->Tanggal = $currentDate;
				$absen->Apegawai = $request[$data->IdPeg.'Nama'];
				$absen->AIdPeg = $data->IdPeg;
				if ($request[$data->IdPeg.'Status'] == "hadir")
				{
					$absen->Hadir = 1;
					$absen->Sakit = 0;
					$absen->Izin = 0;
					$absen->Alpha = 0;	
				}
				elseif ($request[$data->IdPeg.'Status'] == "sakit")
				{
					$absen->Hadir = 0;
					$absen->Sakit = 1;
					$absen->Izin = 0;
					$absen->Alpha = 0;	
				}
				elseif ($request[$data->IdPeg.'Status'] == "izin")
				{
					$absen->Hadir = 0;
					$absen->Sakit = 0;
					$absen->Izin = 1;
					$absen->Alpha = 0;	
				}
				elseif ($request[$data->IdPeg.'Status'] == "alpha")
				{
					$absen->Hadir = 0;
					$absen->Sakit = 0;
					$absen->Izin = 0;
					$absen->Alpha = 1;	
				}

				$absen->JamDatang = $request[$data->IdPeg.'JamDtg'];
				$absen->JamPulang = $request[$data->IdPeg.'JamPlg'];
				$currentMonth = $dateTime->format('M');
				$currentYear = $dateTime->format('Y');
				$idTerm = Term::select('IdTerm')->where('Bulan', '=', $currentMonth)->where('Tahun', '=', $currentYear)->get();
				foreach ($idTerm as $IdTerm) {
					$absen->Aterm = $IdTerm->IdTerm;
				}

				$absen->save();
			}
		}
		else
		{
			foreach ($dataPegawai as $data)
			{
				$Hadir = 0;
				$Sakit = 0;
				$Izin = 0;
				$Alpha = 0;
				$JamDatang = 0;
				$JamPulang = 0;

				if ($request[$data->IdPeg.'Status'] == "hadir")
				{
					$Hadir = 1;
					$Sakit = 0;
					$Izin = 0;
					$Alpha = 0;	
				}
				elseif ($request[$data->IdPeg.'Status'] == "sakit")
				{
					$Hadir = 0;
					$Sakit = 1;
					$Izin = 0;
					$Alpha = 0;	
				}
				elseif ($request[$data->IdPeg.'Status'] == "izin")
				{
					$Hadir = 0;
					$Sakit = 0;
					$Izin = 1;
					$Alpha = 0;	
				}
				elseif ($request[$data->IdPeg.'Status'] == "alpha")
				{
					$Hadir = 0;
					$Sakit = 0;
					$Izin = 0;
					$Alpha = 1;	
				}

				$JamDatang = $request[$data->IdPeg.'JamDtg'];
				$JamPulang = $request[$data->IdPeg.'JamPlg'];

				Absensi::where('Tanggal', '=', $currentDate)
						->where('AIdPeg', '=', $data->IdPeg)
						->update(array('Hadir' => $Hadir, 
										'Sakit' => $Sakit, 
										'Izin' => $Izin,
										'Alpha' => $Alpha,
										'JamDatang' => $JamDatang,
										'JamPulang' => $JamPulang));
			}
		}
		return redirect()->action('AbsensiController@index')->with('message', 'Absensi Berhasi Disimpan');
	}


	/**
	 * Display the specified resource.
	 *
	 * @return Response
	 */
	public function show()
	{
		$term = Term::all();

		Return View::make('absensi/view', compact('term'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function showDetail(Request $request)
	{
		$idTerm = $request['IdTerm'];
		$listTanggal = Absensi::distinct()->select('Tanggal')->where('Aterm', '=', $idTerm)->get();
		
		return redirect()->back()->with('data', $idTerm)->with('listTanggal', $listTanggal);
	}

	public function showMoreDetail(Request $request)
	{
		$rekap = $request['Rekap'];
		$idTerm = $request['idTerm'];
		$mode = $request['mode'];
		if ($mode == "Rekapan") {
			if ($rekap == "Bulanan") {
				$absen = Absensi::selectRaw('Apegawai, AIdPeg, SUM(Hadir) as Hadir, SUM(Sakit) as Sakit, SUM(Izin) as Izin, SUM(Alpha) as Alpha')->where('Aterm', '=', $idTerm)->groupBy('AIdPeg')->get();
				$bulan = Term::where('IdTerm', '=', $idTerm)->get();
				$bulanMsg = '';
				foreach ($bulan as $yey) {
					$bulanMsg = $yey->Bulan.' '.$yey->Tahun;
				}
				return redirect()->back()->with('absen', $absen)->with('message', 'Daftar Absen : '.$bulanMsg);
			} else if ($rekap == "Mingguan") {

			}
		} elseif ($mode == "Tanggalan") {
			$tanggal = $request['Tanggal'];
			$tanggalArray = explode("-", $tanggal);
			$tanggalCarbon = Carbon::createFromDate($tanggalArray[0], $tanggalArray[1], $tanggalArray[2]);
			var_dump($tanggalCarbon);
			$absen = Absensi::where('Aterm', '=', $idTerm)->where('Tanggal', '=', $tanggal)->get();

			$statusApproval = "";
			$approval = Absensi::where('IsApproved', '=', $statusApproval)->where('Tanggal', '=', $tanggal)->get();
			foreach ($absen as $tmp)
			{
				if($tmp->IsApproved == 1)
				{
					$statusApproval = "Disetujui";
				}
				else
				{
					$statusApproval = "Belum Disetujui";
				}
			}
			echo($statusApproval);
			return redirect()->back()->with('absen', $absen)->with('message', 'Daftar Absen : '.$tanggalCarbon->format('d M Y'))->with('tanggal', $tanggal)
									 ->with('messageApprove', 'Keterangan persetujuan : '.$statusApproval);
		}	
	}

	/**
	 * Display the specified resource.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function showApproval(Request $request)
	{
		// $time = Carbon::now();
		// $currentDate = $time->toDateString();
		// $daftarAbsen = Absensi::where('Tanggal', '=', $currentDate)->get();
		// Return View::make('absensi/approval', compact('daftarAbsen'));
		$term = Term::all();

		Return View::make('absensi/approval', compact('term'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function approve(Request $request)
	{
		$date = $request["date"];
		$submit = $request['submit'];
		$daftarAbsen = Absensi::where('Tanggal', '=', $date)->get();
		if ($submit === "approve")
		{
			foreach ($daftarAbsen as $absen) {
				Absensi::where('Tanggal', '=', $date)
						->where('AIdPeg', '=', $absen->AIdPeg)
						->update(array('IsApproved' => 1));
			}
			Return redirect()->back()->with('message1', 'Absensi berhasi disetujui');
		} 
		elseif ($submit == "disapprove") 
		{
			echo "masuk disapprove";
			foreach ($daftarAbsen as $absen) {
				Absensi::where('Tanggal', '=', $date)
						->where('AIdPeg', '=', $absen->AIdPeg)
						->update(array('IsApproved' => 0));
			}
			Return redirect()->back()->with('message1', 'Absensi tidak disetujui');
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function confirm($id)
	{
		//
	}
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{	
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}