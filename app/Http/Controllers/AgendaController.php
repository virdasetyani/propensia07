<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use App\Agenda;
use Illuminate\Http\Request;
use Validator;

class AgendaController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('admin', ['only' => ['create', 'store', 'edit', 'update', 'confirm', 'destroy' ]]);
		$this->middleware('manajer', ['only' => ['create', 'store', 'edit', 'update', 'confirm', 'destroy' ]]);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$agenda = array();

		$a2 = Agenda::where('Waktu', '<', date('Y-m-d'))->orderby('Waktu', 'asc')->get();
		if($a2 != null){
			foreach ($a2 as $a2){
				array_unshift($agenda, $a2);
			}
		}

		$a1 = Agenda::where('Waktu', '>=', date('Y-m-d'))->orderby('Waktu', 'desc')->get();
		if($a1 != null){
			foreach ($a1 as $a1){
				array_unshift($agenda, $a1);
			}
		}

		Return View::make('agenda/listagenda', compact('agenda'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('agenda/create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */

	public function store(Request $request)
	{
		$data = $request->all();

		$rules = array(
			'Aktivitas' => 'required',
			'Waktu' => 'required'
			);

		$messages = array(
       		'required' => 'Harus diisi!'
    	);

		$validator= Validator::make($data, $rules, $messages);

		if($validator->fails()){
			$messages = $validator->messages();

			return redirect('/agenda/create')
				->withInput($request->only('Aktivitas', 'Deskripsi', 'Waktu'))
				->withErrors($validator);
		}
		else{
		$agenda = new Agenda;
		$agenda->Aktivitas = $data['Aktivitas'];
		$agenda->Deskripsi = $data['Deskripsi'];
		$agenda->Waktu = $data['Waktu'];
		$agenda->save();
		return redirect('/agenda');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$content = Agenda::where('IdAgenda', '=', compact('id'))->firstOrFail();
		Return View::make('agenda/agenda', compact('content'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data = Agenda::where('IdAgenda', '=', compact('id'))->firstOrFail();
		Return View::make('agenda/edit', compact('data'));
	}

	public function confirm($id)
	{
		$data = Agenda::where('IdAgenda', '=', compact('id'))->firstOrFail();
		return View::make('agenda/confirm', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		$data = $request->all();

		$rules = array(
			'Aktivitas' => 'required',
			'Waktu' => 'required'
			);

		$messages = array(
       		'required' => 'Harus diisi!'
    	);

		$validator= Validator::make($data, $rules, $messages);

		if($validator->fails()){
			$messages = $validator->messages();

			return redirect('/agenda/create')
				->withInput($request->only('Aktivitas', 'Deskripsi', 'Waktu'))
				->withErrors($validator);
		}
		else{
		$agenda = Agenda::where('IdAgenda', '=', $data['id'])->firstOrFail();
		$agenda->Aktivitas = $data['Aktivitas'];
		$agenda->Deskripsi = $data['Deskripsi'];
		$agenda->Waktu = $data['Waktu'];
		$agenda->save();
		return redirect('/agenda');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$agenda = Agenda::where('IdAgenda', '=', compact('id'))->firstOrFail();
		$agenda->delete();
		return redirect()->action('AgendaController@index')->with('delete', 'Agenda berhasil dihapus');
	}

}
