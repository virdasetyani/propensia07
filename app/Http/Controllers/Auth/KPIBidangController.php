<?php namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use App\Kpi;
use App\Kpi_Pegawai;
use App\Pegawai;
use App\Users;
use App\Term;

use Illuminate\Http\Request;

class KPIPegawaiController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data = Kpi::all();
		$term = Term::all();
		Return View::make('kpip/kpip', compact('data', 'term'));
	}

	public function term()
	{
		$data = Term::all();
		Return View::make('kpip/index', compact('data'));
	} 

	public function viewKpi(Request $request)
	{
		$model = Pegawai::where('Username', '=', Auth::user()->Username)->firstOrFail();
		$id = $model['IdPeg'];
		$kpi = Pegawai::find($id)->kpi;

		/*for ($i=0; $i < count($kpi) ; $i++) { 
			$tmp = Kpi::where("IdKpi","=",$kpi[$i]['IdKpi']);
		}
		/*foreach($kpi as $kpi){
			$tmp = Kpi::where("IdKpi","=",$kpi['IdKpi']);
			var_dump($tmp['Indikator']);
		}*/
		
		$tmp = Kpi::where('KTerm', '=', $request['IdTerm'])->get();
		//return View::make('/kpip/kpip', compact('term', 'data'));
		return redirect()->back()->with('data', $tmp);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		//$model = Pegawai::where('Username', '=', Auth::user()->Username)->firstOrFail();
		//$id = $model->IdPeg;
		$id = $request->all();
		$term = Term::where('IdTerm','=',$id['IdTerm'])->firstOrFail();
		Return View::make('kpip/create', compact('term'));
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$model = Pegawai::where('Username', '=', Auth::user()->Username)->firstOrFail();
		$id = $model['IdPeg'];

		for($i=0; $i<2; $i++){
			$kpi = new Kpi;
			$kpi->Indikator = $request['Indikator'][$i];
			$kpi->Target = $request['Target'][$i];
			$kpi->Aktual = $request['Aktual'][$i];
			$kpi->PersenTarget = $request['PersenTarget'][$i];
			$kpi->PersenAktual = $request['PersenAktual'][$i];
			$kpi->KTerm = $request['IdTerm'];
			$kpi->save();
			
			$last = Kpi::orderby('IdKpi','desc')->first();
			$kpip = new Kpi_Pegawai;
			$kpip->IdKpi = $last['IdKpi'];
			$kpip->KPegawai = $id;
			$kpip->save();
		}

		//return redirect('kpip/create')->with('message', 'KPI Berhasil Disimpan!');
		return redirect()->back()->with('message', 'KPI Pegawai Telah Berhasil Disimpan!');
		//return redirect()->action('KPIPegawaiController@create', compact('request'));	
		/*
		

		$last = Kpi::orderby('created_at','desc')->first();
		$kpip = new Kpi_Pegawai;
		$kpip->IdKpi = $last['IdKpi'];
		$kpip->KPegawai = $id;
		$kpip->save();
		//$ind = $request->all();
		//$model->
**/
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data = Kpi::where('IdKpi', '=', compact('id'))->firstOrFail();
		Return View::make('kpip/edit', compact('data'));
	}

	public function feedback(Request $request)
	{
		$tmp = $request->all();
		$kpi = Kpi::where('IdKpi', '=', $tmp['IdKpi'])->firstOrFail();
		$kpi->Notes = $tmp['Notes'];
		$kpi->PersenAchievement = $tmp['PersenAchievement'];
		if($tmp['Status']= '1'){
			$kpi->isApproved = true;
		}
		else{
			$kpi->isApproved = false;
		}
		$kpi->save();

		return redirect()->back()->with('approve', 'KPI Pegawai Telah Berhasil Diubah!');
	}

	public function approve()
	{
		$data = Kpi::all();
		$term = Term::all();
		Return View::make('/kpip/approve', compact('data', 'term'));
	}
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		$kpi = Kpi::where('IdKpi', '=', $request['IdKpi'])->firstOrFail();
		$kpi->Indikator = $request['Indikator'];
		$kpi->Target = $request['Target'];
		$kpi->Aktual = $request['Aktual'];
		$kpi->PersenTarget = $request['PersenTarget'];
		$kpi->PersenAktual = $request['PersenAktual'];
		$kpi->save();
		return redirect()->action('KPIPegawaiController@index');
	}

	public function view()
	{
		$term = Term::all();
		$data = Kpi::all();
		return View::make('kpip/update', compact('data', 'term'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$kpi = Kpi::where('IdKpi', '=', compact('id'))->firstOrFail();
		$kpi->delete();
		return redirect()->action('KPIPegawaiController@index');

	}

	public function confirm($id)
	{
		$data = Kpi::where('IdKpi', '=', compact('id'))->firstOrFail();
		return View::make('kpip/confirm', compact('data'));
	}

	public function notes($id)
	{
		$data = Kpi::where('IdKpi', '=', compact('id'))->firstOrFail();
		return View::make('/kpip/notes', compact('data'));
	}

	public function devplan()
	{
		return View::make('/kpip/devplan');
	}

	public function storeDevplan(Request $request)
	{
		return redirect()->back()->with('devplan', 'Development Plan Berhasil Disimpan!');
	}
}
