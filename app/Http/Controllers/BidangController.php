<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use App\Bidang;
use Validator;
use Illuminate\Http\Request;

class BidangController extends Controller {

public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('admin', ['only' => ['create', 'store', 'edit','update','confirm', 'destroy','index' ]]);
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data = Bidang::all();
		Return View::make('bidang/bidang', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('bidang/create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$nama = $request->all();

		$rules = array(
			'NamaBid' => 'required|unique:bidang');


		$validator= Validator::make($nama, $rules);

		if($validator->fails()){
			$messages = $validator->messages();

			return redirect('/bidang/create')
				->withInput($request->only('NamaBid'))
				->withErrors($validator);
		}
		else
		{

		$bid = new Bidang;
		$bid->NamaBid = $nama['NamaBid'];
		$bid->save();
		return redirect()->back()->with('message', 'Bidang Baru Berhasil Disimpan!');	
		}
		
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data = Bidang::where('IdBid', '=', compact('id'))->firstOrFail();
		Return View::make('bidang/edit', compact('data'));
	}
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function confirm($id)
	{
		$data = Bidang::where('IdBid', '=', compact('id'))->firstOrFail();
		return View::make('bidang/confirm', compact('data'));
	}
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{	
		$nama = $request->all();
		$rules = array('NamaBid' => 'required');
		$validator= Validator::make($nama, $rules);

		if($validator->fails()){

			$messages = $validator->messages();
			return redirect()->back()->with('message', 'Nama Bidang gagal diubah, Pastikan Nama Bidang terisi')
				->withInput($request->only('NamaBid'))
				->withErrors($validator);
		} 

			$bid = Bidang::where('IdBid', '=', $nama['id'])->firstOrFail();
			$bid->NamaBid = $nama['NamaBid'];
			$bid->save();

			return redirect()->back()->with('message', 'Nama Bidang berhasil diubah ');	
		
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$bid = Bidang::where('IdBid', '=', compact('id'))->firstOrFail();
		$bid->delete();
		return redirect('/bidang')->with('message','Bidang Berhasil Dihapus!');
	}

}
