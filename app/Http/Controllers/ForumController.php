<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use App\Forum;
use App\Thread;
use App\Comment;
use Illuminate\Http\Request;
use Validator;

class ForumController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('super_admin', ['only' => ['create', 'store', 'edit', 'update', 'confirm', 'destroy' ]]);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data = Forum::orderby('IdForum', 'desc')->get();
		Return View::make('forum/listforum', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('forum/create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$nama = $request->all();

		$rules = array(
			'Nama' => 'required');

		$messages = array(
       		'required' => 'Harus diisi!'
    	);

		$validator= Validator::make($nama, $rules, $messages);

		if($validator->fails()){
			$messages = $validator->messages();

			return redirect('/forum/create')
				->withInput($request->only('Nama', 'Keterangan'))
				->withErrors($validator);
		}
		else{
		$forum = new Forum;
		$forum->Nama = $nama['Nama'];
		$forum->Keterangan = $nama['Keterangan'];
		$forum->save();
		return redirect('/forum');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data = Forum::where('IdForum', '=', compact('id'))->firstOrFail();
		Return View::make('forum/edit', compact('data'));
	}

	public function confirm($id)
	{
		$data = Forum::where('IdForum', '=', compact('id'))->firstOrFail();
		return View::make('forum/confirm', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		$tmp = $request->all();

		$rules = array(
			'Nama' => 'required',
			'Keterangan' => 'required');

		$messages = array(
       		'required' => 'Harus diisi!'
    	);

		$validator= Validator::make($tmp, $rules, $messages);

		if($validator->fails()){
			$messages = $validator->messages();

			return redirect()->back()
				->withInput($request->only('Nama', 'Keterangan'))
				->withErrors($validator);
		}
		else{
		$forum = Forum::where('IdForum', '=', $tmp['id'])->firstOrFail();
		$forum->Nama = $tmp['Nama'];
		$forum->Keterangan = $tmp['Keterangan'];
		$forum->save();
		return redirect('/forum');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$forum = Forum::where('IdForum', '=', compact('id'))->firstOrFail();
		$thrd = Thread::where('IdFor', '=', compact('id'))->get();
		foreach($thrd as $t){
			$com = Comment::where('IdPost', '=', $t->IdThread)->get();
			foreach ($com as $c) {
				$c->delete();
			}

			$t->delete();
		}

		$forum->delete();
		return redirect('/forum')->with('message', 'Forum successfully deleted.');
	}

}
