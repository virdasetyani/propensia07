<?php namespace App\Http\Controllers;

use Auth;
use View;
use App\Users;
use App\Pegawai;
use App\Forum;
use App\Agenda;
use App\Laporan;
use Illuminate\Pagination\Paginator;


class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$user = Users::where('Username', '=', Auth::user()->Username)->firstOrFail();
		$forum = Forum::all();

		$agenda = Agenda::where('Waktu', '>=', date('Y-m-d'))->orderby('Waktu', 'asc')->paginate(5);
		$agenda->setPath('home');
			//$agenda = Agenda::orderby('Waktu', 'asc')->paginate(5);

		$data = Pegawai::find($user->Username);

		$lap = Laporan::orderby('IdLap', 'desc')->paginate(5);

		$data = Pegawai::find($user->Username);
		$nama = $data->Nama;
		
		return View::make('/home', compact('user', 'forum', 'data', 'lap', 'agenda'));

	}

}
