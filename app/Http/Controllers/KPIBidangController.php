<?php namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use App\Kpi;
use App\kpic;
use App\Bidang;
use App\Pegawai;
use App\Users;
use App\Term;
use App\Kpi_Bidang;
use Validator;
use DB;


use Illuminate\Http\Request;

class KPIBidangController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('manajer', ['only' => ['create', 'store', 'edit','update','confirm', 'destroy' ]]);
		$this->middleware('pimpinan', ['only' => ['approve','notes' ]]);	

	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		
		$bid = Bidang::all();
		$term = Term::all();
		Return View::make('kpib/kpib', compact('term', 'bid'));
	}

	public function term()
	{	
		
		$data = Term::all();
		$bid = Bidang::all();
		Return View::make('kpib/index', compact('data', 'bid'));
	} 

	public function hapus()
	{	
		
		$data = Term::all();
		$bid = Bidang::all();
		Return View::make('kpib/hapus', compact('data', 'bid'));
	} 

	public function viewKpi(Request $request)
	{	
	$kpi = Kpi::where('KBidang', '=',$request['IdBid'])->get();

		$arrayKPI = array();
		foreach($kpi as $singkong){
			
				$data = DB::table('kpi')->join('kpi_bidang', 'kpi.IdKpi', '=', 'kpi_bidang.IdKpi')
				->where('kpi.IdKpi','=', $singkong->IdKpi)->where('kpi_bidang.deleted_at', '=', null)->get();
				$data=array_map(function($item){
              		return (array) $item; },$data);

				foreach($data as $oke){
					if($oke['KBTerm'] == $request['IdTerm']){
						array_push($arrayKPI, $oke);
					} 
				}
		}
		return redirect()->back()->with('data', $arrayKPI);
	}


	public function lihat(Request $request)
	{	
		$kpi = Kpi::where('KBidang', '=',$request['IdBid'])->get();
		$arrayKPI = array();
		foreach($kpi as $singkong){
			
				$data = DB::table('kpi')->join('kpi_bidang', 'kpi.IdKpi', '=', 'kpi_bidang.IdKpi')
				->where('kpi.IdKpi','=', $singkong->IdKpi)->get();
				$data=array_map(function($item){
              		return (array) $item; },$data);

				foreach($data as $oke){
					if($oke['KBTerm'] == $request['IdTerm']){
						array_push($arrayKPI, $data);
					} 
				}
			}
			
				
		return redirect()->back()->with('data', $kpi); 
	}


	public function del(Request $request)
	{
		
	$kpi = Kpi::where('KBidang', '=',$request['IdBid'])->get();

		$arrayKPI = array();
		foreach($kpi as $singkong){
			
				$data = DB::table('kpi')->join('kpi_bidang', 'kpi.IdKpi', '=', 'kpi_bidang.IdKpi')
				->where('kpi.IdKpi','=', $singkong->IdKpi)->where('kpi_bidang.deleted_at', '=', null)->get();
				$data=array_map(function($item){
              		return (array) $item; },$data);

				foreach($data as $oke){
					if($oke['KBTerm'] == $request['IdTerm']){
						array_push($arrayKPI, $oke);
					} 
				}
		}
		return redirect()->back()->with('data', $arrayKPI);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		$id = $request->all();
		$data = Bidang::all();
		$term = Term::where('IdTerm','=',$id['IdTerm'])->firstOrFail();
		Return View::make('kpib/create', compact('term', 'data'));
		
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		
		for($i=0; $i<count($request['Indikator']); $i++){
			if($request['Indikator'][$i]==null ||
				$request['Target'][$i]==null || 
				$request['PersenTarget'][$i]==null ){
					return redirect()->back()->with('warning','Mohon isi semua kolom yang ada!');
			}
		} 

		for($i=0; $i<count($request['Indikator']); $i++){
			
			$kpi = new Kpi;
			$kpi->Indikator = $request['Indikator'][$i];
			$kpi->Target = $request['Target'][$i];
			$kpi->PersenTarget = $request['PersenTarget'][$i];
			$kpi->KBidang = $request['IdBid'];
			$kpi->Kterm = $request['IdTerm'];
			$kpi->save();

			$last = Kpi::orderby('IdKpi','desc')->first();
			$kpib = new Kpi_Bidang;
			$kpib->IdKpi = $last['IdKpi'];
			$kpib->KBTerm = $request['IdTerm'];
			$kpib->save();

		}

		return redirect()->back()->with('message', 'KPI Bidang Telah Berhasil Disimpan!');

		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data = Kpi::where('IdKpi', '=', compact('id'))->firstOrFail();
		Return View::make('kpib/edit', compact('data'));
	}

	public function feedback(Request $request)
	{
		$tmp = $request->all();
		$kpi = Kpi_Bidang::where('IdKpi', '=', $tmp['IdKpi'])
				->where('KBTerm', '=', $tmp['IdTerm'])->firstOrFail();
		$kpi->Notes = $tmp['Notes'];
		$kpi->PersenAchievement = $tmp['PersenAchievement'];
		if($tmp['Status']== '1'){
			$kpi->IsDone = true;
		}
		else{
			$kpi->IsDone = false;
		}
		$kpi->save();

		return redirect()->back()->with('approve', 'KPI Bidang Telah Berhasil Diubah!');
	}

	public function approve()
	{	
		
		$data = Term::all();
		$bid = Bidang::all();
		Return View::make('kpib/approve', compact('data', 'bid'));

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		
		$kpi = Kpi::where('IdKpi', '=', $request['IdKpi'])->firstOrFail();
		$kpi->Indikator = $request['Indikator'];
		$kpi->Target = $request['Target'];
		$kpi->PersenTarget = $request['PersenTarget'];
		$kpi->KTerm = $request['IdTerm'];
		$kpi->save();
		return redirect()->back()->with('message', 'KPI Bidang Berhasil Diubah!');
		
	}

	public function updateKpi(Request $request)
	{
		$kpi = Kpi::where('KBidang', '=',$request['IdBid'])->get();
		echo $request['Idbid'];
		//return redirect()->back()->with('data',$kpi);

	}
	

	public function view()
	{
		$bid = Bidang::all();
		Return View::make('kpib/update', compact('bid'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$kpi = Kpi::find($id);
		$kpi->delete();
		return redirect('kpic/hapus')->with('message', 'Indikator KPI Bidang Berhasil Dihapus!');

	}

	public function confirm($id)
	{
		$data = Kpi::where('IdKpi', '=', compact('id'))->firstOrFail();
		return View::make('kpib/confirm', compact('data'));
	}

	public function notes($id)
	{	
		$data = DB::table('kpi')->join('kpi_bidang', 'kpi.IdKpi', '=', 'kpi_bidang.IdKpi')
				->where('kpi_bidang.IdKpiBid','=', $id)->first();


		
		Return View::make('kpib/notes', compact('data'));

		
	} 

	public function setuju(Request $request)
	{	

		$kpi = Kpi::where('KBidang', '=',$request['IdBid'])->get();
		$arrayKPI = array();
		foreach($kpi as $singkong){
			
				$data = DB::table('kpi')->join('kpi_bidang', 'kpi.IdKpi', '=', 'kpi_bidang.IdKpi')
				->where('kpi.IdKpi','=', $singkong->IdKpi)->get();
				$data=array_map(function($item){
              		return (array) $item; },$data);

				
				foreach($data as $oke){
					if($oke['KBTerm'] == $request['IdTerm']){
						array_push($arrayKPI, $oke);
					} 
				}
		} 
		
			return redirect()->back()->with('data', $arrayKPI);
		
	} 

	public function approveBid($id){
		$Id = $id;
		$kpi = Kpi::find($id);
		if($kpi->IsApproved == 1){
			$kpi->IsApproved = false;
		}else{
			$kpi->IsApproved = true;
		}
		$kpi->save();
		return redirect()->back();
		
	}

	public function isi($id)
	{
		$data = Kpi::where('IdKpi', '=', compact('id'))->firstOrFail();
		$term = Term::all();
		Return View::make('kpib/isi', compact('data', 'term'));
	}

	public function updateIsi(Request $request){

		$data = Kpi_Bidang::where('IdKpi', '=', $request['IdKpi'])
			->where('KBTerm', '=', $request['IdTerm'])->first();
		if($data == null){
			$kpib = new Kpi_Bidang;
			$kpib->IdKpi = $request['IdKpi'];
			$kpib->KBTerm = $request['IdTerm'];
			$kpib->Aktual = $request['Aktual'];
			$kpib->PersenAktual = $request['PersenAktual'];
			$kpib->save();
			return redirect()->action('KPIBidangController@isi', ['kpib' => $request['IdKpi']])->with('message', 'KPI Bidang Berhasil Diisi!');
		}else{
			$data->Aktual = $request['Aktual'];
			$data->PersenAktual = $request['PersenAktual'];
			$data->save();
			return redirect()->action('KPIBidangController@isi', ['kpib' => $request['IdKpi']])->with('message', 'KPI Bidang Berhasil Diisi!');
		}
	}

	public function termIsi(Request $request)
	{
		$data = Kpi_Bidang::where('IdKpi', '=', $request['IdKpi'])
			->where('KBTerm', '=', $request['IdTerm'])->first();

		if($data == null){
			return redirect()->action('KPIBidangController@isi', ['kpib' => $request['IdKpi']])->with('term', array($request['IdTerm'], $request['IdKpi']));
		}else{
			return redirect()->action('KPIBidangController@isi', ['kpib' => $request['IdKpi']])->with('oke', $data);
		}
	}

	
}
