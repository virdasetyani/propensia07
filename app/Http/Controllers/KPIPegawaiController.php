<?php namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use App\Kpi;
use App\Kpi_Pegawai;
use App\Pegawai;
use App\Users;
use App\Term;
use App\Dev_Plan;
use Validator;
use DB;

use Illuminate\Http\Request;

class KPIPegawaiController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}
		
	public function index()
	{
		$data = Kpi::all();
		$term = Term::all();
		Return View::make('kpip/kpip', compact('data', 'term'));
	}

	public function term()
	{
		$data = Term::all();
		Return View::make('kpip/index', compact('data'));
	} 

	public function viewKpi(Request $request)
	{
		$model = Pegawai::where('Username', '=', Auth::user()->Username)->firstOrFail();
		$kpi = Kpi::where('KPegawai', '=',$model['IdPeg'])->get();

		$arrayKPI = array();
		foreach($kpi as $singkong){
			
				$data = DB::table('kpi')->join('kpi_pegawai', 'kpi.IdKpi', '=', 'kpi_pegawai.IdKpi')
				->where('kpi.IdKpi','=', $singkong->IdKpi)->where('kpi_pegawai.deleted_at', '=', null)->get();
				$data=array_map(function($item){
              		return (array) $item; },$data);

				foreach($data as $oke){
					if($oke['KPTerm'] == $request['IdTerm']){
						array_push($arrayKPI, $oke);
					} 
				}
		}
		return redirect()->back()->with('data', $arrayKPI);
	}

	public function updateKpi(Request $request)
	{
		$model = Pegawai::where('Username', '=', Auth::user()->Username)->firstOrFail();
		$kpi = Kpi::where('KPegawai', '=',$model['IdPeg'])->get();
		$term = Term::find($request['IdTerm']);
		return redirect()->back()->with('data', $kpi);
	}

	public function viewSup(Request $request)
	{
		$model = Pegawai::where('Username', '=', Auth::user()->Username)->firstOrFail();
		$data = Pegawai::where('IdSup', '=',$model['IdPeg'])->get();

		$bawahan = array();
		/*foreach($sup as $anak){
			$kpi = Kpi_Pegawai::where('KPegawai', '=', $anak['IdPeg'])->get();
			foreach($kpi as $kpi){
				$data = Kpi::find($kpi->IdKpi);
				if($data->KTerm == $request['IdTerm'])
				{
					array_push($bawahan, $data);
				} 
			}
		}*/
		Return View::make('kpip/approve', compact('data'));
	}

	public function viewKpiSup($id)
	{
		$term = Term::all();
		$pegawai = Pegawai::where('IdPeg', '=', $id)->firstOrFail();
		$kpi = Kpi::where('KPegawai', '=', $id)->get();
		Return View::make('kpip/approvekpi', compact('pegawai', 'term', 'kpi'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		//$model = Pegawai::where('Username', '=', Auth::user()->Username)->firstOrFail();
		//$id = $model->IdPeg;
		$id = $request->all();
		$term = Term::where('IdTerm','=',$id['IdTerm'])->firstOrFail();
		Return View::make('kpip/create', compact('term'));
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$model = Pegawai::where('Username', '=', Auth::user()->Username)->firstOrFail();
		$id = $model['IdPeg'];


		for($i=0; $i<count($request['Indikator']); $i++){
			if($request['Indikator'][$i]==null ||
				$request['Target'][$i]==null || 
				$request['PersenTarget'][$i]==null){
					return redirect()->back()->with('warning','Mohon isi semua kolom yang ada!');
			}
		} 
/*
		$rules = array(
			'Indikator' => 'required',
			'Target' => 'required',
			'Aktual' => 'required',
			'PersenTarget' => 'required',
			'PersenAktual' => 'required',
			'PersenAchievement[]' => 'required');

		$validator = Validator::make($all, $rules);

		if($validator->fails()){
			//$message = $validator->message();

			return redirect()->back();
			//->withInput($request->only('Indikator', 'Target', 'Aktual', 'PersenTarget', 'PersenAktual', 'PersenAchievement'));
			//->withErrors($validator);

		}else{*/

		for($i=0; $i<count($request['Indikator']); $i++){
			$kpi = new Kpi;
			$kpi->Indikator = $request['Indikator'][$i];
			$kpi->Target = $request['Target'][$i];
			$kpi->PersenTarget = $request['PersenTarget'][$i];
			$kpi->KTerm = $request['IdTerm'];
			$kpi->KPegawai = $id;
			$kpi->save();
			
			$last = Kpi::orderby('IdKpi','desc')->first();
			$kpip = new Kpi_Pegawai;
			$kpip->IdKpi = $last['IdKpi'];
			$kpip->KPTerm = $request['IdTerm'];
			$kpip->save();

		}

		//return redirect('kpip/create')->with('message', 'KPI Berhasil Disimpan!');
		return redirect()->back()->with('message', 'KPI Pegawai Telah Berhasil Disimpan!');
	
		//return redirect()->action('KPIPegawaiController@create', compact('request'));	
		/*
		

		$last = Kpi::orderby('created_at','desc')->first();
		$kpip = new Kpi_Pegawai;
		$kpip->IdKpi = $last['IdKpi'];
		$kpip->KPegawai = $id;
		$kpip->save();
		//$ind = $request->all();
		//$model->
**/
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data = Kpi::where('IdKpi', '=', compact('id'))->firstOrFail();
		Return View::make('kpip/edit', compact('data'));
	}

	public function feedback(Request $request)
	{
		$tmp = $request->all();
		$kpi = Kpi_Pegawai::where('IdKpi', '=', $tmp['IdKpi'])
				->where('KPTerm', '=', $tmp['IdTerm'])->firstOrFail();
		$kpi->Notes = $tmp['Notes'];
		$kpi->PersenAchievement = $tmp['PersenAchievement'];
		$kpi->save();

		return redirect()->back()->with('approve', 'KPI Pegawai Telah Berhasil Diubah!');
	}

	public function approve(Request $request)
	{
		$bawahan = array();
	//	$kpi = Kpi::where('KPegawai', '=',$request['Id'])->get();
		$kpi = DB::table('kpi')->join('kpi_pegawai', 'kpi.IdKpi', '=', 'kpi_pegawai.IdKpi')
				->where('kpi.KPegawai','=', $request['Id'])
				->where('kpi_pegawai.deleted_at','=', null)
				->where('kpi.deleted_at','=', null)->get();
		$bawahan = array();
		foreach($kpi as $singkong){
			if($singkong->KTerm == $request['IdTerm'])
			{
				array_push($bawahan, $singkong);
			} 
		}

		return redirect()->back()->with('data', $bawahan);
	}
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		$kpi = Kpi::where('IdKpi', '=', $request['IdKpi'])->firstOrFail();
		$kpi->Indikator = $request['Indikator'];
		$kpi->Target = $request['Target'];
		$kpi->PersenTarget = $request['PersenTarget'];
		$kpi->save();
		return redirect()->back()->with('message', 'KPI Pegawai Berhasil Diubah!');
	}

	public function view()
	{
		$model = Pegawai::where('Username', '=', Auth::user()->Username)->firstOrFail();
		$data = Kpi::where('KPegawai', '=',$model['IdPeg'])->get();
		return View::make('kpip/update', compact('data'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$kpi = Kpi_Pegawai::find($id);
		$kpi->delete();
		return redirect('kpi/delete')->with('message', 'KPI Pegawai Berhasil Dihapus!');

	}

	public function confirm($id)
	{
		$data = DB::table('kpi')->join('kpi_pegawai', 'kpi.IdKpi', '=', 'kpi_pegawai.IdKpi')
				->join('term', 'kpi_pegawai.KPTerm', '=', 'term.IdTerm')
				->where('kpi_pegawai.IdKpiPeg','=', $id)->first();
		return View::make('kpip/confirm', compact('data'));
	}

	public function notes($peg, $id, $term)
	{
		$data = DB::table('kpi')->join('kpi_pegawai', 'kpi.IdKpi', '=', 'kpi_pegawai.IdKpi')
				->where('kpi_pegawai.IdKpi','=', $id)
				->where('kpi_pegawai.KPTerm', '=', $term)
				->where('kpi.KPegawai', '=', $peg)->first();
		$user = Pegawai::where('IdPeg', '=', $peg)->first();
		return View::make('/kpip/notes', compact('data', 'user'));
	}

	public function devplan($id)
	{
		$model = Pegawai::where('Username', '=', Auth::user()->Username)->firstOrFail();
		$IdPeg = $model['IdPeg'];
		$test = Dev_Plan::where('DTerm', '=', $id)->where('IdPeg', '=', $IdPeg)->first();
		if($test == null){
			return View::make('/kpip/devplan', compact('id'));
		}else{
			return View::make('/kpip/devplane', compact('test'));
		}
		
	}

	public function storeDevplan(Request $request)
	{
		$idT = $request['IdTerm'];
		$model = Pegawai::where('Username', '=', Auth::user()->Username)->firstOrFail();
		$idP = $model['IdPeg'];
		$test = Dev_Plan::where('DTerm', '=', $idT)->where('IdPeg', '=', $idP)->get();
		$dev = new Dev_Plan;
		$dev->IdPeg = $idP;
		$dev->DTerm = $idT;
		$dev->PlanPegawai = $request['PlanPegawai'];
		$dev->PlanSupervisi = $request['PlanSuper'];
		$dev->save();
		return redirect()->back()->with('devplan', 'Rencana Pengembangan Berhasil Disimpan!');
	}


	public function editDevplan(Request $request)
	{
		$oke = $request->all();
		$dev = Dev_Plan::find($oke['Id']);
		$dev->PlanPegawai = $oke['PlanPegawai'];
		$dev->save();
		
		$test = $dev;
		return redirect()->back()->with('devplan', 'Rencana Pengembangan Berhasil Diubah!');
		//return View::make('kpip/devplane', compact('test'))->with('devplan', 'Development Plan Berhasil Diubah!');
		
	}

	public function devSup(Request $request)
	{
		$test = Dev_Plan::where('DTerm', '=', $request['IdTerm'])->where('IdPeg', '=', $request['Id'])->get();
		$peg = Pegawai::where('IdPeg', '=', $request['Id'])->first();
		if($test->isEmpty()){
			$test = array('DTerm'=>$request['IdTerm'],'IdPeg'=>$request['Id'], 'Nama'=>$peg->Nama);
			return View::make('/kpip/devplansup', compact('test'));
		}else{
			$test = Dev_Plan::where('DTerm', '=', $request['IdTerm'])->where('IdPeg', '=', $request['Id'])->first();
			return View::make('/kpip/devplansupe', compact('test', 'peg'));
		}
	}

	public function storeDevSup(Request $request)
	{
		$idT = $request['IdTerm'];
		$idP = $request['IdPeg'];
		$test = Dev_Plan::where('DTerm', '=', $idT)->where('IdPeg', '=', $idP)->get();
		$peg = Pegawai::where('IdPeg', '=', $idP)->first();

		//$id = $test[0]['Id'];
		if($test->isEmpty()){
			$dev = new Dev_Plan;
			$dev->IdPeg = $idP;
			$dev->DTerm = $idT;
			$dev->PlanPegawai = $request['PlanPegawai'];
			$dev->PlanSupervisi = $request['PlanSuper'];
			$dev->save();
			$test = array('DTerm'=>$idT,'IdPeg'=>$idP);
			return View::make('kpip/devplansup', compact('test' , 'peg'))->with('devplan', 'Rencana Pengembangan Berhasil Disimpan!');
		}else{
			$dev = Dev_Plan::find($request['Id']);
			$dev->PlanSupervisi = $request['PlanSuper'];
			$dev->save();
			$test = $dev;
			return View::make('kpip/devplansupe', compact('test', 'peg'))->with('devplan', 'Rencana Pengembangan Berhasil Diubah!');
		}
	}

	public function viewDevplan(Request $request)
	{
		$model = Pegawai::where('Username', '=', Auth::user()->Username)->firstOrFail();
		$IdPeg = $model['IdPeg'];
		$dev = Dev_Plan::where('IdPeg','=',$IdPeg)->where('DTerm','=',$request['IdTerm'])->get();
		$term = Term::where('IdTerm', '=', $request['IdTerm'])->firstOrFail();
		if ($dev->isEmpty()){
			return View::make('kpip/viewdevplan2', compact('term'));
		}else{
			$dev = Dev_Plan::where('IdPeg','=',$IdPeg)->where('DTerm','=',$request['IdTerm'])->firstOrFail();
			return View::make('kpip/viewdevplan', compact('dev', 'term'));
		}
	}

	public function isi($id)
	{
		$data = Kpi::where('IdKpi', '=', compact('id'))->firstOrFail();
		$term = Term::where('IdTerm' , '>=', $data->KTerm)->get();
		Return View::make('kpip/isi', compact('data', 'term'));
	}

	public function termIsi(Request $request)
	{
		$data = Kpi_Pegawai::where('IdKpi', '=', $request['IdKpi'])
			->where('KPTerm', '=', $request['IdTerm'])->first();

		if($data == null){
			return redirect()->action('KPIPegawaiController@isi', ['kpip' => $request['IdKpi']])->with('term', array($request['IdTerm'], $request['IdKpi']));
		}else{
			return redirect()->action('KPIPegawaiController@isi', ['kpip' => $request['IdKpi']])->with('oke', $data);
		}
	}

	public function updateIsi(Request $request){
		$data = Kpi_Pegawai::where('IdKpi', '=', $request['IdKpi'])
			->where('KPTerm', '=', $request['IdTerm'])->first();
			var_dump($data);
		if($data == null){
			$kpip = new Kpi_Pegawai;
			$kpip->IdKpi = $request['IdKpi'];
			$kpip->KPTerm = $request['IdTerm'];
			$kpip->Aktual = $request['Aktual'];
			$kpip->PersenAktual = $request['PersenAktual'];
			$kpip->save();
			return redirect()->action('KPIPegawaiController@isi', ['kpip' => $request['IdKpi']])->with('message', 'KPI Pegawai Berhasil Diisi!');
		}else{
			$data->Aktual = $request['Aktual'];
			$data->PersenAktual = $request['PersenAktual'];
			$data->save();
			return redirect()->action('KPIPegawaiController@isi', ['kpip' => $request['IdKpi']])->with('message', 'KPI Pegawai Berhasil Diubah!');
		}
	}

	public function approveInd($id){
		$Id = $id;
		$kpi = Kpi::find($id);
		if($kpi->IsApproved == 1){
			$kpi->IsApproved = false;
		}else{
			$kpi->IsApproved = true;
		}
		$kpi->save();
		return redirect()->back();
	}

	public function hapus(){
		$term = Term::all();
		return View::make('kpip/delete', compact('data', 'term'));
	}

	public function hapusKPI(Request $request){
		$model = Pegawai::where('Username', '=', Auth::user()->Username)->firstOrFail();
		$kpi = Kpi::where('KPegawai', '=',$model['IdPeg'])->get();

		$arrayKPI = array();
		foreach($kpi as $singkong){
			
				$data = DB::table('kpi')->join('kpi_pegawai', 'kpi.IdKpi', '=', 'kpi_pegawai.IdKpi')
				->where('kpi.IdKpi','=', $singkong->IdKpi)
				->where('kpi_pegawai.deleted_at', '=', null)->get();
				$data=array_map(function($item){
              		return (array) $item; },$data);

				foreach($data as $oke){
					if($oke['KPTerm'] == $request['IdTerm']){
						array_push($arrayKPI, $oke);
					} 
				}
		}
		return redirect()->back()->with('data', $arrayKPI);
	}

    public function confirmInd($id){
        $data = Kpi::find($id);
        return View::make('kpip/confirmind', compact('data'));
    }

    public function destroyInd($id){
        $kpi = Kpi::find($id);
        $kpi->delete();
        return redirect('kpi/view')->with('message', 'KPI Pegawai Berhasil Dihapus!');
    }

    public function approveKpip($id){
    	$data = Kpi_Pegawai::find($id);
    	if($data->IsDone == 1){
    		$data->IsDone = false;
    	}else{
    		$data->IsDone = true;
    	}
    	$data->save();
    	return redirect()->back();
    }

}

