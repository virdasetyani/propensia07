<?php namespace App\Http\Controllers;

use App\Http\Requests;
//use Illuminate\Support\Facades\Request;
//use App\Http\Controllers\Controller;
use App\Laporan;
use App\Term;
use App\Regional;
use View;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Http\Request;
use DB;
use Illuminate\Routing\Redirector;
use Illuminate\Pagination\Paginator;
use File;
use Illuminate\Contracts\Routing\ResponseFactory;
use Validator;

class LaporanController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('pimpinan', ['only' => ['create', 'store', 'edit', 'update', 'confirm', 'destroy' ]]);
		$this->middleware('admin', ['only' => ['create', 'store', 'edit', 'update', 'confirm', 'destroy' ]]);
		$this->middleware('super_admin', ['only' => ['create', 'store', 'edit', 'update', 'confirm', 'destroy' ]]);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$lap = DB::table('laporan')->join('regional', 'laporan.LReg', '=', 'regional.IdReg')
			->join('term', 'laporan.LTerm', '=', 'term.IdTerm')
			->where('laporan.deleted_at','=', null)
			->orderby('IdLap', 'desc')->paginate(5);
		$lap->setPath('laporan');
		//$lap = Laporan::orderby('IdLap', 'desc')->get();
		Return View::make('laporan/laporan', compact('lap'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$term = Term::all();
		$reg = Regional::all();
		Return View::make('laporan/create', compact('term', 'reg'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$data = $request->all();

		$rules = array(
			'Aktivitas' => 'required',
			'Deskripsi' => 'required',
			'Target' => 'required',
			'Aktual' => 'required',
			'PersenTarget' => 'required',
			'PersenAktual' => 'required',
			);

		$messages = array(
       		'required' => 'Harus diisi!'
    	);

		$validator= Validator::make($data, $rules, $messages);

		if($validator->fails()){
			$messages = $validator->messages();

			return redirect('/laporan/create')
				->withInput($request->only('Aktivitas', 'Deskripsi', 'Target', 'Aktual', 'PersenTarget', 'PersenAktual'))
				->withErrors($validator);
		}
		else{

			$lap = new Laporan;
			$lap->LTerm = $request['IdTerm'];
			$lap->LReg = $request['IdReg'];
			$lap->Aktivitas = $request['Aktivitas'];
			$lap->Deskripsi = $request['Deskripsi'];
			$lap->Target = $request['Target'];
			$lap->Aktual = $request['Aktual'];
			$lap->PersenTarget = $request['PersenTarget'];
			$lap->PersenAktual = $request['PersenAktual'];
			$lap->save();

	  		$laporan = Laporan::orderby('IdLap', 'desc')->first();
	  		$id = $laporan->IdLap;
			$destination = public_path() . '/upload/laporan/';
			$file = $request->file('file');
			$ext = $file->getClientOriginalExtension();
			$filename = $id . '.' . $ext;
	    	$file->move($destination, $filename);
	    	$lap->Attachment = '/upload/laporan/' . $filename;
	    	$lap->save();

	    	return redirect()->action('LaporanController@show', ['laporan' => $lap->IdLap])->with('message', 'Laporan Kegiatan Berhasil Disimpan!');
    		//return redirect('laporan/detail')->with('message', 'Laporan Kegiatan Berhasil Disimpan!')->with('lap', $lap);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$lap = DB::table('laporan')->join('regional', 'laporan.LReg', '=', 'regional.IdReg')
			->join('term', 'laporan.LTerm', '=', 'term.IdTerm')
			->where('laporan.deleted_at','=', null)
			->where('IdLap', '=', compact('id'))->first();
		Return View::make('laporan/detail', compact('lap'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$lap = Laporan::find($id);
		$term = Term::all();
		$reg = Regional::all();
		Return View::make('laporan/edit', compact('lap', 'term', 'reg'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		$data = $request->all();

		$rules = array(
			'Aktivitas' => 'required',
			'Deskripsi' => 'required',
			'Target' => 'required',
			'Aktual' => 'required',
			'PersenTarget' => 'required',
			'PersenAktual' => 'required',
			);

		$messages = array(
       		'required' => 'Harus diisi!'
    	);

		$validator= Validator::make($data, $rules, $messages);

		if($validator->fails()){
			$messages = $validator->messages();

			return redirect()->back()
				->withInput($request->only('Aktivitas', 'Deskripsi', 'Target', 'Aktual', 'PersenTarget', 'PersenAktual'))
				->withErrors($validator);
		}
		else{

			$lap = Laporan::where('IdLap','=',$request['IdLap'])->firstOrFail();
			$lap->LTerm = $request['IdTerm'];
			$lap->LReg = $request['IdReg'];
			$lap->Aktivitas = $request['Aktivitas'];
			$lap->Deskripsi = $request['Deskripsi'];
			$lap->Target = $request['Target'];
			$lap->Aktual = $request['Aktual'];
			$lap->PersenTarget = $request['PersenTarget'];
			$lap->PersenAktual = $request['PersenAktual'];
			$lap->save();

			if($request->file('file') != null){
				\File::Delete(public_path() . $lap->Attachment);
				$destination = public_path() . '/upload/laporan/';
				$file = $request->file('file');
				$ext = $file->getClientOriginalExtension();
				$filename = $request['IdLap'] . '.' . $ext;
    			$file->move($destination, $filename);	
    			$lap->Attachment = '/upload/laporan/' . $filename;
    			$lap->save();
			}

			return redirect()->action('LaporanController@show', ['laporan' => $lap->IdLap])->with('message', 'Laporan Kegiatan Berhasil Diubah!');
		
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$lap = Laporan::find($id);
		$lap->delete();
		return redirect()->action('LaporanController@index')->with('delete', 'Laporan Kegiatan Berhasil Dihapus');
	}

	public function confirm($id)
	{
		$lap = Laporan::find($id);
		Return View::make('laporan/confirm', compact('lap'));
	}

	public function download($id)
	{
		$lap = Laporan::find($id);
		$path = public_path() . $lap->Attachment;
		return response()->download($path);
	}

}
