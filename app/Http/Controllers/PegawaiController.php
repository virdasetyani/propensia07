<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use App\Pegawai;
use App\Users;
use App\Regional;
use App\Bidang;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Hash;

class PegawaiController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data = Pegawai::where('Username', '=', Auth::user()->Username)->firstOrFail();
		$sup = Pegawai::where('IdPeg' , '=', $data->IdSup)->firstOrFail();
		//echo $data->IdPeg;
		Return View::make('pegawai/pegawai', compact('data', 'sup'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit()
	{
		$oke = Pegawai::where('Username', '=', Auth::user()->Username)->firstOrFail();
		$all = Pegawai::orderby('Nama')->get();
		$sup = array();
		$supa = Pegawai::where('IdPeg','=', $oke->IdSup)->first();
			if(!$supa == null){
				$waw = Pegawai::find($supa->Username);
				array_push($sup, $waw);

			}
		Return View::make('pegawai/edit', compact('oke', 'all', 'sup'));
	}

	public function confirm($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		$nama = $request->all();
		$rules = array(
			'Nama' => 'required',
			//'IdPeg' => 'required|unique:pegawai',
			'Email' => 'email');
		$validator= Validator::make($nama, $rules);

		if($validator->fails()){

			$messages = $validator->messages();
			return redirect()->back()
				->withInput($request->only('Nama'))
			//	->withInput($request->only('IdPeg'))
				->withInput($request->only('Email'))
				->withErrors($validator);
		} else{
		$peg = Pegawai::where('Username', '=', Auth::user()->Username)->firstOrFail();
		$peg->Nama = $request['Nama'];
		$peg->IdPeg = $request['IdPeg'];
		$peg->Gender = $request['Gender'];
		$peg->Alamat = $request['Alamat'];
		$peg->NoTelp = $request['NoTelp'];
		$peg->Email = $request['Email'];
		if($nama['IdSup'] == "0"){
			$peg->IdSup = null;
		}else{
			$peg->IdSup = $request['IdSup'];
		}
		
		$peg->save();

		return redirect()->back()->with('message', 'Profile Anda Berhasil Disimpan!');
		}
		/*$tmp = $request->all();
		$pegawai = Pegawai::where('IdPeg', '=', $tmp['id'])->firstOrFail();
		$pegawai->IdPeg = $tmp['IdPeg'];
		$pegawai->save();
		Return redirect()->action('PegawaiController@index');*/
		//$data = Regional::where('IdReg', '=', 'id')->firstOrFail();
		//Return View::make('regional/edit');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function editpass(){
		$data = Pegawai::where('Username','=',Auth::user()->Username)->firstOrFail();
		Return View::make('pegawai/password', compact('data'));
	}

	public function savepass(Request $request){
		$nama = $request->all();
		$rules = array(
			'oldpassword' => 'required',
			'password' => 'required',
			'password_confirmation' => 'required');

		$validator= Validator::make($nama, $rules);

		if($validator->fails()){

			$messages = $validator->messages();
			return redirect()->back()
				->withInput($request->only('oldpassword'))
				->withInput($request->only('password'))
				->withInput($request->only('password_confirmation'))
				->withErrors($validator);
		}

		$data = Users::where('Username','=',Auth::user()->Username)->firstOrFail();
		$oldpass = $data['password'];
		if(Hash::check($nama['oldpassword'], $oldpass)){
			$data->password = bcrypt($nama['password']);
			$data->save();
			return redirect()->back()->with('message', 'The new password is updated');
		}else{
			return redirect()->back()->with('old', 'The old password does not match');
		}
	}

	public function editFoto($id){
		$peg = Pegawai::find($id);
		Return View::make('pegawai/foto', compact('peg'));
	}

	public function saveFoto(Request $request){
		$peg = Pegawai::where('Username','=',Auth::user()->Username)->firstOrFail();
	  	$id = $peg->IdPeg;
		$destination = public_path() . '/upload/pegawai/';
		$file = $request->file('file');
		$ext = $file->getClientOriginalExtension();
		$filename = $id . '.' . $ext;
	    $file->move($destination, $filename);
	    $peg->Foto = '/upload/pegawai/' . $filename;
	    $peg->save();
	    return redirect()->action('PegawaiController@editFoto', ['pegawai' => $peg->Username])->with('message', 'Foto berhasil diubah!');
	}
}
