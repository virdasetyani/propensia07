<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use App\Regional;
use App\Bidang;
use App\Regional_Bidang;
use Validator;




use Illuminate\Http\Request;

class RegController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('super_admin', ['only' => ['create', 'store', 'edit','update','confirm', 'destroy','index' ]]);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data = Regional::all();
		Return View::make('regional/regional', compact('data'));
		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data = Bidang::all();
		return View::make('regional/create', compact('data'));
	
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)

	{
		$nama = $request->all();

		$rules = array(
			'NamaReg' => 'required|unique:regional');


		$validator= Validator::make($nama, $rules);

		if($validator->fails()){
			$messages = $validator->messages();

			return redirect('/regional/create')
				->withInput($request->only('NamaReg'))
				->withErrors($validator);
		}
		else{
			$reg = new Regional;
			$reg->NamaReg = $request['NamaReg'];

			$bid = $request['NamaBid'];

			if($bid==null){
				return redirect()->back()->with('message', 'Regional Baru gagal dibuat, Bidang harus diisi');
			} 
			else{

				$reg->save();
				$id = Regional::orderby('IdReg', 'desc')->first();
				foreach($bid as $b){
					
					$regbid = new Regional_Bidang;
					$regbid->Reg = $id['IdReg'];
					$regbid->Bid = $b;
					$regbid->save();
					
				}
				
				return redirect()->back()->with('message', 'Regional Baru Berhasil Disimpan!');		
			}
		}
	}
	//session//
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{	
	
		$data = Regional::where('IdReg', '=', compact('id'))->firstOrFail();
		$bidang = Regional_Bidang::where('Reg', '=', $data->IdReg)->get();		
		$coba = Bidang:: all();
		


		$oke = array();
		foreach ($bidang as $b ) {
			$j = Bidang::find($b->Bid);
			array_push($oke, $j);
		}

		Return View::make('regional/edit', compact('data','oke','coba'));
		
	}

	public function confirm($id)
	{
		$data = Regional::where('IdReg', '=', compact('id'))->firstOrFail();
		//return redirect()->back()->with('message', 'Regional Baru Telah Berhasil Disimpan		
		return View::make('regional/confirm', compact('data'));
		
	}
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{

		$tmp = $request->all();
		$reg = Regional::where('IdReg', '=', $tmp['id'])->firstOrFail();

		$rules = array(
			'NamaReg' => 'required');


		$validator= Validator::make($tmp, $rules);

		if($validator->fails()){
			$messages = $validator->messages();

			return redirect()->back()->with('message', 'Nama Regional harus diisi')
				->withInput($request->only('NamaReg'))
				->withErrors($validator);

		}
		else 
		{
		$reg->NamaReg = $tmp['NamaReg'];
		$reg->save();

		$bidang = Regional_Bidang::where('Reg', '=', $reg->IdReg)->get();		
		$coba = Bidang:: all();

		
		$s = $tmp['NamaBid'];
		
		$oke = array();
		foreach ($bidang as $b ) {
			$j = Bidang::find($b->Bid);
			array_push($oke, $j);
		}

		foreach($coba as $c){

			$dulu = in_array($c, $oke);
			$sekarang = in_array($c->IdBid, $s);
			if ($dulu && !$sekarang){
				$bid = Regional_Bidang::where('Reg', '=', $reg->IdReg)-> where ('Bid', '=', $c->IdBid) ->firstOrFail();
				$bid->delete();

			}
			//dulu ada, sekarang ada, abaikan
			//dulu ga ada, sekarang ga ada, abaikan
			if (!$dulu && $sekarang){
				$regbid = new Regional_Bidang;
				$regbid->Reg = $reg->IdReg;
				$regbid->Bid=$c->IdBid;
				$regbid->save(); 
			}
			//dulu ada, sekarang ga ada, apus
			//dulu ga ada, sekarang ada, tambah
			
		}

		return redirect()->back()->with('message', 'Data Regional berhasil diubah ');	
			}

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$reg = Regional::where('IdReg', '=', compact('id'))->firstOrFail();
		$reg->delete();
		$regbidang = Regional_Bidang::where('Reg', '=', compact('id'));
		$regbidang->delete();
		return redirect('/regional')->with('message','Regional Berhasil Dihapus!');
			
	}

}

