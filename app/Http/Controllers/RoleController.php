<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use App\Role;


use Illuminate\Http\Request;

class RoleController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data = Role::all();
		Return View::make('role/role', compact('data'));
		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('role/create');

	
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		

		$nama = $request->all();
		$role = new Role;
		$role->Nama = $nama['Nama'];
		$role->save();
		return redirect()->back()->with('message', 'Role Baru Berhasil Disimpan!');	
		//return redirect()->action('BidangController@index');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data = Role::where('IdRole', '=', compact('id'))->firstOrFail();
		Return View::make('role/edit', compact('data'));
	}

	public function confirm($id)
	{
		$data = Role::where('IdRole', '=', compact('id'))->firstOrFail();
		return View::make('role/confirm', compact('data'));
	}
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		$tmp = $request->all();
		$role = Role::where('IdRole', '=', $tmp['id'])->firstOrFail();
		$role->Nama = $tmp['Nama'];
		$role->save();
		return redirect()->action('RoleController@index');	
		
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$role = Role::where('IdRole', '=', compact('id'))->firstOrFail();
		$role->delete();
		//return redirect()->back()->with('message', 'Bidang Baru Berhasil Disimpan!');
		return redirect()->action('RoleController@index');
		// redirect()->with('message', 'Bidang Baru Berhasil Disimpan!');
	}

}
