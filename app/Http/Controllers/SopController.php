<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use App\Sop;
use App\Bidang;
use Validator;
use Illuminate\Http\Request;
use File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
//use Symfony\Component\HttpFoundation\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Pagination\Paginator;
use Illuminate\Contracts\Routing\ResponseFactory;


class SopController extends Controller {

public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('admin', ['only' => ['create', 'store', 'confirm', 'destroy','index' ]]);
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{	
		$bidang = Bidang::all();
		$data = Sop::all();
		Return View::make('Sop/upload', compact('bidang','data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

		$bidang = Bidang::all();
		Return View::make('Sop/create', compact('bidang'));

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		
		$nama = $request->all();
		$rules = array(
			'Kode' => 'required|unique:Sop',
			'Judul' => 'required|unique:Sop'
			);


		$validator= Validator::make($nama, $rules);

		if($validator->fails()){
			$messages = $validator->messages();

			return redirect('/Sop/create')
				->withInput($request->only('Kode', 'Judul'))
				->withErrors($validator);
		}
		else{



		$sop = new Sop;
		$sop->SBid = $request['IdBid'];
		$sop->Judul = $request['Judul'];
		$sop->Kode = $request['Kode'];
	
		$destination = public_path() . '/upload/Sop/';
		$file = $request->file('file');
		$ext = $file->getClientOriginalExtension();
		$filename = $request['Kode'].'-'.$file->getClientOriginalName();
    	$file->move($destination, $filename);
    	$sop->File = '/upload/Sop/' . $filename;
    	$sop->save(); 

		return redirect()->back()->with('message', 'Dokumen SOP berhasil ditambahkan!');	
		}	

	}

	public function view ( Request $request )
	{	

		$sop = Sop::where ('SBid', '=', $request ['IdBid'])->get();
		return redirect('/Sop')->with('sop',$sop);
		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function confirm($id)
	{
		$data = Sop::where('Kode', '=', compact('id'))->firstOrFail();
		return View::make('Sop/confirm', compact('data'));
		
	}
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$sop = Sop::where('Kode', '=', compact('id'))->firstOrFail();
		$sop->delete();
		return redirect('/Sop')->with('message','Sop Berhasil Dihapus!');
		
	}

	public function download($id)
	{
		$sop = Sop::find($id);
		$path = public_path() . $sop->File;
		return response()->download($path);
	}

}
