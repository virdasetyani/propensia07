<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Term;
use View;
use Validator;

use Illuminate\Http\Request;

class TermController extends Controller {


	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('super_admin', ['only' => ['create', 'index' ]]);
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data = Term::all();
		Return View::make('term/term', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		Return View::make('term/create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$nama = $request->all();

		$rules = array(
			'Tahun' => 'required');

		$messages = array(
       		'required' => 'Harus diisi!'
    	);

		$validator= Validator::make($nama, $rules, $messages);

		if($validator->fails()){
			$messages = $validator->messages();

			return redirect('/term/create')
				->withInput($request->only(''))
				->withErrors($validator);
		}
		else
		{

		$term = new Term;
		$term->Bulan = $nama['Bulan'];
		$term->Tahun = $nama['Tahun'];
		$term->save();
		return redirect()->back()->with('message', 'Term Baru Berhasil Disimpan!');	
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
