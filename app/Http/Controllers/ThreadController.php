<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use App\Thread;
use App\Forum;
use App\Comment;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Pegawai;

class ThreadController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('admin', ['only' => ['confirm', 'destroy' ]]);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$data = Thread::all();
		Return View::make('thread/thread', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($id)
	{
		$for = Forum::where('IdForum', '=', compact('id'))->first();
		if($for == null){
			$forum = null;
		}
		else{
			$forum = $id;			
		}
		return View::make('thread/create', compact('forum'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		
		$peg = Pegawai::where('Username', '=', Auth::user()->Username)->firstOrFail();

		$nama = $request->all();
		$rules = array(
			'Judul' => 'required',
			'Isi' => 'required');

		$messages = array(
       		'required' => 'Harus diisi!'
    	);

		$validator= Validator::make($nama, $rules, $messages);

		if($validator->fails()){
			$messages = $validator->messages();

			return redirect('/thrd/buat/'.$nama['IdForum'])
				->withInput($request->only('Judul', 'Isi'))
				->withErrors($validator);
		}
		else{
			date_default_timezone_set('Asia/Jakarta');
			$tanggal= mktime(date("m"),date("d"),date("Y"));
			$jam = mktime(date("H"), date("i"), date("s"));
			$tglsekarang = date("Y-m-d", $tanggal);
			$jamsekarang = date("H:i:s", $jam);

			$thread = new Thread;
			$thread->Judul = $nama['Judul'];
			$thread->Isi = $nama['Isi'];
			$thread->IdFor = $nama['IdForum'];
			$thread->TUser = $peg['Nama'];
			$thread->Tanggal = $tglsekarang;
			$thread->Jam = $jamsekarang;
			$thread->save();
			return redirect()->action('ThreadController@show', $nama['IdForum']);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$forum = Forum::where('IdForum', '=', compact('id'))->firstOrFail();
		$thrd = Thread::orderby('IdThread','desc')->where('IdFor','=', compact('id'))->get();
		Return View::make('thread/thread', compact('forum', 'thrd'));
	}

	public function isi($id)
	{
		$content = Thread::where('IdThread', '=', compact('id'))->firstOrFail();
		$komentar = Comment::where('IdPost', '=', compact('id'))->get();
		
		Return View::make('thread/view', compact('content' , 'komentar'));
	}
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function confirm($id)
	{
		$thrd = Thread::where('IdThread', '=', compact('id'))->first();
		if($thrd == null){
			$data = null;
		}
		else{
			$data = $thrd;			
		}
		return View::make('thread/confirm', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$thread = Thread::where('IdThread', '=', compact('id'))->firstOrFail();
		$idF = $thread['IdFor'];
		$comment = Comment::where('IdPost', '=', compact('id'))->get();
		foreach($comment as $c){
			$c->delete();
		}
		$thread->delete();
		return redirect()->action('ThreadController@show', $idF)->with('message', 'Thread successfully deleted.');
	}

	public function comment(Request $request)
	{
		$com = $request->all();
		$peg = Pegawai::where('Username', '=', Auth::user()->Username)->firstOrFail();

		$rules = array(
			'Comment' => 'required');

		$messages = array(
       		'required' => 'Harus diisi!'
    	);

		$validator= Validator::make($com, $rules, $messages);

		if($validator->fails()){
			$messages = $validator->messages();

			return redirect('/thrd/'.$com['IdThread'])
				->withInput($request->only('Comment'))
				->withErrors($validator);
		}
		
		else{
			date_default_timezone_set('Asia/Jakarta');
			$tanggal= mktime(date("m"),date("d"),date("Y"));
			$jam = mktime(date("H"), date("i"), date("s"));
			$tglsekarang = date("Y-m-d", $tanggal);
			$jamsekarang = date("H:i:s", $jam);

			$komentar = new Comment;
			$komentar->Isi = $com['Comment'];
			$komentar->IdPost = $com['IdThread'];
			$komentar->CUser = $peg['Nama'];
			$komentar->Tanggal = $tglsekarang;
			$komentar->Jam = $jamsekarang;
			$komentar->save();
			return redirect()->action('ThreadController@isi', $com['IdThread']);;
		}

	}

}
