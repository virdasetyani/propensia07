<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use App\Users;
use App\Regional;
use App\Bidang;
use App\Regional_Bidang;
use App\Role;
use Illuminate\Http\Request;
use DB;
use App\Pegawai;
use Validator;

class UserAccountController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		
		$data = DB::table('user')->join('regional_bidang', 'user.Uregbid', '=', 'regional_bidang.IdRegBid')
				->join('bidang','regional_bidang.Bid','=', 'bidang.IdBid')
				->join('regional', 'regional_bidang.Reg','=', 'regional.IdReg')
				->join('role', 'user.Urole','=', 'role.IdRole')
				->join('pegawai', 'user.Username', '=', 'pegawai.Username')
				->where('user.deleted_at','=', null)->get();
					
		Return View::make('accountUser/userAccount', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$dataReg = Regional::all();
		$dataBid = Bidang::all();
		$dataRole = Role::all();
		Return View::make('accountUser/create', compact('dataReg', 'dataBid', 'dataRole'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		
		$nama = $request->all();
		$rules = array(
			'Username' => 'required|unique:user',
			'IdPeg' => 'required|unique:pegawai',
			'password' => 'required|confirmed|min:6');
		$validator= Validator::make($nama, $rules);

		if($validator->fails()){

			$messages = $validator->messages();
			return redirect('/accountUser/create')
				->withInput($request->only('Username'))
				->withInput($request->only('IdPeg'))
				->withInput($request->only('password'))
				->withErrors($validator);
		} else{
		$user = new Users;
		$user->Username = $request['Username'];
		$user->password = bcrypt($request['password']);
		$user->Urole = $request['Urole'];
		$user->Uregbid = $request['Uregbid'];
		$user->save();

		
		$pegawai = new Pegawai;
		$pegawai->Username = $request['Username'];
		$pegawai->IdPeg = $request['IdPeg'];
		$pegawai->save();
		Return redirect()->action('UserAccountController@index')->with('message', 'Akun Berhasi Disimpan');
		}

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data = DB::table('user')->join('regional_bidang', 'user.Uregbid', '=', 'regional_bidang.IdRegBid')
				->join('bidang','regional_bidang.Bid','=', 'bidang.IdBid')
				->join('regional', 'regional_bidang.Reg','=', 'regional.IdReg')
				->join('role', 'user.Urole','=', 'role.IdRole')
				->join('pegawai', 'user.Username', '=', 'pegawai.Username')
				->where('user.deleted_at','=', null)
			->where('user.Username', '=', compact('id'))->first();
		//$data = Users::where('Username', '=', compact('id'))->firstOrFail();
		$dataReg = Regional::all();
		$dataBid = Bidang::all();
		$dataRole = Role::all();
		$dataIdPeg = Pegawai::all();

		//echo var_dump($data);
		/*$oke = array();
		$lala = Bidang::find($data->Ubid);
		array_push($oke, $lala);

		$oke2 = array();
		$apa = Regional::find($data->Ureg);
		array_push($oke2, $apa);*/

		$oke3 = array();
		$lolo = Role::find($data->Urole);
		array_push($oke3, $lolo);

		Return View::make('accountUser/edit', compact('dataIdPeg', 'data', 'dataReg', 'dataBid', 'dataRole', 'oke', 'oke2', 'oke3'));
	}

	public function confirm($id)
	{
		$data = Users::where('Username', '=', compact('id'))->firstOrFail();
		Return View::make('accountUser/confirm', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		$tmp = $request->all();
		$user = Users::where('id', '=', $tmp['id'])->firstOrFail();
		$user->Username = $tmp['Username'];
	//	$user->Ureg = $tmp['Ureg'];
	//	$user->Ubid = $tmp['Ubid'];
		$user->Urole = $tmp['Urole'];
		$user->save();
		Return redirect()->action('UserAccountController@index');
		//$data = Regional::where('IdReg', '=', 'id')->firstOrFail();
		//Return View::make('regional/edit');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$userAccount = Users::where('Username', '=', compact('id'))->firstOrFail();
		$userAccount->delete();
		$peg = Pegawai::find($id);
		$peg->delete();
		Return redirect()->action('UserAccountController@index');
	}

	public function editpass($id){
		$data = Users::where('Username', '=', compact('id'))->firstOrFail();
		Return View::make('accountUser/password', compact('data'));
	}

	public function savepass(Request $request){
		$nama = $request->all();
		$rules = array(
			'password' => 'required|confirmed|min:6');
		$validator= Validator::make($nama, $rules);

		if($validator->fails()){

			$messages = $validator->messages();
			return redirect()->back()
				->withInput($request->only('password'))
				->withErrors($validator);
		} else{
			$user = Users::where('Username', '=', $nama['Username'])->firstOrFail();
			$user->password = bcrypt($nama['password']);
			$user->save();
			return redirect()->back()->with('message', 'Password berhasil diubah!');
		}
		
	}

	public function regional(Request $request){
		//$regbid = Regional_Bidang::where('Reg','=' $request['Ureg'])->get();
		$bid = DB::table('regional_bidang')->join('bidang', 'regional_bidang.Bid', '=', 'bidang.IdBid')
			->where('regional_bidang.Reg','=', $request['Ureg'])->get();
		return redirect()->back()->with('bid', $bid);
	}

	public function bidang(Request $request){
		$regbid = DB::table('regional_bidang')->join('bidang', 'regional_bidang.Bid', '=', 'bidang.IdBid')
					->join('regional', 'regional_bidang.Reg', '=', 'regional.IdReg')
					->where('regional_bidang.Reg','=', $request['Ureg'])
					->where('regional_bidang.Bid', '=', $request['Ubid'])->first();
		$dataRole = Role::all();
		return View::make('accountUser/new', compact('regbid', 'dataRole'));
	}

	public function regbid($id){
		$data = Users::where('Username', '=', $id)->first();
		$reg = Regional::all();
		$regbid = Regional_Bidang::where('IdRegBid', '=', $data->Uregbid);
		return View::make('accountUser/regbid', compact('data', 'reg', 'regbid'));
	}

	public function regional1(Request $request){
		$bid = DB::table('regional_bidang')->join('bidang', 'regional_bidang.Bid', '=', 'bidang.IdBid')
			->where('regional_bidang.Reg','=', $request['Ureg'])->get();
		return redirect()->back()->with('bid', $bid);
	}

	public function saveregbid(Request $request){
		$user = Users::where('Username', '=', $request['username'])->first();
		$regbid = Regional_Bidang::where('Reg','=', $request['Ureg'])
					->where('Bid', '=', $request['Ubid'])->first();
		$user->Uregbid = $regbid->IdRegBid;
		$user->save();
		return redirect()->back()->with('message', 'Regional-Bidang pengguna berhasil diubah');
	}

}
