<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');
Route::get('/welcome', 'WelcomeController@home');
Route::get('home', 'HomeController@index');

//Route::get('/profile/edit/{user}', 'PegawaiContoller@store');
Route::get('/profile/view/', 'PegawaiController@index');
Route::get('/profile/edit/', 'PegawaiController@edit');
Route::post('/profile/edit/', 'PegawaiController@update');
Route::get('/profile/edit/foto/{pegawai}', 'PegawaiController@editFoto');
Route::post('/profile/edit/foto', 'PegawaiController@saveFoto');
Route::get('/profile/password/change', 'PegawaiController@editpass');
Route::post('/profile/password/change', 'PegawaiController@savepass');

Route::resource('accountUser', 'UserAccountController');
Route::post('/accountUser/create', 'UserAccountController@store');
Route::post('/accountUser/edit', 'UserAccountController@update');
Route::get('/accountUser/delete/{userAccount}', 'UserAccountController@destroy');
Route::get('/accountUser/confirm/{userAccount}', 'UserAccountController@confirm');
Route::get('/accountUser/password/{userAccount}', 'UserAccountController@editpass');
Route::post('/accountUser/password', 'UserAccountController@savepass');
Route::post('/accountUser/regional/', 'UserAccountController@regional');
Route::post('/accountUser/regional1/', 'UserAccountController@regional1');
Route::post('/accountUser/bidang/', 'UserAccountController@bidang');
Route::post('/accountUser/saveregbid/', 'UserAccountController@saveregbid');
Route::get('/accountUser/regbid/{userAccount}', 'UserAccountController@regbid');

Route::resource('role', 'RoleController');
Route::post('/role/create', 'RoleController@store');
Route::post('/role/edit', 'RoleController@update');
Route::get('/role/delete/{role}', 'RoleController@destroy');
Route::get('/role/confirm/{role}', 'RoleController@confirm');

Route::resource('bidang', 'BidangController');
Route::post('/bidang/create', 'BidangController@store');
Route::post('/bidang/edit', 'BidangController@update');
Route::get('/bidang/delete/{bidang}', 'BidangController@destroy');
Route::get('/bidang/confirm/{bidang}', 'BidangController@confirm');

Route::resource('regional', 'RegController');
Route::post('/regional/create', 'RegController@store');
Route::post('/regional/edit', 'RegController@update');
Route::get('/regional/delete/{regional}', 'RegController@destroy');
Route::get('/regional/confirm/{regional}', 'RegController@confirm');

Route::resource('forum', 'ForumController');
Route::post('/forum/create', 'ForumController@store');
Route::post('/forum/edit', 'ForumController@update');
Route::get('/forum/delete/{forum}', 'ForumController@destroy');
Route::get('/forum/confirm/{forum}', 'ForumController@confirm');

Route::resource('thread', 'ThreadController');
Route::post('/thread/create/', 'ThreadController@store');
Route::get('/thrd/buat/{forum}', 'ThreadController@create');

Route::get('/thread/delete/{thread}', 'ThreadController@destroy');
Route::get('/thread/confirm/{thread}', 'ThreadController@confirm');
Route::get('/thread/{forum}', 'ThreadController@show');
Route::get('/thrd/{thread}', 'ThreadController@isi');
Route::post('/thread/comment/', 'ThreadController@comment');

Route::resource('agenda', 'AgendaController');
Route::get('/agenda', 'AgendaController@index');
Route::post('/agenda/create', 'AgendaController@store');
Route::post('/agenda/create', 'AgendaController@store');
Route::post('/agenda/edit', 'AgendaController@update');
Route::get('/agenda/delete/{agenda}', 'AgendaController@destroy');
Route::get('/agenda/confirm/{agenda}', 'AgendaController@confirm');
Route::get('/agenda/{agenda}', 'AgendaController@show');

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

Route::resource('kpib', 'KPIBidangController');
Route::get('/kpic/term', 'KPIBidangController@term');
Route::post('/kpib/create', 'KPIBidangController@store');
Route::get('/kpic/view', 'KPIBidangController@view');
Route::get('/kpib/confirm/{kpib}', 'KPIBidangController@confirm');
Route::post('/kpib/edit', 'KPIBidangController@update');
Route::get('/kpib/delete/{kpib}', 'KPIBidangController@destroy');
Route::get('/kpib/confirm/{kpib}', 'KPIBidangController@confirm');
Route::get('/kpic/approve', 'KPIBidangController@approve');
Route::get('Kpib/approve/{kpib}', 'KPIBidangController@notes');
Route::post('/kpib/feedback/', 'KPIBidangController@feedback');
Route::post('/kpib' , 'KPIBidangController@viewKpi');
Route::post('/kpic/approve', 'KPIBidangController@setuju');
Route::get('/kpib/approve/{kpib}', 'KPIBidangController@approveBid');
//Route::post('/kpic/view', 'KPIBidangController@updateKpi');
Route::post('/kpib/view', 'KPIBidangController@lihat');
Route::get('/kpic/hapus', 'KPIBidangController@hapus');
Route::get('/kpib/hapus/{kpib}', 'KPIBidangController@destroy');
Route::get('/kpib/hapus', 'KPIBidangController@del');
Route::post('/kpib/hapus', 'KPIBidangController@del');
Route::get('/kpib/isi/{kpib}', 'KPIBidangController@isi');
Route::post('/kpib/isi', 'KPIBidangController@termIsi');
Route::post('/kpib/isi/save', 'KPIBidangController@updateIsi');




Route::resource('kpip', 'KPIPegawaiController');
Route::get('/kpi/term', 'KPIPegawaiController@term');
Route::post('/kpip/create', 'KPIPegawaiController@store');
Route::get('/kpi/view', 'KPIPegawaiController@view');
Route::post('/kpi/view', 'KPIPegawaiController@updateKpi');
Route::get('/kpip/confirm/{kpip}', 'KPIPegawaiController@confirm');
Route::post('/kpip/edit', 'KPIPegawaiController@update');
Route::get('/kpip/delete/{kpip}', 'KPIPegawaiController@destroy');
Route::get('/kpip/confirm/{kpip}', 'KPIPegawaiController@confirm');
Route::post('/kpi/approve', 'KPIPegawaiController@approve');
Route::get('kpip/approve/{pegawai}/{kpip}/{term}', 'KPIPegawaiController@notes');
Route::post('/kpi/feedback', 'KPIPegawaiController@feedback');
Route::post('/kpip' , 'KPIPegawaiController@viewKpi');
Route::get('/kpi/sup' , 'KPIPegawaiController@viewSup');
Route::post('/kpi/devplan', 'KPIPegawaiController@storeDevplan');
Route::post('/kpi/devplane', 'KPIPegawaiController@editDevplan');
Route::get('/kpi/approve/{pegawai}', 'KPIPegawaiController@viewKpiSup');
Route::post('/kpi/approve/sup', 'KPIPegawaiController@devSup');
Route::post('/kpi/devplan/sup', 'KPIPegawaiController@storeDevSup');
Route::post('/kpip/view/devplan', 'KPIPegawaiController@viewDevplan');
Route::get('/kpi/devplan/form/{term}', 'KPIPegawaiController@devplan');
Route::get('/kpip/isi/{kpip}', 'KPIPegawaiController@isi');
Route::post('/kpip/isi', 'KPIPegawaiController@termIsi');
Route::post('/kpip/isi/save', 'KPIPegawaiController@updateIsi');
Route::get('/kpip/approveInd/{kpip}', 'KPIPegawaiController@approveInd');
Route::get('/kpi/delete', 'KPIPegawaiController@hapus');
Route::post('/kpi/delete', 'KPIPegawaiController@hapusKpi');
Route::get('/kpi/confirm/{kpi}', 'KPIPegawaiController@confirmInd');
Route::get('/kpi/delete/{kpi}', 'KPIPegawaiController@destroyInd');
Route::get('/kpi/approve/peg/{kpi}', 'KPIPegawaiController@approveKpip');

Route::resource('laporan', 'LaporanController');
Route::get('/laporan/create', 'LaporanController@create');
Route::post('/laporan/create', 'LaporanController@store');
Route::get('laporan', 'LaporanController@index');
Route::get('/laporan/{laporan}', 'LaporanController@show');
Route::get('/laporan/edit/{laporan}', 'LaporanController@edit');
Route::post('/laporan/edit', 'LaporanController@update');
Route::get('/laporan/confirm/{laporan}', 'LaporanController@confirm');
Route::get('/laporan/delete/{laporan}', 'LaporanController@destroy');
Route::get('/laporan/download/{laporan}', 'LaporanController@download');

Route::resource('Sop', 'SopController');
Route::get('/Sop', 'SopController@index');
Route::post('/Sop', 'SopController@view');
Route::post('/Sop/create', 'SopController@store');
Route::post('file/upload', 'SopController@store');
Route::get('/Sop/confirm/{Sop}', 'SopController@confirm');
Route::get('/Sop/confirm/{Sop}', 'SopController@confirm');
Route::get('/Sop/upload', 'SopController@view');
Route::get('/Sop/delete/{Sop}', 'SopController@destroy');
Route::get('/Sop/download/{Sop}', 'SopController@download');

Route::resource('absensi', 'AbsensiController');
Route::post('/absensi', 'AbsensiController@store');
Route::get('/view', 'AbsensiController@show');
Route::post('/viewDetail', 'AbsensiController@showDetail');
Route::post('/viewMoreDetail', 'AbsensiController@showMoreDetail');
Route::get('/approvalAbsensi', 'AbsensiController@showApproval');
Route::post('/approve', 'AbsensiController@approve');

Route::get('/term/create', 'TermController@create');
Route::post('/term/create', 'TermController@store');
Route::get('/term', 'TermController@index');