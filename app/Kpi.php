<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Kpi extends Model {

	use SoftDeletes;

	protected $table='kpi';

	public $timestamps = false;

	protected $fillable = ['Indikator', 'Target', 'Aktual', 'PersenTarget', 'PersenAktual'];

	protected $primaryKey = 'IdKpi';

	protected $dates = ['deleted_at'];

}
