<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kpi_Bidang extends Model {

	use SoftDeletes;

	protected $table='kpi_bidang';

	public $timestamps = false;

	protected $primaryKey = 'IdKpiBid';

	protected $dates = ['deleted_at'];

}
