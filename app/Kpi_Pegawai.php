<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kpi_Pegawai extends Model {

	use SoftDeletes;

	protected $table='kpi_pegawai';

	public $timestamps = false;

	protected $primaryKey = "IdKpiPeg";

	protected $dates = ['deleted_at'];

	//public function kpiList()
    //{
      //  return $this->hasOne('App\Kpi', 'IdKpi', 'IdKpi');
    //}

}
