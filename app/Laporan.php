<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Laporan extends Model {

	protected $table='laporan';

	use SoftDeletes;

	public $timestamps = false;

	protected $primaryKey = "IdLap";

	protected $dates = ['deleted_at'];

}
