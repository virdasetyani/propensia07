<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pegawai extends Model {

    use SoftDeletes;

	protected $table = 'pegawai';
	public $timestamps = false;
    protected $fillable = ['Nama', 'Alamat', 'NoTelp', 'Email'];
	protected $primaryKey = 'Username';
    protected $dates = ['deleted_at'];

	public function kpi()
    {
        return $this->hasMany('App\Kpi_Pegawai', 'KPegawai', 'IdPeg');
    }

    public function user()
    {
        return $this->hasOne('App\Users', 'Username', 'Username');
    }

    public function regional()
    {
        return $this->hasOne('App\Regional', 'IdReg', 'PregID');
    }

    public function bidang()
    {
        return $this->hasOne('App\Bidang', 'IdBid', 'PbidID');
    }

    public function kpiall()
    {
        return $this->hasManyThrough('App\Kpi_Pegawai', 'App\Kpi', 'IdKpi', 'IdKpi');
    }

}
