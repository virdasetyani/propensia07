<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Regional extends Model {

	protected $table = 'regional';

	public $timestamps = false;

	protected $fillable = ['IdReg', 'Nama'];

	protected $primaryKey = 'IdReg';


}
