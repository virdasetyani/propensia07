<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Regional_Bidang extends Model {

	protected $table='regional_bidang';
	public $timestamps = false;
	protected $fillable = ['IdRegBid', 'Bid', 'Reg'];
	protected $primaryKey = 'IdRegBid';
}
