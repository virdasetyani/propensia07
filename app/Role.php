<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model {

	protected $table = 'role';

	public $timestamps = false;

	protected $fillable = ['IdRole', 'Nama'];

	protected $primaryKey = 'IdRole';

}
