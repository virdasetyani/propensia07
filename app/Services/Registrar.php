<?php namespace App\Services;

use App\Users;
use App\Pegawai;
use Validator;
use Illuminate\Contracts\Auth\Registrar as RegistrarContract;

class Registrar implements RegistrarContract {

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	public function validator(array $data)
	{
		return Validator::make($data, [
			'Username' => 'required|max:20|unique:user',
			'password' => 'required|confirmed|min:6',
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	public function create(array $data)
	{
		$user = Users::create([
			'Username' => $data['Username'],
			'Password' => bcrypt($data['password']),
			'Ureg' => $data['Ureg'],
			'Urole' => $data['Urole'],
		]);

		$pegawai = new Pegawai;
		$pegawai->Username = $data['Username'];
		$pegawai->IdPeg = $data['IdPeg'];
		$pegawai->save();

		return $user; 
	}

}
