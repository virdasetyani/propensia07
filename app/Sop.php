<?php namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Sop extends Model {

	use SoftDeletes;

	protected $table='sop';

	public $timestamps = false;

	protected $fillable = ['Kode', 'SBid', 'Judul', 'File'];

	protected $primaryKey = 'Kode';


}
