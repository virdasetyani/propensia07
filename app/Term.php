<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Term extends Model {

	protected $table='term';
	//protected $fillable = ['IdTerm', 'Bulan', 'Tahun'];
	public $timestamps = false;

	protected $primaryKey = 'IdTerm';


}
