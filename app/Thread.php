<?php namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Thread extends Model {
	use SoftDeletes;

    protected $dates = ['deleted_at'];
    
	protected $table='thread';

	public $timestamps = false;

	protected $fillable = ['Judul', 'Isi'];

	protected $primaryKey = 'IdThread';

	public function comments()
    {
        return $this->hasMany('App\Comment', 'IdPost', 'IdThread');
    }

}
