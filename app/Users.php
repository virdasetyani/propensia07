<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;



class Users extends Model implements AuthenticatableContract, CanResetPasswordContract{

	use Authenticatable, CanResetPassword, SoftDeletes;
	
	protected $table = 'user';
	public $timestamps = false;
	protected $fillable = ['Username', 'Password', 'UpegID', 'UregID', 'UbidID', 'UroleID'];
	protected $hidden = ['password', 'remember_token'];
	protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];

	public function pegawai() 
    {
        //$this->belongsTo('App\Regional', 'UpegID', 'IdPeg');
        return $this->belongsTo('App\Pegawai', 'UpegID', 'IdPeg');
    }

    public function regional() 
	{
        //$this->belongsTo('App\Regional', 'URegID', 'IdReg');
        return $this->belongsTo('App\Regional', 'UregID', 'IdReg');
    }

    
	public function bidang()
    {
       // $this->belongsTo('App\Bidang', 'UbidID', 'IdBid');
        return $this->belongsTo('App\Bidang', 'UbidID', 'IdBid');
    }

    public function role()
    {
       // $this->belongsTo('App\Role', 'UroleID', 'IdRole');
        return $this->belongsTo('App\Role', 'UroleID', 'IdRole');
    }
    
}