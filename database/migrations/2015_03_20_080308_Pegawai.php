<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pegawai extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pegawai', function(Blueprint $table)
		{
			$table->string('Username', 20)->references('Username')->on('user');
			$table->string('IdPeg', 10);
			$table->string('Nama', 50);
			$table->text('Alamat');
			$table->string('NoTelp', 20);
			$table->string('Email', 20);
			$table->string('Foto')->nullable();
			$table->string('IdSup', 10)->references('IdPeg')->on('pegawai');
			$table->primary('IdPeg');
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pegawai');
	}

}
