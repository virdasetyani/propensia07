<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Thread extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('thread', function(Blueprint $table)
		{
			$table->increments('IdThread');
			$table->integer('IdFor')->references('IdForum')->on('forum');
			$table->string('Judul', 20);
			$table->text('Isi');
			$table->date('Tanggal');
			$table->time('Jam');
			$table->string("TUser", 20)->references('Username')->on('user');
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('thread');
	}

}
