<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Comment extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('comment', function(Blueprint $table)
		{
			$table->increments('IdComment');
			$table->integer('IdPost')->references('IdThread')->on('thread');
			$table->text('Isi');
			$table->date('Tanggal');
			$table->time('Jam');
			$table->string("CUser", 20)->references('Username')->on('user');
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('comment');
	}

}
