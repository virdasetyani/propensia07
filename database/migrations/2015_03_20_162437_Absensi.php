<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Absensi extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('absensi', function(Blueprint $table)
		{
			$table->integer('Tanggal');
			$table->string('Apegawai')->references('IdPeg')->on('pegawai');
			$table->time('JamDatang');
			$table->time('JamPulang');
			$table->integer('DurasiJam');
			$table->text('AlasanAbsen');
			$table->boolean('IsApproved');
			$table->integer('Aterm')->references('IdTerm')->on('term');
			$table->primary(['Tanggal','Apegawai','Aterm']);
			$table->softDeletes();		
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('absensi');
	}

}
