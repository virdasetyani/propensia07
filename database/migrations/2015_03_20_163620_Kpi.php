<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Kpi extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('kpi', function(Blueprint $table)
		{
			$table->increments('IdKpi');
			$table->string('Indikator', 50);
			$table->string('Target', 50);
			$table->string('Aktual', 50);
			$table->double('PersenTarget');
			$table->double('PersnAktual');
			$table->double('PersenAchievement')->nullable();
			$table->boolean('IsApproved');
			$table->text('Notes')->nullable();
			$table->integer('KTerm')->references('IdTerm')->on('term');
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('kpi');
	}

}
