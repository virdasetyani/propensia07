<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KpiPegawai extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('kpi_pegawai', function(Blueprint $table)
		{
			$table->increments('IdKpiPeg');
			$table->integer('IdKpi')->references('IdKpi')->on('kpi');
			$table->string('KPegawai', 10)->references('IdPeg')->on('pegawai');
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('kpi_pegawai');
	}

}
