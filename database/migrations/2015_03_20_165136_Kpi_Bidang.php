<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KpiBidang extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('kpi_bidang', function(Blueprint $table)
		{
			$table->increments('IdKpiBid');
			$table->integer('IdKpi')->references('IdKpi')->on('kpi');
			$table->integer('Kbidang')->references('IdBid')->on('bidang');
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('kpi_bidang');
	}

}
