<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DevPlan extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dev_plan', function(Blueprint $table)
		{
			$table->increments('Id');
			$table->string('IdPeg', 10)->references('IdPeg')->on('pegawai');
			$table->integer('DTerm')->references('IdTerm')->on('term');
			$table->text('PlanPegawai')->nullable();
			$table->text('PlanSupervisi')->nullable();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('dev_plan');
	}

}
