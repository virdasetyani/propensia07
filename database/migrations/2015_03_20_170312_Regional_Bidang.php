<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RegionalBidang extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('regional_bidang', function(Blueprint $table)
		{
			$table->increments('IdRegBid');
			$table->integer('Bid')->references('IdBid')->on('bidang');
			$table->integer('Reg')->references('IdReg')->on('regional');
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('regional_bidang');
	}

}
