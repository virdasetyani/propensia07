<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Sop extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sop', function(Blueprint $table)
		{
			$table->string('Kode',5);
			$table->integer('SBid')->references('IdBid')->on('bidang');
			$table->string('Judul', 50);
			$table->string('File');
			$table->primary('Kode');
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sop');
	}

}
