<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Laporanbaru extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('laporan', function(Blueprint $table)
		{
			$table->increments('IdLap');
			$table->integer('LTerm')->references('IdTerm')->on('term');
			$table->integer('LReg')->references('IdReg')->on('regional');
			$table->string('Aktivitas', 50);
			$table->text('Deskripsi');
			$table->string('Target', 50);
			$table->string('Aktual',50);
			$table->double('PersenTarget');
			$table->double('PersenAktual');
			$table->string('Attachment')->nullable();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
