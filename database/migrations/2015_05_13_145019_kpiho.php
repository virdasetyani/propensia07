<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Kpiho extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('kpi_pegawai', function($table)
		{
			$table->string('Aktual', 50);
			$table->double('PersenAktual');
			$table->double('PersenAchievement')->nullable();
			$table->boolean('IsDone');
			$table->text('Notes')->nullable();
			$table->integer('KPTerm')->references('IdTerm')->on('term');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
