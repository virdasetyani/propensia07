<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteKpi extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('kpi', function($table)
		{
    		$table->dropColumn('Aktual');
    		$table->dropColumn('PersenAktual');
    		$table->dropColumn('PersenAchievement');
    		$table->dropColumn('Notes');
    		$table->integer('KBidang')->references('IdBid')->on('bidang');
		});	
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
