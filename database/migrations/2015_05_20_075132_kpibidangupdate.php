<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Kpibidangupdate extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
			Schema::table('kpi_bidang', function($table)
	{
   		 	$table->string('Aktual', 50);
			$table->double('PersenAktual');
			$table->double('PersenAchievement');
			$table->boolean('IsDone');
			$table->text('Notes');
			$table->integer('KBTerm')->references('IdTerm')->on('term');
	});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
