<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		Users::create([
			'Username' => 'user1',
			'Password' => bcrypt('123456'),
			'Urole' => '1',
		]);

		Role::create([
			'Jabatan' => 'Super Admin',
		]);

		Role::create([
			'Jabatan' => 'Admin',
		]);

		Role::create([
			'Jabatan' => 'Pimpinan Regional',
		]);

		Role::create([
			'Jabatan' => 'Manajer Bidang',
		]);

		Role::create([
			'Jabatan' => 'Pegawai',
		]);
		// $this->call('UserTableSeeder');
	}

}
