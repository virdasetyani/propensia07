@extends('app')
@extends('header')

@section('content')
<div class="col-md-10 col-md-offset-1">
        <ol class="breadcrumb">
            <li><a href="{{ url('/home') }}">Beranda</a></li>
            <li class="active">Sop</li>
        </ol>
    </div>
<div class="jumbroton"><center><h3>Daftar File SOP</h3></center></div>
@if (Session::has('message'))
   					<div class="alert alert-success">{{ Session::get('message') }}</div>
				@endif
	
	<form method="POST" role="form" action="{{ url('/Sop') }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			
			<center><p>Pilih Bidang : 
			<select name="IdBid">
				@foreach($bidang as $bidang)
				<option value="{{ $bidang->IdBid }}"> {{ $bidang->NamaBid }}</option>
				@endforeach
			</select>
			<button type="submit" value="submit">Pilih</button></p></center>
		</form>

	@if(Session::has('data'))
	<?php $data = Session::get('data'); ?>
	<div class="col-md-8 col-md-offset-2">
	<div class="panel panel-default">
	<table class="table">
		<thead>
			<tr>
				<th style="text-align:center">Kode</th>
				<th style="text-align:center">Judul</th>
				<th style="text-align:center">Dokumen</th>	
			</tr>
		</thead>
		<tbody>
			@foreach($data as $Sop)
			<tr>
				<td>{{ $Sop->Kode}}</td>
				<td>{{ $Sop->Judul}}</td>
				<td>{{ $Sop->File}}</td>
				
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
</div>
@endif
</div>
@endsection