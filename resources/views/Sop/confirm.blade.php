@extends('app')
@extends('header')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<table class="table">
				<tr><td><h3>Apakah Anda yakin akan menghapus dokumen SOP {{ $data->Judul }} ? </h3></td>
				</tr>
			</table>
		</div>

		<div class="col-md-2 col-md-offset-5">
			<table class="table">
				<tr><td><a href='{{ action('SopController@destroy', $data->Kode)}}' class="btn btn-info">Ya</a></td>
					<td><a href='{{ action('SopController@index') }}' class="btn btn-info">Tidak</a></td>
				</tr>
			</table>
		</div>
	</div>
</div>
@endsection