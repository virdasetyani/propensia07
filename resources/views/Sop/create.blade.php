@extends('app')
@extends('header')

@section('content')
<div class="col-md-10 col-md-offset-1">
        <ol class="breadcrumb">
            <li><a href="{{ url('/home') }}">Beranda</a></li>
            <li><a href="{{ url('/Sop') }}">Sop</a></li>
            <li class="active">Unggah</li>
        </ol>
    </div>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			
			

			<div class="panel panel-default">
		
				<div class="panel-heading"><center> Unggah Dokumen SOP </center></div>
				<div class="panel-body">

				@if (Session::has('message'))
   					<div class="alert alert-success">{{ Session::get('message') }}</div>
				@endif


	<form class="form-horizontal" role="form" method="POST" action="{{ url('/Sop/create') }}"  enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

	
			<div class="form-group">
				<label class="col-md-3 control-label">Bidang</label>
				<div class="col-md-5">
				<select name="IdBid">
				@foreach($bidang as $bidang)
				<option value="{{ $bidang->IdBid }}"> {{ $bidang->NamaBid }}</option>
				@endforeach
				</select>
				</div>
			</div>

			<div class="form-group @if ($errors->has('Kode')) has-error @endif">
					<label class="col-md-3 control-label">Kode SOP <font color="red">*</font>
					</label>
					<div class="col-md-7">
						<input type="text" class="form-control" name="Kode" value="{{ old('Kode') }}">
							@if ($errors->has('Kode')) <p class="help-block error-alert">{{ $errors->first('Kode')}}</p> @endif
					</div>

			</div>
			<div class="form-group @if ($errors->has('Judul')) has-error @endif">
					<label class="col-md-3 control-label">Judul SOP <font color="red">*</font>
					</label>
					<div class="col-md-7">
						<input type="text" class="form-control" name="Judul" value="{{ old('Judul') }}">
							@if ($errors->has('Judul')) <p class="help-block error-alert">{{ $errors->first('Judul')}}</p> @endif
					</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label">File </label>
				<div class="col-md-5">
				<input type="file" name="file" value="{{ old('file') }}">
				</div>
			</div>

	<div class="form-group">
		<div class="col-md-6 col-md-offset-4">
		<button type="submit" class="btn btn-primary">
		Unggah
		</button>
		</div>
	</div>
	<div style="float:right"><font color="red" size="2">* Wajib diisi</font></div>
	</div>
	</form>
@endsection