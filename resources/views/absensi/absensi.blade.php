@extends('app')
@extends('header')

@section('content')
<div class="col-md-10 col-md-offset-1">
        <ol class="breadcrumb">
            <li><a href="{{ url('/home') }}">Beranda</a></li>
            <li><a href="{{ url('/public/view') }}">Absensi</a></li>
            <li class="active">Isi</li>
        </ol>
    </div><br><br>
<div class="container-fluid">
<div class="row">
	<div class="jumbroton"><center><h3>Isi Absensi Hari Ini</h3></center></div>
	<div class="jumbroton"><center><h4>
	<?php date_default_timezone_set('Asia/Jakarta');
		$tanggal= mktime(date("m"),date("d"),date("Y"));
		$tglsekarang = date("d-m-Y", $tanggal);
		echo $tglsekarang;
	?></h3></center></div>
	<div class="col-md-10 col-md-offset-1">
	@if (Session::has('message'))
   		<div class="alert alert-success">{{ Session::get('message') }}</div>
	@endif
	<div class="panel panel-default">
	<table class="table">
		<thead>
			<tr>
				<th style="text-align:center">Id Pegawai</th>
				<th style="text-align:center">Nama</th>
				<th style="text-align:center">Hadir</th>
				<th style="text-align:center">Sakit</th>
				<th style="text-align:center">Izin</th>
				<th style="text-align:center">Alpha</th>
				<th style="text-align:center">Jam Datang</th>
				<th style="text-align:center">Jam Pulang</th>
			</tr>
			<?php $i=1; ?>
			<form method="POST" role="form" action="{{ url('/absensi/') }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			@if (isset($dataPegawai))
				@foreach($dataPegawai as $absen)
				<?php $idPegawai = $absen->IdPeg?>
				<?php $namaPegawai = $absen->Nama ?>
				<tr>
					<td>{{ $absen->IdPeg }}</td>
					<td>{{ $absen->Nama }}</td>
					<input type="hidden" name=<?php echo $idPegawai ?>Nama value=<?php echo $namaPegawai ?>>
					<td><input type="radio" name=<?php echo $idPegawai ?>Status value="hadir"></td>
					<td><input type="radio" name=<?php echo $idPegawai ?>Status value="sakit"></td>
					<td><input type="radio" name=<?php echo $idPegawai ?>Status value="izin"></td>
					<td><input type="radio" name=<?php echo $idPegawai ?>Status value="alpha"></td>
					<td><input type="time" name=<?php echo $idPegawai ?>JamDtg value="JamDatang"></td>
					<td><input type="time" name=<?php echo $idPegawai ?>JamPlg value="JamPulang"></td>
				</tr>
				<?php $i++; ?>
				@endforeach
			@elseif (isset($dataAbsenHariIni))
				@foreach($dataAbsenHariIni as $absen)
				<?php $idPegawai = $absen->AIdPeg ?>
				<?php $namaPegawai = $absen->Apegawai ?>
				<tr>
					<td>{{ $absen->AIdPeg }}</td>
					<td>{{ $absen->Apegawai }}</td>
					<input type="hidden" name=<?php echo $idPegawai ?>Nama value=<?php echo $namaPegawai ?>>

					@if ($absen->Hadir == 1)
						<td><input type="radio" name=<?php echo $idPegawai ?>Status value="hadir" checked ></td>
						<td><input type="radio" name=<?php echo $idPegawai ?>Status value="sakit"></td>
						<td><input type="radio" name=<?php echo $idPegawai ?>Status value="izin"></td>
						<td><input type="radio" name=<?php echo $idPegawai ?>Status value="alpha"></td>	
						<td><input type="time" name=<?php echo $idPegawai ?>JamDtg value=<?php echo $absen->JamDatang ?>></td>
						<td><input type="time" name=<?php echo $idPegawai ?>JamPlg value=<?php echo $absen->JamPulang ?>></td>

					@elseif ($absen->Sakit == 1)
						<td><input type="radio" name=<?php echo $idPegawai ?>Status value="hadir"></td>
						<td><input type="radio" name=<?php echo $idPegawai ?>Status value="sakit" checked></td>
						<td><input type="radio" name=<?php echo $idPegawai ?>Status value="izin"></td>
						<td><input type="radio" name=<?php echo $idPegawai ?>Status value="alpha"></td>
						<td><input type="time" name=<?php echo $idPegawai ?>JamDtg value=<?php echo $absen->JamDatang ?>></td>
						<td><input type="time" name=<?php echo $idPegawai ?>JamPlg value=<?php echo $absen->JamPulang ?>></td>

					@elseif ($absen->Izin == 1)
						<td><input type="radio" name=<?php echo $idPegawai ?>Status value="hadir"></td>
						<td><input type="radio" name=<?php echo $idPegawai ?>Status value="sakit"></td>
						<td><input type="radio" name=<?php echo $idPegawai ?>Status value="izin" checked></td>
						<td><input type="radio" name=<?php echo $idPegawai ?>Status value="alpha"></td>	
						<td><input type="time" name=<?php echo $idPegawai ?>JamDtg value=<?php echo $absen->JamDatang ?>></td>
						<td><input type="time" name=<?php echo $idPegawai ?>JamPlg value=<?php echo $absen->JamPulang ?>></td>

					@elseif ($absen->Alpha == 1)
						<td><input type="radio" name=<?php echo $idPegawai ?>Status value="hadir"></td>
						<td><input type="radio" name=<?php echo $idPegawai ?>Status value="sakit"></td>
						<td><input type="radio" name=<?php echo $idPegawai ?>Status value="izin"></td>
						<td><input type="radio" name=<?php echo $idPegawai ?>Status value="alpha" checked></td>					
					@endif
				</tr>
				<?php $i++; ?>
				@endforeach
			@endif
		</thead>
	</table>
	</div>
	<center><button type="submit" value="submit">Simpan</button></center> 
	</form>
	</div>
</div>
</div>

@endsection