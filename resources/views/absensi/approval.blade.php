@extends('app')
@extends('header')

@section('content')
<div class="col-md-10 col-md-offset-1">
        <ol class="breadcrumb">
            <li><a href="{{ url('/home') }}">Beranda</a></li>
            <li><a href="{{ url('/public/view') }}">Absensi</a></li>
            <li class="active">Setujui</li>
        </ol>
    </div><br><br>
<div class='jumbroton'><center><h3>Setujui Absensi</h3></center></div>
<div class="col-md-10 col-md-offset-1">
@if(Session::has('message1'))
	<div class="alert alert-success">{{ Session::get('message1') }}</div>
@endif

@if(!Session::has('data'))
	<form method="POST" role="form" action="{{ url('/viewDetail') }}">	
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<center><p>Pilih Term : 
			<select name="IdTerm">
				@foreach($term as $term)
				<option value="{{ $term->IdTerm }}"> {{ $term->Bulan }}  {{ $term->Tahun }}</option>
				@endforeach
			</select>
		<button type="submit" value="submit">Pilih</button></p></center>
	</form>
@else
	<form method="POST" role="form" action="{{ url('/viewDetail') }}">	
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<center><p>Pilih Term : 
			<select name="IdTerm">
				@foreach($term as $term)
				<option value="{{ $term->IdTerm }}"<?php if(Session::get('data') == $term->IdTerm){echo "selected";} ?>> {{ $term->Bulan }}  {{ $term->Tahun }}</option>
				@endforeach
			</select>
		<button type="submit" value="submit">Pilih</button></p></center>
	</form>
	<br><br>

	<?php $listTanggal = Session::get('listTanggal'); ?>
	<form method="POST" role="form" action="{{ url('/viewMoreDetail') }}">	
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="idTerm" value="{{ Session::get('data') }}">
		<input type="hidden" name="mode" value="Tanggalan">
		<center><p>Pilih Tanggal : 
			<select name="Tanggal">
				@foreach($listTanggal as $tanggal)
					<option value="{{ $tanggal->Tanggal }}">{{ $tanggal->Tanggal }}</option>
				@endforeach
			</select>
		<button type="submit" value="submit">Pilih</button></p></center>
	</form>
</div>
@endif
@if(Session::has('absen'))
	<?php $daftarAbsen = Session::get('absen'); ?>
	<div class="col-md-10 col-md-offset-1">
		<?php echo Session::get('message'); ?>
		<div class="panel panel-default">
		<table class="table">
			<thead>
				<tr>
					<th style="text-align:center">Id Pegawai</th>
					<th style="text-align:center">Nama</th>
					<th style="text-align:center">Hadir</th>
					<th style="text-align:center">Sakit</th>
					<th style="text-align:center">Izin</th>
					<th style="text-align:center">Alpha</th>
				</tr>
				<form method="POST" role="form" action="{{ url('/approve') }}">	
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="date" value=<?php echo Session::get('tanggal'); ?>>
				@foreach($daftarAbsen as $absen)
					<tr>
						<td>{{ $absen->Apegawai }}</td>
						<td>{{ $absen->AIdPeg }}</td>
						<td>{{ $absen->Hadir }}</td>
						<td>{{ $absen->Sakit }}</td>
						<td>{{ $absen->Izin }}</td>
						<td>{{ $absen->Alpha }}</td>
					</tr>
				@endforeach
			</thead>
		</table>
		</div>
		<center><button type="submit" name="submit" value="approve">Setujui</button>
		<button type="submit" name="submit" value="disapprove">Tolak</button></center>
	</form>
	</div>

@endif

@endsection