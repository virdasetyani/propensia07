@extends('app')
@extends('header')

@section('content')
<div class="col-md-10 col-md-offset-1">
        <ol class="breadcrumb">
            <li><a href="{{ url('/home') }}">Beranda</a></li>
            <li class="active">Absensi</li>
        </ol>
    </div><br><br>
<div class='jumbroton'><center><h3>Lihat Absensi</h3></center></div>
@if(!Session::has('data'))
	<form method="POST" role="form" action="{{ url('/viewDetail') }}">	
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<center><p>Pilih Term : 
			<select name="IdTerm">
				@foreach($term as $term)
				<option value="{{ $term->IdTerm }}"> {{ $term->Bulan }}  {{ $term->Tahun }}</option>
				@endforeach
			</select>
		<button type="submit" value="submit">Pilih</button></p></center>
	</form>
@else
	<form method="POST" role="form" action="{{ url('/viewDetail') }}">	
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<center><p>Pilih Term : 
			<select name="IdTerm">
				@foreach($term as $term)
				<option value="{{ $term->IdTerm }}"<?php if(Session::get('data') == $term->IdTerm){echo "selected";} ?>> {{ $term->Bulan }}  {{ $term->Tahun }}</option>
				@endforeach
			</select>
		<button type="submit" value="submit">Pilih</button></p></center>
	</form>

	<br><br>

	<form method="POST" role="form" action="{{ url('/viewMoreDetail') }}">	
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="idTerm" value="{{ Session::get('data') }}">
		<input type="hidden" name="mode" value="Rekapan">
		<center><p>Pilih Rekap : 
			<select name="Rekap">
				<option value="Bulanan">Rekap Bulanan</option>
			</select>
		<button type="submit" value="submit">Pilih</button></p>
		atau</center>
	</form>

	<?php $listTanggal = Session::get('listTanggal'); ?>
	<form method="POST" role="form" action="{{ url('/viewMoreDetail') }}">	
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="idTerm" value="{{ Session::get('data') }}">
		<input type="hidden" name="mode" value="Tanggalan">
		<center><p>Pilih Tanggal : 
			<select name="Tanggal">
				@foreach($listTanggal as $tanggal)
					<option value="{{ $tanggal->Tanggal }}">{{ $tanggal->Tanggal }}</option>
				@endforeach
			</select>
		<button type="submit" value="submit">Pilih</button></p></center>
	</form>

@endif
@if(Session::has('absen'))
	<?php $daftarAbsen = Session::get('absen'); ?>
	<div class="col-md-10 col-md-offset-1">
		<p><?php echo Session::get('message'); ?></p>
		<p><?php echo Session::get('messageApprove'); ?></p>
		<div class="panel panel-default">
		<table class="table">
			<thead>
				<tr>
					<th style="text-align:center">Id Pegawai</th>
					<th style="text-align:center">Nama</th>
					<th style="text-align:center">Hadir</th>
					<th style="text-align:center">Sakit</th>
					<th style="text-align:center">Izin</th>
					<th style="text-align:center">Alpha</th>
				</tr>
				@foreach($daftarAbsen as $absen)
					<tr>
						<td>{{ $absen->Apegawai }}</td>
						<td>{{ $absen->AIdPeg }}</td>
						<td>{{ $absen->Hadir }}</td>
						<td>{{ $absen->Sakit }}</td>
						<td>{{ $absen->Izin }}</td>
						<td>{{ $absen->Alpha }}</td>
					</tr>
				@endforeach
			</thead>
		</table>
		</div>
	</div>
@endif

@endsection