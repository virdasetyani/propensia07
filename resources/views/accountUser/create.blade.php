@extends('app')
@extends('header')

@section('content')
<div class="col-md-10 col-md-offset-1">
        <ol class="breadcrumb">
            <li><a href="{{ url('/home') }}">Beranda</a></li>
            <li><a href="{{ url('/accountUser') }}">Akun Pengguna</a></li>
            <li class="active">Buat</li>
        </ol>
    </div>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Form Pembuatan Akun Pengguna</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					@if (Session::has('message'))
   					<div class="alert alert-success">{{ Session::get('message') }}</div>
				@endif

				<form class="form-horizontal" role="form" method="POST" action="{{ url('/accountUser/regional') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">Regional</label>
							<div class="col-md-6">
								<select name="Ureg">
									@foreach($dataReg as $id)
									<option value="{{ $id->IdReg }}">{{ $id->NamaReg }}</option>
									@endforeach
								</select>
								<button type="submit" class="btn btn-default btn-xs">
									Pilih
								</button>
							</div>
						</div>
				</form>

				@if (Session::has('bid'))
				<?php $bid = Session::get('bid'); ?>
				<form class="form-horizontal" role="form" method="POST" action="{{ url('/accountUser/bidang') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						@foreach($bid as $id)
						<input type="hidden" name="Ureg" value="{{ $id->Reg }}">
						@endforeach

						<div class="form-group">
							<label class="col-md-4 control-label">Bidang</label>
							<div class="col-md-6">
								<select name="Ubid">
									@foreach($bid as $id)
									<option value="{{ $id->IdBid }}">{{ $id->NamaBid }}</option>
									@endforeach
								</select>
								<button type="submit" class="btn btn-default btn-xs">
									Pilih
								</button>
							</div>
						</div>
						
				</form>


				@endif
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
