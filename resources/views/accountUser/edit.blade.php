@extends('app')
@extends('header')

@section('content')
<div class="col-md-10 col-md-offset-1">
        <ol class="breadcrumb">
            <li><a href="{{ url('/home') }}">Beranda</a></li>
            <li><a href="{{ url('/accountUser') }}">Akun Pengguna</a></li>
            <li class="active">Ubah</li>
        </ol>
    </div>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Form Ubah Akun Pengguna</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<p><a href="{{ url('/accountUser/regbid/'.$data->Username)}}" class="btn btn-info col-md-offset-9">Change Regional-Bidang</a><br><br>
						<a href="{{ url('/accountUser/password/'.$data->Username)}}" class="btn btn-info col-md-offset-9">Change Password</a></p>

					<form class="form-horizontal" role="form" method="POST" action="{{ url('/accountUser/edit') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="id" value="{{ $data->id }}">

						<div class="form-group">
							<label class="col-md-4 control-label">Username</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="Username" value="{{ $data->Username }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Nomor Pegawai</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="IdPeg" value="{{ $data->IdPeg }}">
							</div>
						</div>					
						
						<div class="form-group">
							<label class="col-md-4 control-label">Role</label>
							<div class="col-md-6">
								<select name="Urole">
									@foreach($dataRole as $id)
									<option value="{{ $id->IdRole }}" @if(in_array($id, $oke3)) selected @endif>{{ $id->Jabatan }}</option>
									@endforeach
								</select>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Update
								</button>
							</div>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection
