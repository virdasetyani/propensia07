@extends('app')
@extends('header')

@section('content')
<div class="col-md-10 col-md-offset-1">
        <ol class="breadcrumb">
            <li><a href="{{ url('/home') }}">Beranda</a></li>
            <li><a href="{{ url('/accountUser') }}">Akun Pengguna</a></li>
            <li class="active">Buat</li>
        </ol>
    </div>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Form Pembuatan Akun Pengguna | Regional : {{ $regbid->NamaReg }} | Bidang : {{ $regbid->NamaBid }}</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					@if (Session::has('message'))
   					<div class="alert alert-success">{{ Session::get('message') }}</div>
				@endif

				<form class="form-horizontal" role="form" method="POST" action="{{ url('/accountUser/create') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="Uregbid" value="{{ $regbid->IdRegBid }}">


						<div class="form-group @if ($errors-> has('Username')) has-error @endif">
							<label class="col-md-4 control-label">Username</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="Username" value="{{ old('Username') }}">
								@if ($errors -> has ('Username')) <p class= "help-block error-alert"> {{$errors->first('Username')}} </p> @endif
							</div>
						</div>

						<div class="form-group  @if ($errors-> has('IdPeg')) has-error @endif">
							<label class="col-md-4 control-label">Id Pegawai</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="IdPeg" value="{{ old('IdPeg') }}">
								@if ($errors -> has ('IdPeg')) <p class= "help-block error-alert"> {{$errors->first('IdPeg')}} </p> @endif
							</div>
						</div>	
						
						<div class="form-group ">
							<label class="col-md-4 control-label">Role</label>
							<div class="col-md-6">
								<select name="Urole">
									@foreach($dataRole as $id)
									<option value="{{ $id->IdRole }}">{{ $id->Jabatan }}</option>
									@endforeach
								</select>
							</div>
						</div>

						<div class="form-group @if ($errors-> has('password')) has-error @endif">
							<label class="col-md-4 control-label">Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password">
								@if ($errors -> has ('password')) <p class= "help-block error-alert"> {{$errors->first('password')}} </p> @endif
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Confirm Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password_confirmation">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Register
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
