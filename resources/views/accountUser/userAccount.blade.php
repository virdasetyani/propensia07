@extends('app')
@extends('header')

@section('content')
<div class="col-md-10 col-md-offset-1">
        <ol class="breadcrumb">
            <li><a href="{{ url('/home') }}">Beranda</a></li>
            <li class="active">Akun Pengguna</li>
        </ol>
    </div>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Daftar Akun Pengguna SIIP</div>
				<div class="panel-body">
 
	<center><a href="{{ url('/accountUser/create') }}" class="btn btn-info">+Tambah Pengguna</a></center>
	
	<table class="table">
		<thead>
			<tr>
				<th style="text-align:center">Username</th>
				<th style="text-align:center">Nomor Pegawai</th>
				<th style="text-align:center">Regional</th>
				<th style="text-align:center">Bidang</th>
				<th style="text-align:center">Role</th>
			</tr>
		</thead>

		<tbody>
			<?php $no=1;?>
			@foreach($data as $id)
			<tr>
				<td> {{ $id->Username }}</td>
				<td> {{ $id->IdPeg }}</td>
				<td> {{ $id->NamaReg }}</td>
				<td> {{ $id->NamaBid }}</td>
				<td> {{ $id->Jabatan }}</td>
				
				<td><div class="col-sm-pull-1"><a href='{{ action('UserAccountController@edit', $id->Username) }}' class="btn btn-primary">Ubah</form></div></td>
				<td><div class="col-sm-pull-1"><a href='{{ action('UserAccountController@confirm', $id->Username) }}' class="btn btn-primary">Hapus</a></div></td>
				
			</tr>
			<?php $no++; ?>
			@endforeach
		</tbody>
	</table>

	</div>
@endsection