@extends('app')
@extends('header')

@section('content')
<div class="col-md-8 col-md-offset-2">
	<ol class="breadcrumb">
 		<li><a href="{{ url('/home') }}">Beranda</a></li>
 		<li><a href="{{ url('/agenda') }}">Agenda</a></li>
 		<li class="active">{{ $content->Aktivitas}}</li>
	</ol>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2" style="background-color:#F8F9FA"><br>	
		@if (Session::has('delete'))
			<div class="alert alert-success">{{ Session::get('delete') }}. Kembali ke <a href = "home">beranda</a></div>
		@else	
			<div class="panel panel-primary ">
				<div class="panel-heading"><center><b>{{ $content->Aktivitas}}</b></center></div>
				<div class="panel-body"><?php echo nl2br($content->Deskripsi); ?></div>
			</div>
		@endif
		</div>
	</div>
</div>
@endsection