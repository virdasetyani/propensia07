@extends('app')
@extends('header')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-5 col-md-offset-3">
			<div class="panel panel-danger">
				<div class="panel-body">
					<h3>Apakah Anda yakin akan menghapus "{{ $data->Aktivitas }}"? </h3>
				</div>

				<div style="text-align:center">
				<a href='{{ action('AgendaController@destroy', $data->IdAgenda)}}' class="btn btn-primary">Ya</a>
				<a href="{{ url('/agenda') }}" class="btn btn-default">Tidak</a>
				</div><br>

			</div>
		</div>		
	</div>
</div>
@endsection