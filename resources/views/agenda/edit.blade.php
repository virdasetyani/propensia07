@extends('app')
@extends('header')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Ubah Agenda</div>
				<div class="panel-body">

					<form class="form-horizontal" role="form" method="POST" action="{{ action('AgendaController@update') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="id" value="{{ $data->IdAgenda }}">
						
						<div class="form-group @if ($errors->has('Aktivitas')) has-error @endif">

							<label class="col-md-2 control-label">Judul Agenda <font color="red">*</font></label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="Aktivitas" style="width:450px;" value="{{ $data->Aktivitas }}">
								@if ($errors->has('Aktivitas')) <p class="help-block error-alert">{{ $errors->first('Aktivitas')}}</p> @endif
							</div>
						</div>

						<div class="form-group @if ($errors->has('Waktu')) has-error @endif">

							<label class="col-md-2 control-label">Tanggal Agenda <font color="red">*</font></label>
							<div class="col-md-6">
								<input type="date" class="form-control" name="Waktu" style="width:450px;" value="{{ $data->Waktu }}" min="<?=date('Y-m-d')?>">
								@if ($errors->has('Waktu')) <p class="help-block error-alert">{{ $errors->first('Waktu')}}</p> @endif
							</div>
						</div>

						<div class="form-group @if ($errors->has('Deskripsi')) has-error @endif">
							<label class="col-md-2 control-label">Deskripsi </label>
							<div class="col-md-10">
								<textarea class="form-control" name="Deskripsi" rows="5" cols="20">{{ $data->Deskripsi }}</textarea>
								<script type="text/javascript" src="<?= url('css/ckeditor/ckeditor.js') ?>"></script>
								<script>CKEDITOR.replace('Deskripsi');</script>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">Simpan</button>
							</div>
						</div>
						<div style="float:right"><font color="red" size="2">* harus diisi</font></div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
	
@endsection