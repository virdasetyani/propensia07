@extends('app')
@extends('header')

@section('content')
<link rel="stylesheet" href="css/bootstrap.min.css">	
<link rel="stylesheet" href="css/dataTables.bootstrap.css">

<div class="col-md-10 col-md-offset-1">
	<ol class="breadcrumb">
 		<li><a href="{{ url('/home') }}">Beranda</a></li>
 		<li class="active">Agenda</li>
	</ol>
</div>

<div class="col-md-8 col-md-offset-2">
		<div class="form-group">
			<div class="jumbroton"><br>
				<div style="text-align:center"><a href="{{ url('/agenda/create') }}" class="btn btn-primary">Buat Agenda Baru</a></div>
			</div><br><br>
			@if (Session::has('delete'))
				<div class="alert alert-success">{{ Session::get('delete') }}</div>
			@endif
		</div>
</div>

<div class="col-md-10 col-md-offset-1">
	<div class="panel panel-default">
	<table class="table table-hover" id="agendas">
		<thead>
			<tr>
				<th style="text-align:center">Judul Agenda</th>
				<th style="text-align:center">Tanggal Agenda</th>
				<th style="text-align:center">Detail</th>
				@if(in_array(Auth::user()->Urole,array(1,2,4)))
				<th style="text-align:center">Ubah</th>
				<th style="text-align:center">Hapus</th>
				@endif
			</tr>
		</thead>
		<tbody>
			<?php $no=1; ?>
			@foreach($agenda as $agenda)
			<tr>
				<td><div class="col-sm-pull-1" style="text-align:left"><h5>{{ $agenda->Aktivitas}}</h5></div></td>
				<td><div class="col-sm-pull-1"><h5>{{ date("d M Y",strtotime($agenda->Waktu))}}</h5></div></td>
				<td><div class="col-sm-pull-2"><a href='{{ url('/agenda/'.$agenda->IdAgenda) }}' class="btn btn-success">Detail</a></div></td>
				@if(in_array(Auth::user()->Urole,array(1,2,4)))
				<td><div class="col-sm-pull-2"><a href='{{ action('AgendaController@edit', $agenda->IdAgenda) }}' class="btn btn-primary">Ubah</a></div></td>
				<td><div class="col-sm-pull-2"><a href='{{ action('AgendaController@confirm', $agenda->IdAgenda) }}' class="btn btn-danger">Hapus</a></div></td>
				@endif
			</tr>
			<?php $no++; ?>
			@endforeach
		</tbody>
	</table>
	</div>
</div>
@stop
@endsection

@section('link')
<link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">	
<link rel="stylesheet" href="{{ asset('css/dataTables.bootstrap.css') }}">
@endsection

@section('js')

@endsection