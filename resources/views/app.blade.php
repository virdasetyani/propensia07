<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Laravel</title>

	@yield('link')
	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				@if (Auth::guest())
				<ul class="nav navbar-nav navbar-right">
					<li><a href="{{ url('/auth/login') }}">Masuk</a></li>
				</ul>
				@else
				<a class="navbar-brand" href="{{ url('/') }}">Beranda</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">

					<!-- Laporan Kegiatan -->
					@if(in_array(Auth::user()->Urole,array(1,2,3)))
					<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Laporan<span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="{{ url('/laporan/create')}}">Buat Laporan Kegiatan</a></li>
					</ul>
					@endif
					<!-- End Laporan Kegiatan -->

					<!-- Absensi -->
					<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Absensi<span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="{{ url('/absensi') }}">Isi Absensi</a></li>
						<li><a href="{{ url('/view') }}">Lihat Absensi</a></li>
						<li><a href="{{ url('/approvalAbsensi') }}">Setujui Absensi</a></li>
					</ul>
					<!-- End Absensi -->

					<!-- Agenda -->
					@if(in_array(Auth::user()->Urole,array(1,2,4)))
					<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Agenda<span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="{{ url('/agenda/create') }}">Buat Agenda</a></li>
					</ul>
					@endif
					<!-- End Agenda -->

					<!-- KPI Pegawai -->
					<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">KPI Pegawai<span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="{{ url('/kpi/term') }}">Buat Indikator</a></li>
						<li><a href="{{ url('/kpip') }}">Lihat KPI Pegawai</a></li>
						<li><a href="{{ url('/kpi/view') }}">Isi KPI Pegawai</a></li>
						<li><a href=" {{ url('/kpi/delete') }} ">Hapus KPI Pegawai</a></li>
						<li><a href=" {{ url('/kpi/sup') }}">Setujui KPI Pegawai</a></li>
					</ul>
					<!-- End KPI Pegawai -->

					<!-- KPI Bidang -->
					<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">KPI Bidang<span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						@if(in_array(Auth::user()->Urole,array(1,2,4)))
						<li><a href="{{ url('/kpic/term') }}">Buat KPI Bidang</a></li>
						@endif
						<li><a href="{{ url('/kpib') }}">Lihat KPI Bidang</a></li>
						@if(in_array(Auth::user()->Urole,array(1,2,4)))
						<li><a href="{{ url('/kpic/view') }}">Isi KPI Bidang</a></li>
						<li><a href=" {{ url('/kpic/hapus') }}">Hapus KPI Bidang</a></li>
						@endif
						@if(in_array(Auth::user()->Urole,array(3)))
						<li><a href=" {{ url('/kpic/approve') }}">Setujui KPI Bidang</a></li>
						@endif
					</ul>
					<!-- End KPI Bidang -->

					<!-- SOP -->
					<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">SOP<span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						@if(in_array(Auth::user()->Urole,array(1,2,4)))
						<li><a href="{{ url('/Sop/create') }}">Unggah SOP</a></li>
						@endif
						<li><a href="{{ url('/Sop') }}"> Lihat SOP</a></li>
					</ul>
					<!-- End SOP -->

					<!-- Bidang -->
					@if(in_array(Auth::user()->Urole,array(1,2)))
					<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Bidang<span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="{{ url('/bidang/create') }}">Buat Bidang</a></li>
						<li><a href="{{ url('/bidang') }}">Lihat Bidang</a></li>
					</ul>
					@endif
					<!-- End Bidang -->

					<!-- Regional -->
					@if(Auth::user()->Urole==1)
					<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Regional<span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="{{ url('/regional/create') }}">Buat Regional</a></li>
						<li><a href="{{ url('/regional') }}">Lihat Regional</a></li>
					</ul>
					@endif
					<!-- End Regional -->

					<!-- User Account -->
					@if(in_array(Auth::user()->Urole,array(1)))
					<li><a href="{{ url('/accountUser') }}">Akun Pengguna</a></li>
					@endif
					<!-- End User Account -->

					<!-- Term -->
					@if(in_array(Auth::user()->Urole,array(1)))
					<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Term<span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="{{ url('/term/create') }}">Buat Term</a></li>
						<li><a href="{{ url('/term') }}">Lihat Term</a></li>
					</ul>
					@endif
					<!-- End Term -->

				</ul>

				<ul class="nav navbar-nav navbar-right">
					@if (Auth::guest())
						<li><a href="{{ url('/auth/login') }}">Login</a></li>
						<li><a href="{{ url('/auth/register') }}">Daftar</a></li>
					@else
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->Username }} <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ url('/auth/logout') }}">Keluar</a></li>
							</ul>
						</li>
					@endif
				</ul>
				@endif
			</div>
		</div>
	</nav>

	@yield('content')

	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>

	@yield('js')
	
</body>
</html>
