@extends('app')
@extends('header')

@section('content')
	<div class="col-md-10 col-md-offset-1">
        <ol class="breadcrumb">
            <li><a href="{{ url('/home') }}">Beranda</a></li>
            <li class="active">Bidang</li>
        </ol>
    </div><br><br>
@if ($data->isEmpty())
	Bidang tidak ada!
@else
				@if (Session::has('message'))
   					<div class="alert alert-success">{{ Session::get('message') }}</div>
				@endif
	<div class="jumbroton"><center><h3>Daftar Bidang</h3></center></div>
	<div class="col-md-8 col-md-offset-2">
	<div class="panel panel-default">
	<table class="table">
		<thead>
			<tr>
				<th style="text-align:center">No</th>
				<th style="text-align:center">Daftar Bidang</th>
				<th style="text-align:center">Ubah</th>
				<th style="text-align:center">Hapus</th>
			</tr>
		</thead>
		<tbody>
			<?php $no=1; ?>
			@foreach($data as $bid)
			<tr>
				<td> <?php echo $no ?></td>
				<td>{{ $bid->NamaBid}}</td>
				
				<td><div class="col-sm-pull-1"><a href='{{ action('BidangController@edit', $bid->IdBid) }}' class="btn btn-primary">Ubah</form></div></td>
				<td><div class="col-sm-pull-1"><a href='{{ action('BidangController@confirm', $bid->IdBid) }}' class="btn btn-primary">Hapus</a></div></td>
				</tr>
			<?php $no++; ?>
			@endforeach
		</tbody>
	</table>
</div>
@endif
@stop
@endsection
