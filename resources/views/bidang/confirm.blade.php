@extends('app')
@extends('header')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<table class="table">
				<tr><td><h4>Apakah Anda yakin akan menghapus bidang {{ $data->NamaBid }} ? </h4></td>
				</tr>
			</table>
		</div>
		<div class="col-md-2 col-md-offset-5">
			<table class="table">

				
				<tr><td><a href='{{ action('BidangController@destroy', $data->IdBid)}}' class="btn btn-info">Ya</a></td>
					<td><a href='{{ action('BidangController@index') }}' class="btn btn-info">Tidak</a></td>
				</tr>
			</table>
		</div>
	</div>
</div>
@endsection