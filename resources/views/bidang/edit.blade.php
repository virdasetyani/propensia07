@extends('app')
@extends('header')

@section('content')
<div class="col-md-10 col-md-offset-1">
        <ol class="breadcrumb">
            <li><a href="{{ url('/home') }}">Beranda</a></li>
            <li><a href="{{ url('/bidang') }}">Bidang</a></li>
            <li class="active">Ubah</li>
        </ol>
    </div>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">
					Ubah Bidang
				</div>
					<div class="panel-body">

						@if (Session::has('message'))
   							<div class="alert alert-success">
   								{{ Session::get('message') }}
   							</div>
						@endif
				
						<form class="form-horizontal" role="form" method="POST" action="{{ action('BidangController@update') }}">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="id" value="{{ $data->IdBid }}">

							<div class="form-group @if ($errors-> has('NamaBid')) has-error @endif">
								<label class="col-md-4 control-label">Nama Bidang 
									<font color="red">*</font>
								</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="NamaBid" value="{{ $data->NamaBid }}">
									@if ($errors -> has ('NamaBid')) 
										<p class= "help-block error-alert"> {{$errors->first('NamaBid')}} </p>
									@endif
								</div>
							</div>
	
		<div class="form-group">
			<div class="col-md-6 col-md-offset-4">
				<button type="submit" class="btn btn-primary">
					Ubah Bidang
				</button>
			</div>
		</div>

		<div style="float:right">
			<font color="red" size"2"> * Wajib diisi 
			</font>
		</div>
	</form>
	
@endsection