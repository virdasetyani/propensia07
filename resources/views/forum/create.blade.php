@extends('app')
@extends('header')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Isian Forum Baru</div>
				<div class="panel-body">

					<form class="form-horizontal" role="form" method="POST" action="{{ url('/forum/create') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="form-group @if ($errors->has('Nama')) has-error @endif">
						<label class="col-md-2 control-label">Nama Forum <font color="red">*</font></label>
						<div class="col-md-6">
							<input type="text" class="form-control" name="Nama" value="{{ old('Nama') }}">
							@if ($errors->has('Nama')) <p class="help-block error-alert">{{ $errors->first('Nama')}}</p> @endif
						</div>
					</div>

					<div class="form-group @if ($errors->has('Keterangan')) has-error @endif">
						<label class="col-md-2 control-label">Keterangan </label>
						<div class="col-md-10">
							<textarea class="form-control" name="Keterangan" rows="5" cols="20">{{ old('Keterangan') }}</textarea>
							<script type="text/javascript" src="<?= url('css/ckeditor/ckeditor.js') ?>"></script>
							<script>CKEDITOR.replace('Keterangan');</script>
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-6 col-md-offset-4">
						<button type="submit" class="btn btn-primary">Tambahkan Forum</button>
						</div>
					</div>
					<div style="float:right"><font color="red" size="2">* harus diisi</font></div>
				
					</form>
	
				</div>
			</div>
		</div>
	</div>
</div>
@endsection