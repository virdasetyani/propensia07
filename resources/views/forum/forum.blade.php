@extends('app')
@extends('header')

@section('content')

<div><center><h3>Forum</h3></center></div>
	<div class="col-md-8 col-md-offset-2">
	<div class="panel panel-default">
		
					<center><a href="{{ url('/forum/create') }}" class="btn btn-primary">Create New Forum</a></center><br>
	<center><table>
		<tbody>
			<?php $no=1; ?>
			@foreach($data as $forum)
			<tr>
				<td><div class="text-left"><a href='{{ action('ThreadController@show', $forum->IdForum) }}'><h5>{{ $forum->Nama}}</h5></a></div></td>
				<td><div class="col-sm-1"><a href='{{ action('ForumController@edit', $forum->IdForum) }}' class="btn btn-primary btn-xs">Edit</form></div></td>
				<td><div class="col-sm-1"><a href='{{ action('ForumController@confirm', $forum->IdForum) }}' class="btn btn-danger btn-xs">Delete</a></div></td>
			</tr>
			<?php $no++; ?>
			@endforeach
		</tbody>
	</table></center>
</div>

@stop
@endsection