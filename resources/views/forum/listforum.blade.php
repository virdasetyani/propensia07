@extends('app')
@extends('header')

@section('content')
<div class="col-md-10 col-md-offset-1">
	<ol class="breadcrumb">
 		<li><a href="{{ url('/home') }}">Beranda</a></li>
 		<li class="active">Forum</li>
	</ol>
</div>

<div class="col-md-8 col-md-offset-2">
		<div class="form-group">
			<div class="jumbroton"><br>
				<div style="text-align:center"><a href="{{ url('/forum/create') }}" class="btn btn-primary">Buat Forum Baru</a></div><br>
			</div>
			@if (Session::has('message'))
				<div class="alert alert-success">{{ Session::get('message') }}</div>
			@endif
		</div>
</div>

<div class="col-md-10 col-md-offset-1">
	<div class="panel panel-default">
	<table class="table table-hover" id="forums">
		<thead>
			<tr>
				<th style="text-align:center">Nama Forum</th>
				<th style="text-align:center">Keterangan</th>
				@if(Auth::user()->Urole==1)
				<th style="text-align:center">Ubah</th>
				<th style="text-align:center">Hapus</th>
				@endif
			</tr>
		</thead>
		<tbody>
			<?php $no=1; ?>
			@foreach($data as $data)
			<tr>
				<td><div style="text-align:left">{{ $data->Nama}}</div></td>
				<td><div class="col-sm-pull-1" style="text-align:left"><?php echo nl2br($data->Keterangan); ?></div></td>
				@if(Auth::user()->Urole==1)
					<td><div class="col-sm-pull-2"><a href='{{ action('ForumController@edit', $data->IdForum) }}' class="btn btn-primary btn-xs">Ubah</a></div></td>
					<td><div class="col-sm-pull-2"><a href='{{ action('ForumController@confirm', $data->IdForum) }}' class="btn btn-danger btn-xs">Hapus</a></div></td>
				@endif
			</tr>
			<?php $no++; ?>
			@endforeach
		</tbody>
	</table>
	</div>
</div>
@stop
@endsection

@section('link')
<link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">	
<link rel="stylesheet" href="{{ asset('css/dataTables.bootstrap.css') }}">
@endsection

@section('js')
<script src="{{ asset('js/jquery.dataTables.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/dataTables.bootstrap.js') }}" type="text/javascript"></script>	
<script>
    $(document).ready(function(){
    	$('#forums').dataTable();
    });
</script>
@endsection