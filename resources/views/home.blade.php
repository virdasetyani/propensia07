@extends('app')
@extends('header')

@section('content')        
		@if($data->Nama ==null)
			<div class="alert alert-danger">Data Anda Belum Lengkap! Mohon Isi Terlebih Dahulu
				<a href="{{ url('/profile/edit/') }}">Disini!</a></div>
		@endif

		<div class="col-md-2">
			<div class="panel panel-default">
				<div class="panel-heading">Selamat Datang</div>

				<div class="panel-body">
					<h4><div style="text-align:center">{{ $data->Nama }}</div></h4>
					@if($data->Foto == null)
					<div style="text-align:center"><img src="{{ asset('/css/img/ava.jpg') }}" style='width:150px;height:150px'></div><br>
					@else
					<div style="text-align:center"><img src="{{ asset($data->Foto) }}" style='width:150px;height:150px'></div><br>
					@endif
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<td><div class="col-sm-pull-1">
					
						<div style="text-align:center"><a href="{{ url('/profile/view') }}" class="btn btn-default btn-md">Lihat Profil</a></div></div></td>
				</div>

				<!-- Panel Forum -->
				<div class="panel-heading">Forum</div>
				<div class="panel-body">
					@if(Auth::user()->Urole==1)
					<div class="row text-center"><a href="{{ url('/forum') }}" class="btn btn-primary">Lihat semua</a></div><br>
					@endif
					
					@foreach($forum as $forum)
					<div class="row">
							<div class="col-sm-8 text-left"><a href='{{ action('ThreadController@show', $forum->IdForum) }}'><h5>{{ $forum->Nama}}</h5></a></div>
					</div>
					@endforeach
				</div>
				<!-- End Panel Forum -->
			</div>
		</div>

		<!-- Panel Laporan Kegiatan -->
		<div class="col-md-8">
			<div class="panel panel-default">
				<div class="panel-heading">Laporan Kegiatan</div>
				<div class="panel-body">
					@foreach($lap as $lap)
						<table style="width:100%">
						<tr>
							<td class="text-center"><h3><b>{{ $lap->Aktivitas }}<b></h3></td>
						</tr>
						<table>
						<table>
						@if(substr($lap->Attachment, -3)=="jpg" || substr($lap->Attachment, -3)=="png"  || substr($lap->Attachment, -4)=="jpeg" || substr($lap->Attachment, -3)=="gif")
						<tr><br><div style="text-align:center"><img src="{{ asset($lap->Attachment) }}" style='height:400px'></id></tr>
						@else
						<tr><br><div style="text-align:center"><img src="{{ asset('css/img/doc.png') }}" style='height:150px'></id></tr>
						@endif
						<tr class="text-left"><br><h5><?php echo nl2br(str_limit($lap->Deskripsi, $limit=300, $end= '...')); ?></h5>
						<a href="{{ url('/laporan/'.$lap->IdLap)}}" class="read-more">Selengkapnya</a></tr>
						</table>
					@endforeach
					<a href="{{ url('/laporan') }}" class="text-right"><h4>Lihat Semua Laporan ...</h4></a>
				</div>
			</div>
		</div>
		<!-- End Panel Laporan Kegiatan -->

		<!-- Panel Agenda -->
		<div class="col-md-2">
			<div class="panel panel-default">
				<div class="panel-heading">Agenda</div>				
				<div class="panel-body">
					@if(in_array(Auth::user()->Urole,array(1,2,4)))	
						<h4><div class="text-center">							
							<a href='{{ url('/agenda') }}' class="btn btn-primary">Lihat semua</a>
						</div></h4>
					@endif

					<?php foreach($agenda as $agen): ?>
					<div class="panel">											
						<table>
							<tr>
								<a href='{{ url('/agenda/'.$agen->IdAgenda) }}'><b>{{ $agen->Aktivitas}}</b></a><br>
								<h6>{{ date("l, d M Y",strtotime($agen->Waktu))}}</h6>
							</tr>
						</table>				
					</div>
					<?php endforeach; ?>
					<?php echo $agenda->render(); ?>
				</div>
			</div>
		</div>

		<!-- End Panel Agenda -->
@endsection

@section('js')

@endsection
