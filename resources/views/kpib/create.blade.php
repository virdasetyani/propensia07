@extends('app')
@extends('header')

@section('content')
<div class="container-fluid">
	<div class="row">
				<div class="col-md-10 col-md-offset-1">
	<ol class="breadcrumb">
 		<li><a href="{{ url('/home') }}">Beranda</a></li>
 		<li><a href="{{ url('/kpib') }}">KPI Bidang</a></li>
 		<li class="active">Buat</li>
	</ol>
	</div>

		<div class="col-md-8 col-md-offset-2">
			<p>Term : Bulan {{ $term->Bulan }} , Tahun {{ $term->Tahun}}</p>

			<div class="alert alert-info"><h4><center>Pastikan Anda mengisi semua kolom yang tersedia!</center></h4></div>

			<div class="panel panel-default">
		
				<div class="panel-heading"><center>Tambah KPI Bidang</center></div>
				<div class="panel-body">

				@if (Session::has('message'))
   					<div class="alert alert-success">{{ Session::get('message') }}</div>
   				@elseif (Session::has('warning'))
   				   	<div class="alert alert-danger">{{ Session::get('warning') }}</div>
				@endif

	<form class="form-horizontal" role="form" method="POST" action="{{ url('/kpib/create') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="IdTerm" value="{{ $term->IdTerm }}">
		
		<center>
		<h4>Pilih Bidang</h4>
		<select name="IdBid">
			@foreach($data as $bid)
			<option value="{{ $bid->IdBid }}"> {{ $bid->NamaBid }}</option>
			@endforeach
		</select>
		</center>

	<div class="form-group">
		<div class="col-md-6 col-md-offset-9">
		<div id="add-indikator" class="btn btn-primary">
		+Add Indikator
		</div>
		</div>
	</div>
	<div id="indikators">
		<div class="indikator">
			<table><tr><th>Indikator 1</th></tr></table>
			<div class="form-group">
				<label class="col-md-4 control-label">Indikator</label>
				<div class="col-md-6">
				<textarea class="form-control" name="Indikator[]" value="{{ old('Indikator') }}"></textarea>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-4 control-label">Target</label>
				<div class="col-md-6">
				<textarea class="form-control" name="Target[]" value="{{ old('Target') }}"></textarea>
				</div>
			</div>

			<!--<div class="form-group">
				<label class="col-md-4 control-label">Aktual</label>
				<div class="col-md-6">
				<textarea type="text" class="form-control" name="Aktual[]" value="{{ old('Aktual') }}"></textarea>
				</div>
			</div>-->

			<div class="form-group">
				<label class="col-md-4 control-label">Persentase Target</label>
				<div class="col-md-3">
				<input type="number" min="0" max="100" step="1" class="form-control" name="PersenTarget[]" value="{{ old('PersenTarget') }}" placeholder="100">
				</div>
				<div class="col-md-4"><h5>% (Persen)</h5></div>
			</div>

			<!--<div class="form-group">
				<label class="col-md-4 control-label">Persentase Aktual</label>
				<div class="col-md-3">
				<input type="number" min="0" max="100" step="1" class="form-control" name="PersenAktual[]" value="{{ old('PersenAktual') }}" placeholder="80">
				</div>
				<div class="col-md-4"><h5>% (Persen)</h5></div>
			</div>-->
		</div>
</div>

	<div class="form-group">
		<div class="col-md-6 col-md-offset-4">
		<button type="submit" class="btn btn-primary">
		Simpan
		</button>
		</div>
	</div>
	
	</div>
	</form>
@endsection

@section('js')
<script>
	$(document).ready(function(){
		$("#add-indikator").click(function(){
			var count = $('.indikator').size()+1;
			if(count == 6){
				alert("Anda hanya bisa memasukkan per 5 indikator!");
			}else{
				$("#indikators").append('<p><div class="indikator"><table><tr><th>Indikator '+count+'</th></tr></table><div class="form-group"><label class="col-md-4 control-label">Indikator</label><div class="col-md-6"><textarea class="form-control" name="Indikator[]"></textarea></div></div><div class="form-group"><label class="col-md-4 control-label">Target</label><div class="col-md-6"><textarea class="form-control" name="Target[]"></textarea></div></div><div class="form-group"><label class="col-md-4 control-label">Persentase Target</label><div class="col-md-3"><input type="number" min="0" max="100" step="1" class="form-control" name="PersenTarget[]" placeholder="100"></div><div class="col-md-4"><h5>% (Persen)</h5></div></div></p>');
				//count++;
				//<br><a href="#" id="delete-indikator" class="col-md-8 col-md-offset-9">Remove Indikator '+count+'</a>
			}
				
		});
		$("#delete-indikator").click(function(){
			$(this).parents('p').remove();
			//count--;
			//$("#indikators").remove();
		});
	});

</script>
@endsection