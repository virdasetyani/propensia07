@extends('app')
@extends('header')

@section('content')
<div class="col-md-10 col-md-offset-1">
	<ol class="breadcrumb">
 		<li><a href="{{ url('/home') }}">Beranda</a></li>
 		<li><a href="{{ url('/kpib') }}">KPI Bidang</a></li>
 		<li class="active">Ubah</li>
	</ol>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Update Indikator KPI Bidang</div>
				<div class="panel-body">

	@if (Session::has('message'))
   					<div class="alert alert-success">{{ Session::get('message') }}</div>
				@endif

	<form class="form-horizontal" role="form" method="POST" action="{{ action('KPIBidangController@update') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="IdKpi" value="{{ $data->IdKpi }}">



	<div class="form-group">
		<label class="col-md-4 control-label">Indikator</label>
		<div class="col-md-6">
		<textarea class="form-control" name="Indikator">{{ $data->Indikator }}</textarea>
		</div>
	</div>

	<div class="form-group">
		<label class="col-md-4 control-label">Target</label>
		<div class="col-md-6">
		<textarea class="form-control" name="Target">{{ $data->Target }}</textarea>
		</div>
	</div>

	<div class="form-group">
		<label class="col-md-4 control-label">Persentase Target</label>
		<div class="col-md-6">
		<input type="text" class="form-control" name="PersenTarget" value="{{ $data->PersenTarget }}">
		</div>
	</div>

	<div class="form-group">
		<div class="col-md-6 col-md-offset-4">
		<button type="submit" class="btn btn-primary">
		Ubah Indikator
		</button>
		</div>
	</div>
	
	</div>
	</form>
@endsection