@extends('app')
@extends('header')

@section('content')
<div class="container-fluid">
	<div class="col-md-10 col-md-offset-1">

	<ol class="breadcrumb">
 		<li><a href="{{ url('/home') }}">Beranda</a></li>
 		<li><a href="{{ url('/kpib') }}">KPI Bidang</a></li>
 		<li class="active">Buat</li>
	</ol>
</div>
	

		<div class="col-md-8 col-md-offset-2">

		<form method="GET" role="form" action="{{ url('/kpib/create') }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<p>Pilih Term : 
			<select name="IdTerm">
				@foreach($data as $term)
				<option value="{{ $term->IdTerm }}"> {{ $term->Bulan }}  {{ $term->Tahun }}</option>
				@endforeach
			</select>
			
			<button type="submit" value="submit">Pilih</button></p>
		</form>

		
	</div>
</div>
</div>
@endsection

