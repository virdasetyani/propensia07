@extends('app')
@extends('header')

@section('content')
<div class="col-md-10 col-md-offset-1">
	<ol class="breadcrumb">
 		<li><a href="{{ url('/home') }}">Beranda</a></li>
 		<li><a href="{{ url('/kpib') }}">KPI Bidang</a></li>
 		<li class="active">Ubah</li>
	</ol>
</div><br><br>

<div class="jumbroton"><center><h3>Daftar KPI Bidang</h3></center></div>
	
	
	<form method="POST" role="form" action="{{ url('/kpib/view') }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="col-md-10 col-md-offset-1">
				<p>Pilih Bidang  : 
			<select name="IdBid">
				@foreach($bid as $bid)
				
					<option value="{{ $bid->IdBid }}">  {{ $bid->NamaBid }}</option>
				@endforeach
			</select>
			<button type="submit" value="submit">Pilih</button></p></div>
		</form>

	@if(Session::has('data'))
	<?php $data = Session::get('data'); ?>
	<center>
		<br>
	<div class="col-md-10 col-md-offset-1">
	<div class="panel panel-default">

	<table class="table-bordered" style="width:100%">
		<thead>
			<tr>
				<th style="text-align:center">No</th>
				<th style="text-align:center">Indikator</th>
				<th style="text-align:center">Target</th>
				<th style="text-align:center">Persentase Target</th>
				<th style="text-align:center">Isi</th>
				<th style="text-align:center">Ubah</th>
			</tr>
		</thead>
		
		<tbody>
			<?php $i=1; ?>
			@foreach($data as $data)
			<tr>
				<td style="text-align:left"><?php echo $i ?></td>
				<td style="text-align:left">{{ $data->Indikator }}</td>
				<td style="text-align:left">{{ $data->Target }}</td>
				<td style="text-align:center">{{ $data->PersenTarget }}</td>

				@if($data->IsApproved)
				<td><div class="col-sm-pull-1"><a href='{{ action('KPIBidangController@isi',$data->IdKpi  )}}' class="btn btn-primary btn-xs">Isi</a></div></td>				
				@else
				<td><span class="glyphicon glyphicon-remove alert-danger"></span></td>
				@endif
				@if(!$data->IsApproved)
				<td><div class="col-sm-pull-1"><a href='{{ action('KPIBidangController@edit',$data->IdKpi ) }}' class="btn btn-primary btn-xs">Ubah</a></div></td>
				@else
				<td><span class="glyphicon glyphicon-remove alert-danger"></span></td>
				@endif
				
			</tr>
			<?php $i++; ?>
			@endforeach
		</tbody>
	</table>
</div>
</div>
@endif
</div>
@endsection