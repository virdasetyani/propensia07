@extends('app')
@extends('header')

@section('content')

	<div class="col-md-10 col-md-offset-1">

	<ol class="breadcrumb">
 		<li><a href="{{ url('/home') }}">Beranda</a></li>
 		<li><a href="{{ url('/kpip') }}">KPI Pegawai</a></li>
 		<li class="active">Setujui</li>
	</ol>
</div>
<br><br>

<div class="jumbroton"><center><h3>Setujui KPI Pegawai</h3></center></div>
	<div class="col-md-10 col-md-offset-1">
		@if($data->isEmpty())
				<div class="alert alert-danger"><h4>Anda tidak memiliki bawahan!</h4></div>
			@else
	<div class="panel panel-default">
		
			
	<table class="table">
		<thead>
			<tr>
				<th style="text-align:center">Nomor Pegawai</th>
				<th style="text-align:center">Nama Pegawai</th>
			</tr>
			
		</thead>
		@endif
		<tbody>
			@foreach($data as $kpi)
			<tr>
				<td class="col-md-3">{{ $kpi->IdPeg }}</td>
				<td><a href='{{ action('KPIPegawaiController@viewKpiSup', $kpi->IdPeg) }}'>{{ $kpi->Nama }}</a></td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
</div>
</div>
@endsection