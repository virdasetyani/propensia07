@extends('app')
@extends('header')

@section('content')
<?php $idT; ?>
<div="row">
<div class="col-md-10 col-md-offset-1">

	<ol class="breadcrumb">
 		<li><a href="{{ url('/home') }}">Beranda</a></li>
 		<li><a href="{{ url('/kpip') }}">KPI Pegawai</a></li>
 		<li><a href="{{ url('/kpi/sup') }}">Setujui</a></li>
 		<li class="active">{{ $pegawai->Nama }}</li>
	</ol>
</div>
</div><br><br>

<center><h3>Setujui KPI Pegawai</h3></center>

<div class="row-md-2"><h2><center>{{ $pegawai->IdPeg }} - {{ $pegawai->Nama }}</center></h2>

	@if (Session::has('devplan'))
   		<div class="alert alert-success">{{ Session::get('devplan') }}</div>
	@endif

<div class="col-md-10 col-md-offset-1">
<div class="panel panel-warning">
    <div class="panel-heading">
        <h3 class="panel-title font-bold"><span class="glyphicon glyphicon-alert"></span>INFORMASI </h3>
    </div>
    <div class="panel-body">
        <span>
            <i class="glyphicon glyphicon-chevron-right chevron-right-color"></i>
            KPI Pegawai hanya dapat diisi apabila sudah disetujui Supervisor.
        </span>
        <br />
        <i class="glyphicon glyphicon-chevron-right chevron-right-color"></i>
            <span>KPI Pegawai yang sudah disetujui tidak dapat diubah. 
            </span>
            <br />
        <i class="glyphicon glyphicon-chevron-right chevron-right-color"></i>
            <span>Status <span class="glyphicon glyphicon-remove alert-danger"></span> : Belum Disetujui
            </span>
            <br />
        <i class="glyphicon glyphicon-chevron-right chevron-right-color"></i>
            <span>Status <span class="glyphicon glyphicon-ok alert-succes"></span> : Sudah Disetujui
            </span>
            <br />
    </div>
</div>
</div></div>

<div class="col-md-10 col-md-offset-1">

<div class="jumbroton"><center><h4>Indikator KPI Pegawai</h4></center></div>


	<div class="col-md-10 col-md-offset-1">

	<div class="panel panel-default">
	<table class="table">
		<thead>
			<tr>
				<th style="text-align:center">No</th>
				<th style="text-align:center">Indikator</th>
				<th style="text-align:center">Target</th>
				<th style="text-align:center">Persentase Target</th>
				<th style="text-align:center">Status</th>
			</tr>
		</thead>
		<tbody>
			<?php $i=1; ?>
			@foreach($kpi as $kpi)
			<tr>
				<td><?php echo $i ?></td>
				<td>{{ $kpi->Indikator }}</td>
				<td>{{ $kpi->Target }}</td>
				<td>{{ $kpi->PersenTarget }}</td>
				<td>@if(!$kpi->IsApproved)
				<a href='{{ action('KPIPegawaiController@approveInd', $kpi->IdKpi) }}' class="btn btn-default">Setuju</a>
				@else
				<a href='{{ action('KPIPegawaiController@approveInd', $kpi->IdKpi) }}' class="btn btn-danger">Tidak Setuju</a>
				@endif</td>
			</tr>
			<?php $i++; ?>
			@endforeach
		</tbody>
	</table>
</div>
</div>
</div>


<div class="col-md-10 col-md-offset-1 text-center"><h4>KPI Pegawai</h4>

<form method="POST" role="form" action="{{ url('/kpi/approve') }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="Id" value="{{ $pegawai->IdPeg }}">
			<center><p>Pilih Term : 
			<select name="IdTerm">
				@foreach($term as $term)
				<option value="{{ $term->IdTerm }}"> {{ $term->Bulan }}  {{ $term->Tahun }}</option>
				@endforeach
			</select>
			<button type="submit" value="submit">Pilih</button></p></center>
		</form>
</div>

@if(Session::has('data'))
	<?php $data = Session::get('data'); ?>

	<table class="table-bordered" style="width:90%; margin: 0px auto;">
		<thead>
			<tr>
				<th style="text-align:center">No</th>
				<th style="text-align:center; width:30%;" >Indikator</th>
				<th style="text-align:center; width:20%;" >Target</th>
				<th style="text-align:center">Target(%)</th>
				<th style="text-align:center; width:10%;">Status</th>
				<th style="text-align:center; width:20%;">Aktual</th>
				<th style="text-align:center">Aktual(%)</th>
				<th style="text-align:center">Capaian(%)</th>
				<th style="text-align:center; width:30%;">Notes</th>
				<th style="text-align:center">Status</th>
				<th style="text-align:center">Setuju</th>
				<th style="text-align:center">Ubah</th>
			</tr>
		</thead>
		<tbody>
		@if($data != null)
			<?php $i=1; ?>
			@foreach($data as $data)
			<tr>
				<td style="text-align:left"><?php echo $i ?></td>
				<td style="text-align:left">{{ $data->Indikator }}</td>
				<td style="text-align:left">{{ $data->Target }}</td>
				<td style="text-align:center">{{ $data->PersenTarget }}</td>
				<td style="text-align:center">@if (!$data->IsApproved)
					<span class="glyphicon glyphicon-remove alert-danger"></span>
					@else
					<span class="glyphicon glyphicon-ok alert-succes"></span>
					@endif</td>
				<td style="text-align:left">{{ $data->Aktual }}</td>
				<td style="text-align:center">{{ $data->PersenAktual }}</td>
				<td style="text-align:center">{{ $data->PersenAchievement }}</td>
				<td style="text-align:left">{{ $data->Notes }}</td>
				<td style="text-align:center"> @if (!$data->IsDone)
						<span class="glyphicon glyphicon-remove alert-danger"></span>
					@else
						<span class="glyphicon glyphicon-ok alert-succes"></span>
					@endif</td>
				<td>@if (!$data->IsDone)
					<a href='{{ action('KPIPegawaiController@approveKpip', $data->IdKpiPeg) }}' class="btn btn-default btn-xs">Setuju</a>
				@else
				<a href='{{ action('KPIPegawaiController@approveKpip', $kpi->IdKpiPeg) }}' class="btn btn-danger btn-xs">Tidak Setuju</a>
				@endif</td>
				<td>@if ($data->IsApproved)
					<a href='{{ url('kpip/approve/'.$data->KPegawai .'/' .$data->IdKpi . '/' .$data->KTerm ) }}' class="btn btn-default btn-xs">Ubah</a></td> 
				@endif
			</tr>
			<form method="POST" role="form" action="{{ url('/kpi/approve/sup') }}">
				<input type="hidden" name="IdTerm" value="{{ $data->KTerm }}">
			<?php $i++; ?>
			@endforeach
		</tbody>
	</table>
</div><br><br>
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="Id" value="{{ $data->KPegawai }}">
	<center><button type="submit" value="submit" class="btn btn-info">Ubah Rencana Pengembangan</button></p></center>
</form>
</div>
@endif
@endif
@endsection