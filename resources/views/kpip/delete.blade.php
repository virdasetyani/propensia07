@extends('app')
@extends('header')

@section('content')

<div class="col-md-10 col-md-offset-1">
	<ol class="breadcrumb">
 		<li><a href="{{ url('/home') }}">Beranda</a></li>
 		 <li><a href="{{ url('/kpip') }}">KPI Pegawai</a></li>
 		<li class="active">Hapus</li>
	</ol>
</div>
<br><br>

<div class="jumbroton"><center><h3>Daftar KPI Pegawai</h3></center></div>
	
	<form method="POST" role="form" action="{{ url('/kpi/delete') }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<center><p>Pilih Term : 
			<select name="IdTerm">
				@foreach($term as $term)
				<option value="{{ $term->IdTerm }}"> {{ $term->Bulan }}  {{ $term->Tahun }}</option>
				@endforeach
			</select>
			<button type="submit" value="submit">Pilih</button></p></center>
		</form>
	</div>
				@if (Session::has('message'))
				<div class="col-md-8 col-md-offset-2">
					<div class="panel-body">
   						<div class="alert alert-success">{{ Session::get('message') }}</div>
   					</div>
   				</div>
				@endif

	@if(Session::has('data'))
	<?php $data = Session::get('data'); ?>
	<?php $i=1; ?>
	<center>
		<br>
	<div class="col-md-10 col-md-offset-1">
	<div class="panel panel-default">

	<table class="table-bordered" style="width:100%">
		<thead>
			<tr>
				<th style="text-align:center">No</th>
				<th style="text-align:center; width:30%;" >Indikator</th>
				<th style="text-align:center; width:20%;" >Target</th>
				<th style="text-align:center">Target(%)</th>
				<th style="text-align:center; width:10%;">Status</th>
				<th style="text-align:center; width:20%;">Aktual</th>
				<th style="text-align:center">Aktual(%)</th>
				<th style="text-align:center">Capaian(%)</th>
				<th style="text-align:center; width:30%;">Notes</th>
				<th>Status</th>
				<th>Hapus</th>
			</tr>
		</thead>
		<tbody>
			@for ($j = 0; $j < count($data); $j++)
			<tr>
				<td style="text-align:left"><?php echo $i ?></td>
				<td style="text-align:left">{{ $data[$j]['Indikator'] }}</td>
				<td style="text-align:left">{{ $data[$j]['Target'] }}</td>
				<td style="text-align:center">{{ $data[$j]['PersenTarget'] }}</td>
				<td style="text-align:center">@if (!$data[$j]['IsApproved'])
						<span class="glyphicon glyphicon-remove alert-danger"></span>
					@else
						<span class="glyphicon glyphicon-ok alert-succes"></span>
					@endif</td>
				<td style="text-align:left">{{ $data[$j]['Aktual'] }}</td>
				<td style="text-align:center">{{ $data[$j]['PersenAktual'] }}</td>
				<td style="text-align:center">{{ $data[$j]['PersenAchievement'] }}</td>
				<td style="text-align:left">{{ $data[$j]['Notes'] }}</td>
				<td style="text-align:center"> @if (!$data[$j]['IsDone'])
						<span class="glyphicon glyphicon-remove alert-danger"></span>
					@else
						<span class="glyphicon glyphicon-ok alert-succes"></span>
					@endif</td>
				<td><a href='{{ action('KPIPegawaiController@confirm', $data[$j]['IdKpiPeg']) }}' class="btn btn-info btn-xs">Hapus</a></td>
			</tr>
			@endfor
		</tbody>
	</table>
	</div>
</div>
	@endif
</div>
</div>
</div>
@endsection