@extends('app')
@extends('header')
@section('content')

<div class="container-fluid">
	<div class="row">

	<div class="col-md-10 col-md-offset-1">
	<ol class="breadcrumb">
 		<li><a href="{{ url('/home') }}">Beranda</a></li>
 		<li><a href="{{ url('/kpip') }}">KPI Pegawai</a></li>
 		<li class="active">Rencana Pengembangan</li>
	</ol>
	</div>


		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Buat Rencana Pengembangan</div>
				<div class="panel-body">

					@if (Session::has('devplan'))
   						<div class="alert alert-success">{{ Session::get('devplan') }}</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{ url('/kpi/devplan')}}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="IdTerm" value="{{ $id }}">

					<div class="form-group">
						<label class="col-md-4 control-label">Rencana Pegawai</label>
						<div class="col-md-6">
							<textarea class="form-control" name="PlanPegawai" rows="5" cols="20" ></textarea>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">Rencana Supervisi</label>
						<div class="col-md-6">
							<textarea class="form-control" name="PlanSuper" rows="5" cols="20" disabled></textarea>
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-6 col-md-offset-4">
						<button type="submit" class="btn btn-primary">Simpan</button>
						</div>
					</div>
				
					</form>
	
				</div>
			</div>
		</div>
	</div>
</div>

@endsection