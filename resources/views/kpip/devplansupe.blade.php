@extends('app')
@extends('header')
@section('content')

<div class="container-fluid">
	<div class="row">


	<div class="col-md-10 col-md-offset-1">
	<ol class="breadcrumb">
 		<li><a href="{{ url('/home') }}">Beranda</a></li>
 		<li><a href="{{ url('/kpi/sup') }}">Setujui KPI Pegawai</a></li>
 		<li><a href="{{ url('/kpi/approve/'.$peg->IdPeg) }}">{{ $peg->Nama }}</a></li>
 		<li class="active">Rencana Pengembangan</li>
	</ol>
	</div>

		<div class="col-md-8 col-md-offset-2">

				<blockquote>
  						<p>{{ $test->PlanPegawai }}</p>
  						<footer>Rencana Pengembangan oleh<cite title="Source Title"> Pegawai</cite></footer>
					</blockquote>

					<blockquote class="blockquote-reverse">
 						<p>{{ $test->PlanSupervisi }}</p>
  						<footer>Rencana Pengembangan oleh<cite title="Source Title"> Saya</cite></footer>
					</blockquote>
				
			<div class="panel panel-default">

				<div class="panel-heading">Ubah Rencana Pengembangan</div>
				<div class="panel-body">
					@if (Session::has('devplan'))
   						<div class="alert alert-success">{{ Session::get('devplan') }}</div>
					@endif

					
					<form class="form-horizontal" role="form" method="POST" action="{{ url('/kpi/devplan/sup')}}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="IdTerm" value="{{ $test->DTerm }}">
						<input type="hidden" name="IdPeg" value="{{ $test->IdPeg }}">
						<input type="hidden" name="Id" value="{{ $test->Id }}">

					<div class="form-group">
						<label class="col-md-4 control-label">Rencana Pegawai</label>
						<div class="col-md-6">
							<textarea class="form-control" name="PlanPegawai" rows="5" cols="20" disabled>{{ $test->PlanPegawai }}</textarea>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">Rencana Supervisi</label>
						<div class="col-md-6">
							<textarea class="form-control" name="PlanSuper" rows="5" cols="20">{{ $test->PlanSupervisi }}</textarea>
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-6 col-md-offset-4">
						<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</div>
				
					</form>
	
				</div>
			</div>
		</div>
	</div>
</div>

@endsection