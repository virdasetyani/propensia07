@extends('app')
@extends('header')

@section('content')
<div class="container-fluid">
	<div class="row">

	<div class="col-md-10 col-md-offset-1">
		<ol class="breadcrumb">
 			<li><a href="{{ url('/home') }}">Beranda</a></li>
 			<li><a href="{{ url('/kpip') }}">KPI Pegawai</a></li>
 			<li><a href="{{ url('/kpi/view') }}">Ubah/Isi</a></li>
 			<li class="active">Ubah</li>
		</ol>
	</div>

		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Ubah Indikator KPI Pegawai</div>
				<div class="panel-body">

				@if (Session::has('message'))
   					<div class="alert alert-success">{{ Session::get('message') }}</div>
				@endif


	<form class="form-horizontal" role="form" method="POST" action="{{ action('KPIPegawaiController@update') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="IdKpi" value="{{ $data->IdKpi }}">

	<div class="form-group">
		<label class="col-md-4 control-label">Indikator</label>
		<div class="col-md-6">
		<textarea class="form-control" name="Indikator">{{ $data->Indikator }}</textarea>
		</div>
	</div>

	<div class="form-group">
		<label class="col-md-4 control-label">Target</label>
		<div class="col-md-6">
		<textarea class="form-control" name="Target">{{ $data->Target }}</textarea>
		</div>
	</div>

	<!--<div class="form-group">
		<label class="col-md-4 control-label">Aktual</label>
		<div class="col-md-6">
		<textarea type="text" class="form-control" name="Aktual">{{ $data->Aktual }}</textarea>
		</div>
	</div>-->

	<div class="form-group">
		<label class="col-md-4 control-label">Persentase Target</label>
		<div class="col-md-6">
		<input type="text" class="form-control" name="PersenTarget" value="{{ $data->PersenTarget }}">
		</div>
	</div>

	<!--<div class="form-group">
		<label class="col-md-4 control-label">Persentase Aktual</label>
		<div class="col-md-6">
		<input type="text" class="form-control" name="PersenAktual" value="{{ $data->PersenAktual }}">
		</div>
	</div>-->

	<div class="form-group">
		<div class="col-md-6 col-md-offset-4">
		<button type="submit" class="btn btn-primary">
		Ubah Indikator
		</button>
		</div>
	</div>
	
	</div>
	</form>
@endsection