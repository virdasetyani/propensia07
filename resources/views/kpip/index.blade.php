@extends('app')
@extends('header')

@section('content')
<div class="container-fluid">
	<div class="row">

	<div class="col-md-10 col-md-offset-1">
	<ol class="breadcrumb">
 		<li><a href="{{ url('/home') }}">Beranda</a></li>
 		<li><a href="{{ url('/kpip') }}">KPI Pegawai</a></li>
 		<li class="active">Buat</li>
	</ol>
	</div>

		<div class="col-md-8 col-md-offset-2">

		<form method="GET" role="form" action="{{ url('/kpip/create') }}">
	
			<p>Pilih Term : 
			<select name="IdTerm">
				@foreach($data as $term)
				<option value="{{ $term->IdTerm }}"> {{ $term->Bulan }}  {{ $term->Tahun }}</option>
				@endforeach
			</select>
			<button type="submit" value="submit">Pilih</button></p>
		</form>

		</div>
	</div>
</div>
</div>
@endsection

