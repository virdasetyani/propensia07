@extends('app')
@extends('header')

@section('content')
<div class="container-fluid">
	<div class="row">

	<div class="col-md-10 col-md-offset-1">
		<ol class="breadcrumb">
 			<li><a href="{{ url('/home') }}">Beranda</a></li>
 			<li><a href="{{ url('/kpip') }}">KPI Pegawai</a></li>
 			<li><a href="{{ url('/kpi/view') }}">Ubah/Isi</a></li>
 			<li class="active">Isi</li>
		</ol>
	</div>

		<div class="col-md-8 col-md-offset-2">

			<div class="panel panel-default">

				<div class="panel-heading">Indikator KPI Pegawai</div>
				<div class="panel-body">

				@if (Session::has('message'))
   					<div class="alert alert-success">{{ Session::get('message') }}</div>
				@endif

	<form class="form-horizontal" role="form" method="POST" action="{{ url('/kpip/isi') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="IdKpi" value="{{ $data->IdKpi }}">

	<div class="form-group">
		<label class="col-md-4 control-label">Indikator</label>
		<div class="col-md-6">
		{{ $data->Indikator }}
		</div>
	</div>

	<div class="form-group">
		<label class="col-md-4 control-label">Target</label>
		<div class="col-md-6">
		{{ $data->Target }}
		</div>
	</div>

	

	<div class="form-group">
		<label class="col-md-4 control-label">Persentase Target</label>
		<div class="col-md-6">
		{{ $data->PersenTarget }}
		</div>
	</div>
</div>

	<div class="panel-heading">Isi Indikator KPI Pegawai</div>
	<div class="panel-body">

	<div class="form-group">
	<label class="col-md-4 control-label">Pilih Term : </label>
	<div class="col-md-6">
			<select name="IdTerm">
				@foreach($term as $term)
				<option value="{{ $term->IdTerm }}"> {{ $term->Bulan }}  {{ $term->Tahun }}</option>
				@endforeach
			</select>
			<button type="submit" value="submit">Pilih</button>
	</div>
	</div>
	<br>
	</form>

	<form class="form-horizontal" role="form" method="POST" action="{{ action('KPIPegawaiController@updateIsi') }}">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	
	@if (Session::has('oke'))
	<?php $oke = Session::get('oke'); ?>

	<input type="hidden" name="IdKpi" value="{{ $oke->IdKpi }}">
		<input type="hidden" name="IdTerm" value="{{ $oke->KPTerm }}">

	<div class="form-group">
		<label class="col-md-4 control-label">Aktual</label>
		<div class="col-md-6">
		<textarea type="text" class="form-control" name="Aktual">{{ $oke->Aktual }}</textarea>
		</div>
	</div>

	<div class="form-group">
		<label class="col-md-4 control-label">Persentase Aktual</label>
		<div class="col-md-6">
		<input type="text" class="form-control" name="PersenAktual" value="{{ $oke->PersenAktual }}">
		</div>
	</div>

	<div class="form-group">
		<div class="col-md-6 col-md-offset-4">
		<button type="submit" class="btn btn-primary">
		Isi KPI
		</button>
		</div>
	</div>
	@endif

	@if (Session::has('term'))
	<?php $term = Session::get('term'); ?>

	<input type="hidden" name="IdTerm" value="{{ $term[0] }}">
	<input type="hidden" name="IdKpi" value="{{ $term[1] }}">


	<div class="form-group">
		<label class="col-md-4 control-label">Aktual</label>
		<div class="col-md-6">
		<textarea type="text" class="form-control" name="Aktual"></textarea>
		</div>
	</div>

	<div class="form-group">
		<label class="col-md-4 control-label">Persentase Aktual</label>
		<div class="col-md-6">
		<input type="text" class="form-control" name="PersenAktual" value="{{ old('PersenAktual') }}">
		</div>
	</div>

	<div class="form-group">
		<div class="col-md-6 col-md-offset-4">
		<button type="submit" class="btn btn-primary">
		Isi KPI
		</button>
		</div>
	</div>
	@endif

	
	</div>
	</form>
@endsection