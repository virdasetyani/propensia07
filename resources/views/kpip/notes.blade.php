@extends('app')
@extends('header')

@section('content')
<div class="container-fluid">
	<div class="row">

	<div class="col-md-10 col-md-offset-1">
		<ol class="breadcrumb">
 			<li><a href="{{ url('/home') }}">Beranda</a></li>
 			<li><a href="{{ url('/kpip') }}">KPI Pegawai</a></li>
 			<li><a href="{{ url('/kpi/sup') }}">Setujui</a></li>
 			<li><a href="{{ url('/kpi/approve/'. $user->IdPeg) }}">{{ $user->Nama }}</a></li>
 			<li class="active">Ubah</li>
		</ol>
	</div>

		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Setujui KPI Pegawai</div>
				<div class="panel-body">

	@if (Session::has('approve'))
   		<div class="alert alert-success">{{ Session::get('approve') }}</div>
	@endif

	<form class="form-horizontal" role="form" method="POST" action="{{ action('KPIPegawaiController@feedback')}}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="IdKpi" value="{{ $data->IdKpi }}">
		<input type="hidden" name="IdTerm" value="{{ $data->KTerm }}">
		<input type="hidden" name="IdPeg" value="{{ $data->KPegawai }}">

	<div class="form-group">
		<label class="col-md-4 control-label">Indikator</label>
		<div class="col-md-6">{{ $data->Indikator }}</div>
	</div>

	<div class="form-group">
		<label class="col-md-4 control-label">Target</label>
		<div class="col-md-6">{{ $data->Target }}</div>
	</div>

	<div class="form-group">
		<label class="col-md-4 control-label">Aktual</label>
		<div class="col-md-6">{{ $data->Aktual }}</div>
	</div>

	<div class="form-group">
		<label class="col-md-4 control-label">Persentase Target</label>
		<div class="col-md-6">{{ $data->PersenTarget }}</div>
	</div>

	<div class="form-group">
		<label class="col-md-4 control-label">Persentase Aktual</label>
		<div class="col-md-6">{{ $data->PersenAktual }}</div>
	</div>

	<div class="form-group">
		<label class="col-md-4 control-label">Persentase Pencapaian</label>
		<div class="col-md-3">
		<input type="number" min="0" max="100" step="1" class="form-control" name="PersenAchievement" value="{{ $data->PersenAchievement }}" placeholder="100">
		</div>
		<div class="col-md-4"><h5>% (Persen)</h5></div>
	</div>

	<div class="form-group">
		<label class="col-md-4 control-label">Notes</label>
		<div class="col-md-6">
		<textarea class="form-control" name="Notes">{{ $data->Notes }}</textarea>
		</div>
	</div>

	<div class="form-group">
		<div class="col-md-6 col-md-offset-4">
		<button type="submit" class="btn btn-primary">
		Ubah Indikator
		</button>
		</div>
	</div>
	
	</div>
	</form>
@endsection