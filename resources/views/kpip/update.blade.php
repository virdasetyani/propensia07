@extends('app')
@extends('header')

@section('content')


	<?php $i=1; ?>
	<div class="col-md-10 col-md-offset-1">
		<ol class="breadcrumb">
 			<li><a href="{{ url('/home') }}">Beranda</a></li>
 			<li><a href="{{ url('/kpip') }}">KPI Pegawai</a></li>
 			<li class="active">Ubah/Isi</li>
		</ol>
	</div>
	<br><br>

	<div class="jumbroton"><center><h3>Daftar KPI Pegawai</h3></center></div>

	<div class="col-md-10 col-md-offset-1">
<div class="panel panel-warning">
    <div class="panel-heading">
        <h3 class="panel-title font-bold"><span class="glyphicon glyphicon-alert"></span>INFORMASI </h3>
    </div>
    <div class="panel-body">
        <span>
            <i class="glyphicon glyphicon-chevron-right chevron-right-color"></i>
            KPI Pegawai hanya dapat diisi apabila sudah disetujui Supervisor.
        </span>
        <br />
        <i class="glyphicon glyphicon-chevron-right chevron-right-color"></i>
            <span>KPI Pegawai yang sudah disetujui tidak dapat diubah. 
            </span>
            <br />
        <i class="glyphicon glyphicon-chevron-right chevron-right-color"></i>
            <span>Status <span class="glyphicon glyphicon-remove alert-danger"></span> : Tidak Berfungsi
            </span>
            <br />
    </div>
</div>
</div><br><br><br><br><br><br><br>

		@if (Session::has('message'))
		<div class="col-md-8 col-md-offset-2">
			<div class="panel-body">
   				<div class="alert alert-success">{{ Session::get('message') }}</div>
   			</div>
   		</div>
		@endif

	<div class="col-md-10 col-md-offset-1">

	<div class="panel panel-default">
	<table class="table">
		<thead>
			<tr>
				<th style="text-align:center">No</th>
				<th style="text-align:center">Indikator</th>
				<th style="text-align:center">Target</th>
				<th style="text-align:center">Persentase Target</th>
				<th style="text-align:center">Isi</th>
				<th style="text-align:center">Ubah</th>
				<th style="text-align:center">Hapus</th>
			</tr>
		</thead>
		<tbody>
			@foreach($data as $kpi)
			<tr>
				<td><?php echo $i ?></td>
				<td>{{ $kpi->Indikator }}</td>
				<td>{{ $kpi->Target }}</td>
				<td>{{ $kpi->PersenTarget }}</td>
				@if($kpi->IsApproved)
				<td><div class="col-sm-pull-1"><a href='{{ action('KPIPegawaiController@isi', $kpi->IdKpi) }}' class="btn btn-primary">Isi</a></div></td>				
				@else
				<td><span class="glyphicon glyphicon-remove alert-danger"></span></td>
				@endif
				@if(!$kpi->IsApproved)
				<td><div class="col-sm-pull-1"><a href='{{ action('KPIPegawaiController@edit', $kpi->IdKpi) }}' class="btn btn-primary">Ubah</a></div></td>
				@else
				<td><span class="glyphicon glyphicon-remove alert-danger"></span></td>
				@endif
				@if(!$kpi->IsApproved)
				<td><div class="col-sm-pull-1"><a href='{{ action('KPIPegawaiController@confirmInd', $kpi->IdKpi) }}' class="btn btn-primary">Hapus</a></div></td>
				@else
				<td><span class="glyphicon glyphicon-remove alert-danger"></span></td>
				@endif
			</tr>
			<?php $i++; ?>
			@endforeach
		</tbody>
	</table>
	</div>
</div>
</div>
</div>
@endsection