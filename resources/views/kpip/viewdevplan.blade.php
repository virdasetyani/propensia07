@extends('app')
@extends('header')
@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
		<ol class="breadcrumb">
 				<li><a href="{{ url('/home') }}">Beranda</a></li>
 				<li><a href="{{ url('/kpip') }}">KPI Pegawai</a></li>
 				<li class="active">Rencana Pengembangan</li>
			</ol>
		</div>


		<div class="col-md-8 col-md-offset-2">

			

			<div class="panel panel-default">
				<div class="panel-heading">Rencana Pengembangan</div>
				<div class="panel-body">
					<p>Periode : {{ $term->Bulan }} {{ $term->Tahun }}</p>

					<blockquote>
  						<p>{{ $dev->PlanPegawai }}</p>
  						<footer>Development Planning By<cite title="Source Title"> Me</cite></footer>
					</blockquote>

					<blockquote class="blockquote-reverse">
 						<p>{{ $dev->PlanSupervisi }}</p>
  						<footer>Development Planning By<cite title="Source Title"> My Supervisor</cite></footer>
					</blockquote>
					
					</div>
				
				<div class="panel-heading">Ubah Rencana Pengembangan</div>
				<div class="panel-body">

					<center><a href="{{ url('/kpi/devplan/form/' . $dev->DTerm )}}" class="btn btn-info">Ubah Rencana Pengembangan</a></center>
						
				</div>
		</div>
	</div>
</div>

@endsection