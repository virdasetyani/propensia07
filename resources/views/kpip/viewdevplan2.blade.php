@extends('app')
@extends('header')
@section('content')

<div class="container-fluid">
	<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<ol class="breadcrumb">
 			<li><a href="{{ url('/home') }}">Beranda</a></li>
 			<li><a href="{{ url('/kpip') }}">KPI Pegawai</a></li>
 			<li class="active">Rencana Pengembangan</li>
		</ol>
	</div>

		<div class="col-md-8 col-md-offset-2">

			<div class="panel panel-default">
				<div class="panel-heading">Rencana Pengembangan</div>
				<div class="panel-body">
					<p>Periode : {{ $term->Bulan }} {{ $term->Tahun }}</p>
					
					<div class="alert alert-danger">Rencana Pengembangan Belum Dibuat!</div>
				</div>
			</div>

			
					<center><a href="{{ url('/kpi/devplan/form/' . $term->IdTerm )}}" class="btn btn-info">Ubah Rencana Pengembangan</a></center>

		</div>
	</div>
</div>

@endsection