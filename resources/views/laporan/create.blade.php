@extends('app')
@extends('header')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">

		<ol class="breadcrumb">
 			<li><a href="{{ url('/home') }}">Beranda</a></li>
 			<li><a href="{{ url('/laporan') }}">Laporan</a></li>
 			<li class="active">Buat</li>
		</ol>

			

			<div class="panel panel-default">
		
				<div class="panel-heading">Tambah Laporan Kegiatan</div>
				<div class="panel-body">

				
   				@if (Session::has('warning'))
   				   	<div class="alert alert-danger">{{ Session::get('warning') }}</div>
				@endif

	<form class="form-horizontal" role="form" method="POST" action="{{ url('/laporan/create') }}"  enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
	
			<div class="form-group @if ($errors->has('Aktivitas')) has-error @endif">
				<label class="col-md-3 control-label">Judul Kegiatan <font color="red">*</font></label>
				<div class="col-md-7">
				<input type="text" class="form-control" name="Aktivitas" value="{{ old('Aktivitas') }}" style="width:450px;">
				@if ($errors->has('Aktivitas')) <p class="help-block error-alert">{{ $errors->first('Aktivitas')}}</p> @endif
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label">Term</label>
				<div class="col-md-5">
				<select name="IdTerm">
				@foreach($term as $term)
				<option value="{{ $term->IdTerm }}"> {{ $term->Bulan }}  {{ $term->Tahun }}</option>
				@endforeach
				</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label">Regional</label>
				<div class="col-md-5">
				<select name="IdReg">
				@foreach($reg as $reg)
				<option value="{{ $reg->IdReg }}"> {{ $reg->NamaReg }}</option>
				@endforeach
				</select>
				</div>
			</div>

			<div class="form-group @if ($errors->has('Deskripsi')) has-error @endif">
				<label class="col-md-3 control-label">Deskripsi <font color="red">*</font></label>
				<div class="col-md-7">
				<textarea class="form-control" name="Deskripsi" value="{{ old('Deskripsi') }}" rows="30"></textarea>
				<script type="text/javascript" src="<?= url('css/ckeditor/ckeditor.js') ?>"></script>
				<script>CKEDITOR.replace('Deskripsi');</script>
				@if ($errors->has('Deskripsi')) <p class="help-block error-alert">{{ $errors->first('Deskripsi')}}</p> @endif
				</div>
			</div>

			<div class="form-group @if ($errors->has('Target')) has-error @endif">
				<label class="col-md-3 control-label">Target <font color="red">*</font></label>
				<div class="col-md-7">
				<textarea type="text" class="form-control" name="Target" value="{{ old('Aktual') }}"></textarea>
				@if ($errors->has('Target')) <p class="help-block error-alert">{{ $errors->first('Target')}}</p> @endif
				</div>
			</div>

			<div class="form-group @if ($errors->has('Aktual')) has-error @endif">
				<label class="col-md-3 control-label">Aktual <font color="red">*</font></label>
				<div class="col-md-7">
				<textarea type="text" class="form-control" name="Aktual" value="{{ old('Aktual') }}"></textarea>
				@if ($errors->has('Aktual')) <p class="help-block error-alert">{{ $errors->first('Aktual')}}</p> @endif
				</div>
			</div>

			<div class="form-group @if ($errors->has('PersenTarget')) has-error @endif">
				<label class="col-md-3 control-label">Persentase Target <font color="red">*</font></label>
				<div class="col-md-5">
				<input type="number" min="0" max="100" step="1" class="form-control" name="PersenTarget" value="{{ old('PersenTarget') }}" placeholder="100">
				@if ($errors->has('PersenTarget'))<p class="help-block error-alert">{{ $errors->first('PersenTarget')}}</p> @endif
				</div>
				<div class="col-md-3"><h5>% (Persen)</h5></div><br>
			</div>

			<div class="form-group @if ($errors->has('PersenAktual')) has-error @endif">
				<label class="col-md-3 control-label">Persentase Aktual <font color="red">*</font></label>
				<div class="col-md-5">
				<input type="number" min="0" max="100" step="1" class="form-control" name="PersenAktual" value="{{ old('PersenAktual') }}" placeholder="80">
				@if ($errors->has('PersenAktual'))<p class="help-block error-alert">{{ $errors->first('PersenAktual')}}</p> @endif
				</div>
				<div class="col-md-3"><h5>% (Persen)</h5></div><br>

			</div>

			<div class="form-group">
				<label class="col-md-3 control-label">Lampiran</label>
				<div class="col-md-5">
				<input type="file" name="file" value="{{ old('file') }}">
				</div>
			</div>

	<div class="form-group">
		<div class="col-md-6 col-md-offset-4">
		<button type="submit" class="btn btn-primary">
		Simpan
		</button>
		</div>
	</div>

	<div style="float:right"><font color="red" size="2">* harus diisi</font></div>
	
	</div>
	</form>


@endsection