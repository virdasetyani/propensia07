@extends('app')
@extends('header')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">

        <ol class="breadcrumb">
            <li><a href="{{ url('/home') }}">Beranda</a></li>
            <li><a href="{{ url('/laporan') }}">Laporan</a></li>
            <li class="active">{{ $lap->Aktivitas }}</li>
        </ol>

				@if (Session::has('message'))
   					<div class="alert alert-success">{{ Session::get('message') }}</div>
   				@endif
   				
 				@if (Session::has('delete'))
   					<div class="alert alert-success">{{ Session::get('delete') }} . <a href="{{ url('/laporan') }}">View All Laporan</a></div>
   				@else
   				


<article class="row">
	<header>        
		<center>
            <h2><b>{{$lap->Aktivitas}}</b></h2><br>
            <figure>
            	<a class="thumbnail" href="#">
            		 @if(substr($lap->Attachment, -3)=="jpg" || substr($lap->Attachment, -3)=="png" || substr($lap->Attachment, -4)=="jpeg"
                        || substr($lap->Attachment, -3)=="gif")
       				 <img src="{{ asset($lap->Attachment) }}" style='height:500px' class="img-responsive">
       				 @else
       				  <img src="{{ asset('/css/img/doc.png') }}" class="img-responsive">
					  <a href="{{ url('/laporan/download/'.$lap->IdLap) }}">Download : {{ $lap->Aktivitas }} </a>
					  <br>
					@endif
           		 </a>
        	</figure>  
 		</center>
    </header>
    <div class="col-md-4 pull-left alert alert-info">
        <table style="width:100%">
        	<tr>
        		<td style="text-align:left" width="40%">Periode </td>
        		<td width="10%"> : </td>
        		<td style="text-align:left">{{$lap->Bulan}} {{$lap->Tahun}}</td>
        	</tr>
        	<tr>
        		<td style="text-align:left">Regional </td>
        		<td> : </td>
        		<td style="text-align:left">{{$lap->NamaReg}}</td>
        	</tr>
        	<tr>
        		<td style="text-align:left">Target </td>
        		<td> : </td>
        		<td style="text-align:left">{{$lap->Target}}</td>
        	</tr>
        	<tr>
        		<td style="text-align:left">Aktual </td>
        		<td> : </td>
        		<td style="text-align:left">{{$lap->Aktual}}</td>
        	</tr>
        	<tr>
        		<td style="text-align:left">Persentase Target </td>
        		<td> : </td>
        		<td style="text-align:left">{{$lap->PersenTarget}}</td>
        	</tr>
        	<tr>
        		<td style="text-align:left">Persentase Aktual </td>
        		<td> : </td>
        		<td style="text-align:left">{{$lap->PersenAktual}}</td>
        	</tr>
        </table>
    </div>
    <div class="col-md-8 pull-right">
        <p><?php echo nl2br($lap->Deskripsi); ?></p>
    </div>
</article>


@endif
			</div>
		</div>
	</div>
@endsection