@extends('app')
@extends('header')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">

		<ol class="breadcrumb">
 			<li><a href="{{ url('/home') }}">Beranda</a></li>
 			<li class="active">Laporan</li>
		</ol>

				@if (Session::has('delete'))
   				   	<div class="alert alert-success">{{ Session::get('delete') }}</div>
				@endif

			<div class="panel panel-default">


    			<?php foreach ($lap as $oke): ?>
    				<div class="panel-heading">
    				<table style="width:100%"><tr><th width="90%"><h3><b>{{ $oke->Aktivitas }}</b></h3></th>
    					@if(in_array(Auth::user()->Urole,array(1,2,3)))	
						<td width="5%"><a href="{{ url('/laporan/edit/'.$oke->IdLap )}}"><span class="glyphicon glyphicon-pencil"></span></a></td>
						<td width="5%"><a href="{{ url('/laporan/confirm/'.$oke->IdLap )}}"><span class="glyphicon glyphicon-trash"></span></a></td>
						@endif
						</tr>
					</table>
					</div>
					<div class="panel-body">
						<table style="width:100%">
							<tr>
							@if(substr($oke->Attachment, -3)=="jpg" || substr($oke->Attachment, -3)=="png"  || substr($oke->Attachment, -4)=="jpeg" || substr($oke->Attachment, -3)=="gif")
							<td class="col-md-6" width="70%"><center><img src="{{ asset($oke->Attachment) }}" style='height:450px'></center><br></td>
							@else
							<td class="col-md-6" width="70%"><center><img src="{{ asset('css/img/doc.png') }}" style='height:350px'></center><br></td>
							@endif
							<td width="30%">
							<table>
								<tr><td width="10%" class="text-left">Periode</td><td width="5%">:</td><td width="15%" class="text-left">{{$oke->Bulan}} {{$oke->Tahun}}</td></tr>
								<tr><td class="text-left">Regional</td><td width="5%">:</td><td class="text-left"> {{$oke->NamaReg}}</td></tr>
								<tr><td class="text-left">Target</td><td width="5%">:</td><td class="text-left">{{$oke->Target}}</td></tr>
								<tr><td class="text-left">Aktual</td><td width="5%">:</td><td class="text-left">{{$oke->Aktual}}</td></tr>
								<tr><td class="text-left">Persentase Target</td><td width="5%">:</td><td class="text-left">{{$oke->PersenTarget}}</td></tr>
								<tr><td class="text-left">Persentase Aktual</td><td width="5%">:</td><td class="text-left">{{$oke->PersenAktual}}</td></tr>
							</table>
							</td></tr>
							<tr><td colspan="2" class="text-left"><p><?php echo nl2br(str_limit($oke->Deskripsi, $limit=300, $end= '...')); ?>
							<a href="{{ url('/laporan/'.$oke->IdLap)}}" class="read-more">Selengkapnya</a></td></tr>
						</table>
					</div>
    			<?php endforeach; ?>
    		</div>
    	</div>
    </div>
</div>

<center> <?php echo $lap->render(); ?></center>

@endsection