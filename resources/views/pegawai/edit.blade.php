@extends('app')
@extends('header')

@section('content')


<div class="container-fluid">
	<div class="row">

		<div class="col-md-10 col-md-offset-1">
			<ol class="breadcrumb">
 				<li><a href="{{ url('/home') }}">Beranda</a></li>
 				<li><a href="{{ url('/profile/view') }}">Profil : {{ $oke->Nama }}</a></li>
 				<li class="active">Ubah</li>
			</ol>
		</div>


		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Form Edit Profile</div>
				<div class="panel-body">

				@if (Session::has('message'))
   				<div class="alert alert-success">{{ Session::get('message') }}</div>
				@endif

				<p><a href="{{ url('/profile/password/change')}}" class="btn btn-info col-md-offset-10">Ubah Password</a></p>

					
	<form class="form-horizontal" role="form" method="POST" action="{{ action('PegawaiController@update') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group" @if ($errors-> has('Nama')) has-error @endif>
							<label class="col-md-4 control-label">Nama Pengguna</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="Nama" value="{{ $oke->Nama }}">
								@if ($errors -> has ('Nama')) <p class= "help-block error-alert"> {{$errors->first('Nama')}} </p> @endif
							</div>
						</div>

						<div class="form-group" @if ($errors-> has('IdPeg')) has-error @endif>
							<label class="col-md-4 control-label">Nomor Pegawai</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="IdPeg" value="{{ $oke->IdPeg }}">
								@if ($errors -> has ('IdPeg')) <p class= "help-block error-alert"> {{$errors->first('IdPeg')}} </p> @endif
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Jenis Kelamin</label>
							<div class="col-md-6">
								<select name="Gender">
									<option value="Laki-Laki" @if($oke->Gender == "Laki-Laki") selected @endif>Laki-Laki</option>
									<option value="Perempuan" @if($oke->Gender == "Perempuan") selected @endif>Perempuan</option>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Alamat</label>
							<div class="col-md-6">
								<textarea class="form-control" name="Alamat">{{ $oke->Alamat }}</textarea>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Nomor Telepon</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="NoTelp" value="{{ $oke->NoTelp }}">
							</div>
						</div>

						<div class="form-group @if ($errors-> has('Email')) has-error @endif">
							<label class="col-md-4 control-label">Email</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="Email" value="{{ $oke->Email }}">
								@if ($errors -> has ('Email')) <p class= "help-block error-alert"> {{$errors->first('Email')}} </p> @endif
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Supervisor</label>
							<div class="col-md-6">
							<select name="IdSup">
							@if($sup == null)
								@foreach($all as $id)
									<option value="{{ $id->IdPeg }}">{{ $id->Nama }}</option>
								@endforeach
									<option value="0">Tidak Punya Supervisor</option>
							@else
								@foreach($all as $id)
									<option value="{{ $id->IdPeg }}" @if(in_array($id, $sup)) selected @endif>{{ $id->Nama }}</option>
								@endforeach
									<option value="0">Tidak Punya Supervisor</option>
							@endif
							</select>

							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Update
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
