@extends('app')
@extends('header')

@section('content')

<div class="container-fluid">
	<div class="row">

		<div class="col-md-10 col-md-offset-1">
			<ol class="breadcrumb">
 				<li><a href="{{ url('/home') }}">Beranda</a></li>
 				<li><a href="{{ url('/profile/view') }}">Profil : {{ $peg->Nama }}</a></li>
 				<li class="active">Ubah Foto</li>
			</ol>
		</div>

		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Ubah Foto Profil</div>
				<div class="panel-body">

					@if (Session::has('message'))
   					<div class="alert alert-success">{{ Session::get('message') }}</div>
					@endif

					@if($peg->Foto == null)
					<center><img src="{{ asset('/css/img/ava.jpg') }}" style='width:300px;height:300px'></center><br><br>
					@else
					<center><img src="{{ asset($peg->Foto) }}" style='width:300px;height:300px'></center><br><br>
					@endif
					<form class="form-horizontal" role="form" method="POST" action="{{ url('/profile/edit/foto') }}"  enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">

					<center>
					<div class="form-group">
						<input type="file" name="file" value="{{ old('file') }}">
						</div>
					</div>
					</center>
					<center>
					<div class="form-group">
						<button type="submit" class="btn btn-primary">
						Submit
						</button>
					</div>
					</center>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
