@extends('app')
@extends('header')

@section('content')
<div class="container-fluid">
	<div class="row">

		<div class="col-md-10 col-md-offset-1">
			<ol class="breadcrumb">
 				<li><a href="{{ url('/home') }}">Beranda</a></li>
 				<li><a href="{{ url('/profile/view') }}">Profil : {{ $data->Nama }}</a></li>
 				<li class="active">Ubah Password</li>
			</ol>
		</div>



		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Form Ubah Akun Pengguna</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					@if (Session::has('old'))
   					<div class="alert alert-danger">{{ Session::get('old') }}</div>
   					@elseif (Session::has('message'))
   					<div class="alert alert-success">{{ Session::get('message') }}</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{ url('/profile/password/change') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="Username" value="{{ $data->Username }}">

						<div class="form-group @if ($errors-> has('oldpassword')) has-error @endif">
							<label class="col-md-4 control-label">Old Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="oldpassword">
								@if ($errors -> has ('oldpassword')) <p class= "help-block error-alert"> {{$errors->first('oldpassword')}} </p> @endif
							</div>
						</div>

						<div class="form-group @if ($errors-> has('password')) has-error @endif">
							<label class="col-md-4 control-label">New Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password">
								@if ($errors -> has ('password')) <p class= "help-block error-alert"> {{$errors->first('password')}} </p> @endif
							</div>
						</div>

						<div class="form-group @if ($errors-> has('password_confirmation')) has-error @endif">
							<label class="col-md-4 control-label">Confirm New Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password_confirmation">
								@if ($errors -> has ('password_confirmation')) <p class= "help-block error-alert"> {{$errors->first('password_confirmation')}} </p> @endif
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Save New Password
								</button>
							</div>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection
