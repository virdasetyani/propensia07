@extends('app')
@extends('header')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<ol class="breadcrumb">
 				<li><a href="{{ url('/home') }}">Beranda</a></li>
 				<li class="active">Profil : {{ $data->Nama }}</li>
			</ol>
		</div>
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-default">
				<div class="panel-heading">Profile</div>
				<div class="panel-body">
					<center><h1>{{ $data->Nama }}</h1><br>
					@if($data->Foto == null)
					<center><img src="{{ asset('/css/img/ava.jpg') }}" style='width:150px;height:150px'></center><br>
					@else
					<center><img src="{{ asset($data->Foto) }}" style='width:250px;height:250px'></center><br>
					@endif
					<center><a href="{{ url('/profile/edit/foto/' .$data->Username) }}" class="btn btn-primary btn-xs">Ubah Foto</a></center><br><br>
						<table class="table">
							<tr>
								<th>Nama Pengguna</th>
								<td> {{ $data->Nama }}</td>
							</tr>
							<tr>
								<th>Id Pegawai</th>
								<td> {{ $data->IdPeg }}</td>
							</tr>
							<tr>
								<th>Jenis Kelamin</th>
								<td> {{ $data->Gender }}</td>
							</tr>
							<tr>
								<th>Username</th>
								<td> {{ $data->Username }}</td>
							</tr>
							<tr>
								<th>Alamat</th>
								<td><?php echo nl2br($data->Alamat); ?></td>
							</tr>
							<tr>
								<th>Nomor Telepon</th>
								<td> {{ $data->NoTelp }}</td>
							</tr>
							<tr>
								<th>Email</th>
								<td> {{ $data->Email }}</td>
							</tr>
							<tr>
								<th>Supervisor</th>
								<td> {{ $sup->IdPeg }} - {{ $sup->Nama }}</td>
							</tr>
						</table>
					<br>
					<div class="col-md-offset-10"><a href="{{ url('/profile/edit/') }}" class="btn btn-info">Ubah Profile</a></div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection