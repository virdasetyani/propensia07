@extends('app')
@extends('header')


@section('content')
<div class="col-md-10 col-md-offset-1">
        <ol class="breadcrumb">
            <li><a href="{{ url('/home') }}">Beranda</a></li>
            <li><a href="{{ url('/regional') }}">Regional</a></li>
            <li class="active">Buat</li>
        </ol>
    </div>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2"><br>
			<div class="panel panel-default">
				<div class="panel-heading">Buat Regional</div>
				<div class="panel-body">

					@if (Session::has('message'))
   					<div class="alert alert-success">{{ Session::get('message') }}</div>
				@endif </div>
	<form class="form-horizontal" role="form" method="POST" action="{{ url('/regional/create') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		

	<div class="form-group @if ($errors-> has('NamaReg')) has-error @endif">
		<label class="col-md-4 control-label">Nama Regional <font color="red">*</font></label>
		<div class="col-md-6">
		<input type="text" class="form-control" name="NamaReg" value="{{ old('NamaReg') }}">
		@if ($errors -> has ('NamaReg')) <p class= "help-block error-alert"> {{$errors->first('NamaReg')}} </p> @endif
		</div>
	</div>
		
	<div class="form-group">
		<label class="col-md-4 control-label">Bidang </label>
			@foreach($data as $bid)
			<div class="col-md-10">
			<label class="checkbox-inline">
				<input type="checkbox"  name="NamaBid[]" value="{{ $bid->IdBid }}"> {{ $bid->NamaBid}}
			</label>
			</div>
			@endforeach
		</div>

	<div class="form-group">
		<div class="col-md-6 col-md-offset-4">
		<br><button type="submit" class="btn btn-primary">
		Buat Regional
		</button>
		</div>
	</div>
	<div style="float:right"><font color="red" size"2"> * Wajib diisi </font></div>
	
	</form>
@endsection