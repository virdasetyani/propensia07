@extends('app')
@extends('header')

@section('content')
<div class="col-md-10 col-md-offset-1">
        <ol class="breadcrumb">
            <li><a href="{{ url('/home') }}">Beranda</a></li>
            <li><a href="{{ url('/regional') }}">Regional</a></li>
            <li class="active">Ubah</li>
        </ol>
    </div>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2" style="background-color:#F8F9FA">
			<div class="panel panel-default">
				<div class="panel-heading"><center>Ubah Regional</center></div>
				<div class="panel-body">
					
					@if (Session::has('message'))
   					<div class="alert alert-success">{{ Session::get('message') }}</div>
				@endif

	<form class="form-horizontal" role="form" method="POST" action="{{ action('RegController@update') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="id" value="{{ $data->IdReg }}">
		
	<div class="form-group @if ($errors-> has('NamaReg')) has-error @endif"">
		<label class="col-md-4 control-label">Nama Regional</label>
		<div class="col-md-6">
		<input type="text" class="form-control" name="NamaReg" value="{{ $data->NamaReg }}">
		@if ($errors -> has ('NamaReg')) <p class= "help-block error-alert"> {{$errors->first('NamaReg')}} </p> @endif
		</div>
	</div>

<div class="form-group">
		<label class="col-md-4 control-label">Bidang </label>
			<div class="col-md-12" center> <br>

				@foreach($coba as $ya)
				
				<input id="cb{{$ya->IdBid}}" type="checkbox"  name="NamaBid[]"  value="{{ $ya->IdBid }}" 
				@if(in_array($ya, $oke)) checked @endif>
				<label for="cb{{$ya->IdBid}}" class="checkbox-inline">{{ $ya->NamaBid }}</label>	
				<br>
				@endforeach 
			</div>
			
		

</div>
		

	<div class="form-group">
		<div class="col-md-6 col-md-offset-4">
		<button type="submit" class="btn btn-primary">
		Ubah Regional
		</button>
		</div>
	</div>
	
	</div>
	</form>
@endsection