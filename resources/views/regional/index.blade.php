@extends('app')
@extends('header')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2" style="background-color:#F8F9FA">

		<form method="GET" role="form" action="{{ url('/regional/create') }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<p>Bidang
			<select name="IdBid">
				@foreach($data as $bidang)
				<option value="{{ $bidang->IdBid }}"> {{ $bidang->Nama }}</option>
				@endforeach
			</select>
			<button type="submit" value="submit">Pilih</button></p>
		</form>

		</div>
	</div>
</div>
</div>
@endsection

