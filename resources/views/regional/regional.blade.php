@extends('app')
@extends('header')

@section('content')
<div class="col-md-10 col-md-offset-1">
        <ol class="breadcrumb">
            <li><a href="{{ url('/home') }}">Beranda</a></li>
            <li class="active">Regional</li>
        </ol>
    </div><br><br>
@if ($data->isEmpty())
	Regional tidak ada!
@else
@if (Session::has('message'))
   					<div class="alert alert-success">{{ Session::get('message') }}</div>
				@endif
	<div class="jumbroton"><center><h3>Daftar Regional</h3></center></div>
	<div class="col-md-8 col-md-offset-2">
	<div class="panel panel-default">
	<table class="table">
		<thead>
			<tr>
				<th style="text-align:center">No</th>
				<th style="text-align:center">Cabang Regional</th>
				<th style="text-align:center">Ubah</th>
				<th style="text-align:center">Hapus</th>
			</tr>
		</thead>
		<tbody>
			<?php $no=1; ?>
			@foreach($data as $reg)
			<tr>
				<td> <?php echo $no ?></td>
				<td>{{ $reg->NamaReg}}</td>
				<td>
					<div class="col-sm-pull-1"><a href='{{ action('RegController@edit', $reg->IdReg) }}' class="btn btn-primary">Ubah</form></div></td>
				<td><div class="col-sm-pull-1"><a href='{{ action('RegController@confirm', $reg->IdReg) }}' class="btn btn-primary">Hapus</a></div></td>
			</tr>
			<?php $no++; ?>
			@endforeach
		</tbody>
	</table>
</div>
@endif
@stop
@endsection
