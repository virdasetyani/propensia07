@extends('app')
@extends('header')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Create Role</div>
				<div class="panel-body">

					@if (Session::has('message'))
   					<div class="alert alert-success">{{ Session::get('message') }}</div>
				@endif
	<form class="form-horizontal" role="form" method="POST" action="{{ url('/role/create') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

	<div class="form-group">
		<label class="col-md-4 control-label">Nama Role</label>
		<div class="col-md-6">
		<input type="text" class="form-control" name="Nama" value="{{ old('Nama') }}">
		</div>
	</div>

	<div class="form-group">
		<div class="col-md-6 col-md-offset-4">
		<button type="submit" class="btn btn-primary">
		Create Role
		</button>
		</div>
	</div>
	
	</div>
	</form>
@endsection