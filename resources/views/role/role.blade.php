@extends('app')
@extends('header')

@section('content')
@if ($data->isEmpty())
	Role tidak ada!
@else
	<div class="jumbroton"><center><h3>Daftar Role</h3></center></div>
	<div class="col-md-8 col-md-offset-2">
	<div class="panel panel-default">
	<table class="table">
		<thead>
			<tr>
				<th style="text-align:center">Id</th>
				<th style="text-align:center">Daftar Role</th>
				<th style="text-align:center">Edit</th>
				<th style="text-align:center">Delete</th>
			</tr>
		</thead>
		<tbody>
			<?php $no=1; ?>
			@foreach($data as $role)
			<tr>
				<td> <?php echo $no ?></td>
				<td>{{ $role->Nama}}</td>
				<td>
					<div class="col-sm-pull-1"><a href='{{ action('RoleController@edit', $role->IdRole) }}' class="btn btn-primary">Edit</form></div></td>
				<td><div class="col-sm-pull-1"><a href='{{ action('RoleController@confirm', $role->IdRole) }}' class="btn btn-primary">Delete</a></div></td>
			</tr>
			<?php $no++; ?>
			@endforeach
		</tbody>
	</table>
</div>
@endif
@stop
@endsection
