@extends('app')
@extends('header')

@section('content')
<div class="col-md-10 col-md-offset-1">
        <ol class="breadcrumb">
            <li><a href="{{ url('/home') }}">Beranda</a></li>
             <li><a href="{{ url('/term') }}">Term</a></li>
            <li class="active">Buat</li>
        </ol>
    </div>


<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Buat Term</div>
				<div class="panel-body">

					@if (Session::has('message'))
   					<div class="alert alert-success">{{ Session::get('message') }}</div>
				@endif
	<form class="form-horizontal" role="form" method="POST" action="{{ url('/term/create') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

	<div class="form-group">
					<label class="col-md-4 control-label">Bulan</font>
					</label>
					<div class="col-md-6">
					<select name="Bulan">
						<option value="January">January</option>
						<option value="February">February</option>
						<option value="March">March</option>
						<option value="April">April</option>
						<option value="May">May</option>
						<option value="June">June</option>
						<option value="July">July</option>
						<option value="August">August</option>
						<option value="September">September</option>
						<option value="October">October</option>
						<option value="November">November</option>
						<option value="December">December</option>
					</select>
					</div>
			</div>


	
	<div class="form-group @if ($errors->has('Tahun')) has-error @endif">
					<label class="col-md-4 control-label">Tahun <font color="red">*</font>
					</label>
					<div class="col-md-6">
						<input type="text" class="form-control" name="Tahun" value="{{ old('Tahun') }}">
							@if ($errors->has('Tahun')) <p class="help-block error-alert">{{ $errors->first('Tahun')}}</p> @endif
					</div>
			</div>
	

	<div class="form-group">
		<div class="col-md-6 col-md-offset-4">
		<button type="submit" class="btn btn-primary">
		Buat Term
		</button>
		</div>
	</div>
	<div style="float:right"><font color="red" size"2"> * Wajib diisi </font></div>
	
	
	</div>
	</form>
@endsection