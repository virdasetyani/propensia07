@extends('app')
@extends('header')

@section('content')
	<div class="col-md-10 col-md-offset-1">
        <ol class="breadcrumb">
            <li><a href="{{ url('/home') }}">Beranda</a></li>
            <li class="active">Term</li>
        </ol>
    </div><br><br>
@if ($data->isEmpty())
	Term tidak ada!
@else
@if (Session::has('message'))
   					<div class="alert alert-success">{{ Session::get('message') }}</div>
				@endif
	<div class="jumbroton"><center><h3>Daftar Term</h3></center></div>
	<div class="col-md-8 col-md-offset-2">
	<div class="panel panel-default">
	<table class="table">
		<thead>
			<tr>
				<th style="text-align:center">Term</th>
				<th style="text-align:center">Bulan</th>
				<th style="text-align:center">Tahun</th>
			</tr>
		</thead>
		<tbody>
			<?php $no=1; ?>
			@foreach($data as $term)
			<tr>
				<td> <?php echo $no ?></td>
				<td>{{ $term->Bulan}}</td>
				<td>{{ $term->Tahun}}</td>
			</tr>
			<?php $no++; ?>
			@endforeach
		</tbody>
	</table>
</div>
@endif
@stop
@endsection
