@extends('app')
@extends('header')

@section('content')

@if($data == null)
<div class="col-md-8 col-md-offset-5">
	<h1>NOT Found</h1>
	Thread not found. You can back to <a href="{{ url('home')}}">home page</a>.
</div>
@else
<div class="container-fluid">
	<div class="row">
		<div class="col-md-5 col-md-offset-3">
			<div class="panel panel-danger">
				<div class="panel-body">
					<h3>Apakah Anda yakin akan menghapus "{{ $data->Judul }}"? </h3>
				</div>

				<center>
				<a href='{{ action('ThreadController@destroy', $data->IdThread)}}' class="btn btn-primary">Ya</a>
				<a href='{{ action('ThreadController@show', $data->IdFor) }}' class="btn btn-default">Tidak</a>
				</center><br>

			</div>
		</div>		
	</div>
</div>
@endif
@endsection