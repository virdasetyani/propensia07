@extends('app')
@extends('header')

@section('content')

@if($forum == null)
<div class="col-md-8 col-md-offset-5">
	<h1>NOT Found</h1>
	Forum not found. You can back to <a href="{{ url('home')}}">home page</a>.
</div>
@else
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Buat Thread Baru</div>
				<div class="panel-body">

					<form class="form-horizontal" role="form" method="POST" action="{{ url('/thread/create') }}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="IdForum" value="{{ $forum }}">
					<div class="form-group @if ($errors->has('Judul')) has-error @endif">
						<label class="col-md-2 control-label">Judul <font color="red">*</font></label>
						<div class="col-md-8">
							<input type="text" class="form-control input-xxlarge" name="Judul" value="{{ old('Judul') }}" maxlength="60" style="width:500px;">
							@if ($errors->has('Judul')) <p class="help-block error-alert">{{ $errors->first('Judul')}}</p> @endif
						</div>
						
					</div>

					<div class="form-group @if ($errors->has('Isi')) has-error @endif">
						<label class="col-md-2 control-label">Isi Thread <font color="red">*</font></label>
						<div class="col-md-10">
							<textarea class="form-control" name="Isi">{{ old('Isi') }}</textarea>
							@if ($errors->has('Isi')) <p class="help-block error-alert">{{ $errors->first('Isi')}}</p> @endif
							<script type="text/javascript" src="<?= url('css/ckeditor/ckeditor.js') ?>"></script>
							<script>CKEDITOR.replace('Isi');</script>
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-4" style="float:right">
							<button type="submit" class="btn btn-primary">Buat Thread Baru</button>
						</div>
					</div>
					<div style="float:right"><font color="red" size="2">* harus diisi</font></div>
				
					</form>
	
				</div>
			</div>
		</div>
	</div>
</div>
@endif
@endsection