@extends('app')
@extends('header')

@section('content')
<div class="col-md-10 col-md-offset-1">
	<ol class="breadcrumb">
 		<li><a href="{{ url('/home') }}">Beranda</a></li>
 		<li class="active">Threads</li>
	</ol>
</div>

<div class="col-md-8 col-md-offset-2">
	<div class="panel panel-default" style="background-color:#C2E0FF">
		<div class="panel-body">
			<h6><?php echo nl2br($forum->Keterangan); ?></h6><br>
		</div>
	</div>
</div>

<div class="col-md-8 col-md-offset-2">
		<div class="form-group">
			<div class="jumbroton"><br>
				<div style="text-align:center"><a href="{{ url('/thrd/buat/'.$forum->IdForum) }}" class="btn btn-primary">Buat Thread Baru</a></div><br>
			</div>
			@if (Session::has('message'))
				<div class="alert alert-success">{{ Session::get('message') }}</div>
			@endif
		</div>
</div>

<div class="col-md-10 col-md-offset-1">
	<div class="panel panel-default">
	<table class="table table-hover" id="threads">
		<thead>
			<tr>
				<th style="text-align:center">Judul</th>
				<th style="text-align:center">Author</th>
				<th style="text-align:center">Tanggal dibuat</th>
				<th style="text-align:center">Waktu dibuat</th>
				@if(in_array(Auth::user()->Urole,array(1,2)))
				<th style="text-align:center">Hapus</th>
				@endif
			</tr>
		</thead>
		<tbody>
			<?php $no=1; ?>
			@foreach($thrd as $thread)
			<tr>
				<td><div class="col-sm-pull-1"><a href='{{ action('ThreadController@isi', $thread->IdThread) }}' class="btn btn-link">{{ $thread->Judul}}</a></div></td>
				<td><div class="col-sm-pull-1"><h5>{{ $thread->TUser}}</h5></div></td>
				<td><div class="col-sm-pull-1"><h5>{{ date("d M Y",strtotime($thread->Tanggal))}}</h5></div></td>
				<td><div class="col-sm-pull-1"><h5>{{ $thread->Jam}}</h5></div></td>
				@if(in_array(Auth::user()->Urole,array(1,2)))
				<td><div class="col-sm-pull-2"><a href='{{ action('ThreadController@confirm', $thread->IdThread) }}' class="btn btn-danger">Hapus</a></div></td>
				@endif
			</tr>
			<?php $no++; ?>
			@endforeach
		</tbody>
	</table>
	</div>
</div>

@stop
@endsection

@section('link')
<link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">	
<link rel="stylesheet" href="{{ asset('css/dataTables.bootstrap.css') }}">
@endsection

@section('js')
<script src="{{ asset('js/jquery.dataTables.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/dataTables.bootstrap.js') }}" type="text/javascript"></script>	
<script>
    $(document).ready(function(){
    	$('#threads').dataTable();
    });
</script>
@endsection