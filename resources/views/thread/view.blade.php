@extends('app')
@extends('header')

@section('content')
<div class="col-md-8 col-md-offset-2">
	<ol class="breadcrumb">
 		<li><a href="{{ url('/home') }}">Beranda</a></li>
 		<li><a href="{{ action('ThreadController@show', $content->IdFor) }}">Threads</a></li>
 		<li class="active">{{ $content->Judul }}</li>
	</ol>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2" style="background-color:#F8F9FA"><br>
			
			<div class="panel panel-primary ">
				<div class="panel-heading"><font size="3"><b>{{ $content->Judul }}</b></font><br>
					<font size="2">by {{ $content->TUser }} - {{ date("d M Y",strtotime($content->Tanggal))}}, {{ date("g:i A",strtotime($content->Jam))}}</font>
				</div>
				
				<div class="panel-body"><?php echo nl2br($content->Isi); ?><br><br>
					<div class="panel-footer" style="background-color:#FFFFFF">
 					<div style="float:right"><a href="#jump"><font size="2">Reply</font></a></div>
 				</div>
			</div>
		</div>	

		<div class="panel-body">
			<?php $no=1; ?>
			@foreach($komentar as $com)

			<div class="panel panel-default">
				<div class="panel-heading" style="background-color:#99C6DB"><b>Re: {{ $content->Judul }}</b><br>
					<font size="2">by {{ $com->CUser }} - {{ date("d M Y",strtotime($com->Tanggal))}}, {{ date("g:i A",strtotime($com->Jam))}}</font>
				</div>
		
				<div class="panel-body">
					<?php echo nl2br($com->Isi); ?>
				</div>
			</div>
			<?php $no++; ?>
			@endforeach
		</div>

		<div class="panel panel-default" style="border-color:#1975A3">
				<form class="form-horizontal" role="form" method="POST" action="{{ url('/thread/comment') }}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">

					<div class="panel-body @if ($errors->has('Comment')) has-error @endif">
						<h4>Tulis komentar :</h4>
 						<a name="jump"><textarea class="form-control" rows="3" name="Comment" id="content"></textarea>
 						@if ($errors->has('Comment')) <p class="help-block error-alert">{{ $errors->first('Comment')}}</p> @endif</a>
						<script type="text/javascript" src="<?= url('css/ckeditor/ckeditor.js') ?>"></script>
						<script>CKEDITOR.replace('Comment');</script>
 						<br>
 						<div style="float:right">
 							<button type="submit" class="btn btn-primary">Tambah komentar</button>
 						</div>
 						<input type="hidden" name="IdThread" value=" {{ $content->IdThread }}">
 					</div>
 				</form>
		</div>

	</div>
</div>
@endsection